Folder Name: InstallerProj_V2
The 'InstallerProj_V2' folder contains Visual Studio Project for building the Installer.

Folder Name: Release
The 'Release' folder contains the binary contents/files and other content required for 'Visual Studio Project'

Folder Name: Win7_msiexec

The 'Win7_msiexec' folder contains wondows 'msiexec' and helper file 'msiexec.exe.mui' required for supporting the uninstallation from 'Start Menu'

File Name: YugamiruCloud_JP_2.0_(New)_24_Aug_19_LiveServer_S1
The msi file 'YugamiruCloud_JP_2.0_(New)_24_Aug_19_LiveServer_S1' last 2.0 stable installer shared for testing (not required for new installer creation)

Note: Folder & File locations should not be changed inside parent folder.