﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WebComCation
{
    public class ServerOutInMsg
    {
        public string LicenseKey { get; set; }
        public string ComputerName { get; set; }
        public string ComputerID { get; set; }
        public string Status { get; set; }
        public string keyStatus { get; set; }
        public string ServerMessage { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string ActivationDate { get; set; }
        public string LastRunDate { get; set; }

        //Below are hardcoded messae samples for testing only
        public static string hardSampleOfRequestMessage = "[{\"Computer_id\":\"ComputerID/Processer ID from System\",\"Computer_name\":\"ComputerName\",\"Activation_key\":\"LicenseKeyValueFromServer\"}]";
        public static string hardSampleOfResponseMessage ="{\"LicenseKey\":\"LicenseKeyValueFromServer\",\"ComputerName\":\"ComputerNameValueFromServer\",\"ComputerID\":\"ComputerIDValueFromServer\",\"Status\":\"StatusValueFromServer\",\"Message\":\"MessageValueFromServer\",\"StartDate\":\"StartDateValueFromServer\",\"EndDate\":\"EndDateValueFromServer\",\"ActivationDate\":\"ActivationDateValueFromServer\",\"ComputerKey\":\"ComputerBoundKeyFromServer\",\"LicenseType\":\"LicenseTypeValue\",\"KeyStatus\":\"StatusofLicenseKey\"}";

        //
         static string templateOfRequestMessage = "[{\"Computer_id\":\"{0}\",\"Computer_name\":\"{1}\",\"Activation_key\":\"{2}\"}]";
         static string templateOfResponseMessage = "{\"LicenseKey\":\"{0}\",\"ComputerName\":\"{1}\",\"ComputerID\":\"{2}\",\"Status\":\"{3}\",\"Message\":\"{4}\",\"StartDate\":\"{5}\",\"EndDate\":\"{6}\",\"ActivationDate\":\"{7}\",\"ComputerKey\":\"{8}\",\"LicenseType\":\"{9}\",\"KeyStatus\":\"{10}\"}";

       
        public ServerOutInMsg()
        {
            EndDate = Utility.DateTimeToString(DateTime.Now.AddHours(1));
            StartDate = Utility.DateTimeToString(DateTime.Now);
            LastRunDate = Utility.DateTimeToString(DateTime.Now);
        }


        public string GetRawResponseStringToSend()
        {
            string send_message = "";
            string strcomputerID = keyRequest.GetComputerID();
            string strcomputerName = System.Environment.MachineName;
            if ((strcomputerID == "" || strcomputerID == null) ||
                (strcomputerName == "" || strcomputerName == null) ||
                    (this.LicenseKey == "" || this.LicenseKey == null))
            {
                send_message = string.Format(templateOfRequestMessage, strcomputerID, strcomputerName, this.LicenseKey);
            }             

            return send_message;
        }




    }
}
