﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebComCation
{
    public partial class frmCustomMessageBox : Form
    {
        //Updated by Sumit GTL-129   ---START
        int originalHeight = 0;
        //Updated by Sumit GTL-129   ---END

        public frmCustomMessageBox()
        {
            InitializeComponent();
            originalHeight = this.Height;
            btnDetails.Text = Properties.Resources.SHOW_DETAILS;
        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            //Updated by Sumit GTL-129   ---START
            //if (this.Height < 150)
            //{
            //    this.Height = 220;
            //}
            //else
            //{
            //    this.Height = 140;
            //}
            //return;
            if (this.Height < originalHeight + 100)
            {
                this.Height = originalHeight + 100;
            }
            else
            {
                this.Height = originalHeight;
            }

            //Updated by Sumit GTL-129   ---END
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCustomMessageBox_Load(object sender, EventArgs e)
        {
            btnDetails.Text = Properties.Resources.SHOW_DETAILS;
        }
    }
}
