﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebComCation
{
    /// <summary>
    /// Created by Sumit for various m/y and resource release/cleaning activities
    /// </summary>
    public static class Cleaner
    {
        //Method Added by Sumit GSP-1299
        public static void ScrosheetCleaner()
        {
            string[] s = { "\\bin" };
            string Specific_Folder = Application.StartupPath.Split(s, StringSplitOptions.None)[0] + "\\Scoresheet\\" + Properties.Resources.CURRENT_LANGUAGE;
            List<string> lstImgsInUse = new List<string>();//Added by Sumit GTL#141
            System.IO.DirectoryInfo di = new DirectoryInfo(Specific_Folder);
            try
            {
                if (Directory.GetFiles(Specific_Folder).Length > 0)
                {
                    //--Added by Rajnish For GSp-1360-----start
                    string[] filePaths = Directory.GetFiles(Specific_Folder);
                    //Updated by sumit GTL#141   ---START
                    //foreach (string filePath in filePaths)
                    //    File.Delete(filePath);
                    foreach (string filePath in filePaths)
                    {
                        try
                        {
                            File.Delete(filePath);
                        }
                        catch
                        {
                            lstImgsInUse.Add(filePath);
                        }
                    }
                    if(lstImgsInUse.Count>0)
                    {
                        System.Threading.Thread.Sleep(1000);
                        foreach (string filePath1 in lstImgsInUse)
                        {
                            File.Delete(filePath1);
                        }
                    }

                    //Updated by sumit GTL#141   ---END

                    //--Added by Rajnish For GSp-1360-----END
                }
                //Array.ForEach(Directory.GetFiles(Specific_Folder), File.Delete);//Commented by Rajnish For GSP-1360
                if (!File.Exists(Specific_Folder + "temp.txt"))
                {
                    File.CreateText(Specific_Folder + "\\temp.txt").Dispose();
                }
            }
            catch (IOException ex)
            {
                //MessageBox.Show(ex.Message);
                //file is currently locked
                System.Threading.Thread.Sleep(1000);//Added by Sumit GSP-1047
                if (Directory.GetFiles(Specific_Folder).Length > 0)
                {
                    //--Added by Rajnish For GSp-1360-----start
                    string[] filePaths = Directory.GetFiles(Specific_Folder);
                    foreach (string filePath in filePaths)
                        File.Delete(filePath);
                    //--Added by Rajnish For GSp-1360-----END
                }
                //Array.ForEach(Directory.GetFiles(Specific_Folder), File.Delete);//Commented by Rajnish for GSp-1360
                if (!File.Exists(Specific_Folder + "temp.txt"))
                {
                    File.CreateText(Specific_Folder + "\\temp.txt").Dispose();
                }
            }            
            //MessageBox.Show("MyHandler caught : " + e.Message);

        }
    }
}
