﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WebComCation
{
    //Added three elements QlmDownValid, QlmDownInvalid, QlmDownExpired Sumit GSP-1321
    public enum LicenseStatus { Valid=1,Invalid=0,Unknown=-1,Expired=2,Error=3,ReleasedFromServer=4, QlmDownValid=5, QlmDownInvalid=6, QlmDownExpired=7 };
}
