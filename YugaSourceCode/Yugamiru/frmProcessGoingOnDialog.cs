﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    /// <summary>
    /// Implemented by Sumit on 01-May-2019 for GSP-1265 (To Handle the User action during Background Data Restore Process )
    /// </summary>
    public partial class frmProcessGoingOnDialog : Form
    {
        Thread busyThread;
        System.Timers.Timer tmr = new System.Timers.Timer();
        public frmProcessGoingOnDialog(Thread busythread,Label lblAlertTextDisplay=null)
        {
            InitializeComponent();
            if (lblAlertTextDisplay != null)
                lblAlertTextDisplay.Tag = "HIDE";
            busyThread = busythread;
            tmr.Interval = 5000;
            tmr.AutoReset = true;
            tmr.Elapsed += Tmr_Elapsed;
            tmr.Start();
        }

        private void Tmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if(!busyThread.IsAlive)
            {
                if (this.InvokeRequired)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        //kv.ShowDialog();
                        this.Visible = false;
                        tmr.AutoReset = false;
                        tmr.Stop();
                        tmr.Dispose();
                        //MessageBox.Show(CloudManager.AppDataReader.GetRecordCount().ToString() + " Patient reports downloaded successfully", "Yugamiru");
                        MessageBox.Show(CloudManager.AppDataReader.GetRecordCount().ToString() +" "+ Properties.Resources.PATIENT_REPORTS_DOWNLOADED, "Yugamiru");


                    });


                    
                }
            }
            else
            {
                if (this.InvokeRequired)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        int reportDownloadedCount = CloudManager.AppDataReader.GetRecordCount();
                        //lblDownloadCountMsg.Text = "(" + reportDownloadedCount.ToString() + " Reports Downloaded)";
                        lblDownloadCountMsg.Text = "(" + reportDownloadedCount.ToString() + " " + Properties.Resources.REPORTS_DOWNLOADED + ")";
                        this.Refresh();

                    });
                }                
            }
        }
    }
}
