﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Yugamiru
{
    //Class added by sumit on 25-Feb-2019 for general purpose use
    public static class UtilityGlobal
    {
        public static clsIDNameHandler ID_NAME_Handler;
        public static void Set_ID_NAME_Handler(string fullID, string fullName, string AppLanguage)
        {
            ID_NAME_Handler = new clsIDNameHandler(fullID,fullName, AppLanguage);            
        }

        public static string YGAFilePathForName = "";//Added by Sumit For GSP-1341

        /// <summary>
        /// Added by Sumit For GSP-1341
        /// Returns the patient name from a YGA File (any Language)
        /// </summary>
        /// <param name="YGAfilePath"></param>
        /// <returns></returns>
        public static string GetPatientName(string YGAfilePath)
        {
            string FilePath = YGAfilePath;            
            var arLine = File.ReadAllLines(FilePath, Encoding.GetEncoding("shift-jis"));
            var cb = arLine[0] + arLine[1];
            #region UseFull_COmmented_Code
            //var badContent = Encoding.GetEncoding(1252).GetBytes(cb);
            //var jpContent = Encoding.GetEncoding(932).GetString(badContent);
#endregion
            string[] stSpliterWords = new string[] { "UserName", "UserBirthYear" };            
            char[] spltrChar = new char[] { '#' };

            string  jpContent = cb;
            string strWithName = jpContent.Split(stSpliterWords, StringSplitOptions.None)[1];
            strWithName = strWithName.Replace("\0", "#");            
            List<string> lstAll = new List<string>(strWithName.Split(spltrChar, StringSplitOptions.None));
            int lstlen = 0;
            string Name = "";
            foreach (string st in lstAll)
            {
                if (lstlen < st.Length)
                {
                    lstlen = st.Length;
                    Name = st;
                }
            }
            Name = Name.TrimStart(new char[] { '0' });
            Name = Name.Replace('\u0015', ' ').Trim();
            return Name;
        }

        /// <summary>
        /// Implemented By Sumit For GTL#130.
        /// This method returns 3 Char Month name Like Jan or Feb etc
        /// </summary>
        /// <param name="month">Sequence number of the month like 1 for jan and 12 for december etc.</param>
        /// <returns></returns>
        public static string GetShortMonthName(int month)
        {
            string strMonth = "";

            if(month==1)
            {
                strMonth = "Jan";
            }
            else if(month==2)
            {
                strMonth = "Feb";
            }
            else if (month == 3)
            {
                strMonth = "Mar";
            }
            else if (month == 4)
            {
                strMonth = "Apr";
            }
            else if (month == 5)
            {
                strMonth = "May";
            }
            else if (month == 6)
            {
                strMonth = "Jun";
            }
            else if (month == 7)
            {
                strMonth = "Jul";
            }
            else if (month == 8)
            {
                strMonth = "Aug";
            }
            else if (month == 9)
            {
                strMonth = "Sep";
            }
            else if (month == 10)
            {
                strMonth = "Oct";
            }
            else if (month == 11)
            {
                strMonth = "Nov";
            }
            else if (month == 12)
            {
                strMonth = "Dec";
            }

            return strMonth;
        }


    }

    
}
