﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class MyData
    {

        public string m_ID;       // ID
        public string m_Name;     // –¼‘O
        public int m_Gender;      // «•Ê 0:UNKNOWN 1:’j 2:—
                                  //int			m_DoB;		// ¶”NŒŽ“ú
        public int m_iBirthDay;
        public int m_iBirthMonth;
        public int m_iBirthYear;
        public string m_Time;     // ‘ª’è“úŽž YYYYMMDDhhmmss
        public double m_Height; // g’·
        public string m_Comment;  // ƒRƒƒ“ƒg
        public int m_iBenchmarkDistance;

        public FrontBodyPosition m_FrontBodyPositionStanding = new FrontBodyPosition(); // ŠÖß‚ÌÀ•Wi‘ª’èŠJŽnŽžj
        public FrontBodyPosition m_FrontBodyPositionKneedown = new FrontBodyPosition(); // ŠÖß‚ÌÀ•Wi‘ª’èŠ®—¹Žžj
        public SideBodyPosition m_SideBodyPosition = new SideBodyPosition();

        int version;
        public MyData()
        {
            m_ID = "";
            m_Name = "";
            m_Gender = 0;
            //m_DoB	 = 0;
            m_iBirthDay = 0;
            m_iBirthMonth = 0;
            m_iBirthYear = 0;
            m_Height = 0.0;
            m_Comment = "";
            m_iBenchmarkDistance = 80;

            version = Constants.VER_2006Summer_RELEASE;
        }

        ~MyData()
        {
        }

        public void SetDoB(string strY, string strM, string strD)
        {
            //	m_DoB = atoi(strY)*10000 + atoi(strM)*100 + atoi(strD);
            m_iBirthDay = Convert.ToInt32(strD);
            m_iBirthMonth = Convert.ToInt32(strM);
            m_iBirthYear = Convert.ToInt32(strY);
        }

        public void GetDoB(ref string strY, ref string strM, ref string strD)
        {
            //	CString str;
            //	str.Format("%d", m_DoB);

            //	strY = str.Mid(0, 4);
            //	strM = str.Mid(4, 2);
            //	strD = str.Mid(6, 2);
            strY = Convert.ToString(m_iBirthYear);
            strM = Convert.ToString(m_iBirthMonth);
            strD = Convert.ToString(m_iBirthDay);
        }

        /* ‘ª’è“úŽžEŽžŠÔ‚ð”NEŒŽE“úEŽžŠÔ‚É•ª‰ð‚µ‚ÄƒZƒbƒg */
        public void GetAcqDate(ref string strY, ref string strM, ref string strD, ref string strT)
        {
            string str;
            //--Added by Rajnish For GTL #63---Start
            //str = m_Time;
            str = "20" + m_Time;
            //	strY = "20";
            //	strY += str.Mid(0, 2);

            //strY = str.Substring(0, 2);
            //strM = str.Substring(3, 2);
            //strD = str.Substring(6, 2);
            //strT = str.Substring(9, 5);
            //if (m_Time.Length < 15)
            //{
            //    strY = str.Substring(0, 2);
            //    strM = str.Substring(3, 2);
            //    strD = str.Substring(6, 2);
            //    strT = str.Substring(9, 5);
            //}
            //else
            {
                strY = str.Substring(0, 4);
                strM = str.Substring(5, 2);
                strD = str.Substring(8, 2);
                strT = str.Substring(11, 5);
            }
            //--Added by Rajnish For GTL #63---END


        }

        /* DOBƒf[ƒ^‚ð”NEŒŽE“ú‚É•ª‰ð‚µ‚ÄƒZƒbƒg */
        public int GetAge()
        {
            //	int birth = m_DoB;
            int d0, d1, m0, m1, y0, y1;
            //	d1 = birth%100;	birth/=100;
            //	m1 = birth%100;	birth/=100;
            //	y1 = birth;

            d1 = m_iBirthDay;
            m1 = m_iBirthMonth;
            y1 = m_iBirthYear;

            //struct tm * today;
            //time_t t;
            DateTime dt1 = new DateTime(y1, m1, d1);
            DateTime dt2 = DateTime.Now;

            var age = dt2.Year - dt1.Year;

            return age;
        }

        /*
        @”N—î‚ð‚R¢‘ã‚É•ª‰ð
        @25ÎˆÈ‰ºA55ÎˆÈ‰ºA‚»‚êˆÈã
        @‚½‚¾‚µAŒ»’iŠK‚Å‚Í”»’è‚Í¢‘ã‚Åˆá‚¢‚ª–³‚¢
        */
        public int GetGene()
        {
            if (GetAge() < 26)
            {
                return 0;
            }
            else if (GetAge() < 56)
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }

        public bool IsDetected(FrontBodyPosition m_FrontBodyPositionStanding, FrontBodyPosition
            m_FrontBodyPositionKneedown)
        {
            if (!m_FrontBodyPositionStanding.IsUnderBodyPositionDetected())
            {
                return false;
            }
            if (!m_FrontBodyPositionStanding.IsKneePositionDetected())
            {
                return false;
            }
            if (!m_FrontBodyPositionStanding.IsUpperBodyPositionDetected())
            {
                return false;
            }
            if (!m_FrontBodyPositionKneedown.IsUnderBodyPositionDetected())
            {
                return false;
            }
            if (!m_FrontBodyPositionKneedown.IsKneePositionDetected())
            {
                return false;
            }
            if (!m_FrontBodyPositionKneedown.IsUpperBodyPositionDetected())
            {
                return false;
            }
            return true;
        }

        /* ƒf[ƒ^‚Ì‰Šú‰» */
        public void ClearData()
        {
            m_ID = "";
            m_Name = "";
            m_Gender = 0;
            //	m_DoB = 0;
            m_iBirthDay = 0;
            m_iBirthMonth = 0;
            m_iBirthYear = 0;
            m_Height = 0.0;
            m_iBenchmarkDistance = 80;
            m_Comment = "";
        }


        public void InitBodyBalance(int width, int height, CCalibInf CalibInf, CCalcPostureProp CalcPostureProp)
        {
            m_FrontBodyPositionStanding.Init(width, height, CalibInf, CalcPostureProp);
            m_FrontBodyPositionKneedown.Init(width, height, CalibInf, CalcPostureProp);
            m_SideBodyPosition.Init(width, height, CalibInf, CalcPostureProp);

            m_FrontBodyPositionStanding.InitUpperPostion(width, height, CalibInf);
            m_FrontBodyPositionKneedown.InitUpperPostion(width, height, CalibInf);
            m_SideBodyPosition.InitUpperPostion(width, height, CalibInf);
        }
        public void InitSideBodyPosition(int width, int height, CCalibInf CalibInf, CCalcPostureProp CalcPostureProp)
        {
            m_SideBodyPosition.Init(width, height, CalibInf, CalcPostureProp);
            m_SideBodyPosition.InitUpperPostion(width, height, CalibInf);
        }

        public void CalcBodyAngleStanding(ref FrontBodyAngle angleStanding, double dCameraAngle)
        {
            angleStanding.CalcByPosition(true, ref m_FrontBodyPositionStanding, dCameraAngle);
        }

        public void CalcBodyAngleKneedown(ref FrontBodyAngle angleKneedown, double dCameraAngle)
        {
            angleKneedown.CalcByPosition(false, ref m_FrontBodyPositionKneedown, dCameraAngle);

        }

        public void CalcBodyAngleSide(ref SideBodyAngle angleSide, double dCameraAngle)
        {
            angleSide.CalcByPosition(ref m_SideBodyPosition, dCameraAngle);
        }

        public void CalcStandingFrontBodyResultData(ref FrontBodyResultData resultStanding, double dCameraAngle, ResultActionScriptMgr ResultActionScriptMgr)
        /* CResultPropArray &ResultPropArray */
        {
            FrontBodyAngle bodyAngleStanding = new FrontBodyAngle();
            bodyAngleStanding.CalcByPosition(true, ref m_FrontBodyPositionStanding, dCameraAngle);

            resultStanding.Reset();
            int gender = ((m_Gender == 2) ? 1 : 0);
            int gene = GetGene();
            resultStanding.Calc(gender, gene, bodyAngleStanding, ResultActionScriptMgr);
        }

        public void CalcKneedownFrontBodyResultData(ref FrontBodyResultData resultKneedown, double dCameraAngle, ResultActionScriptMgr ResultActionScriptMgr /* CResultPropArray &ResultPropArray */ )
        {
            FrontBodyAngle bodyAngleKneedown = new FrontBodyAngle();
            bodyAngleKneedown.CalcByPosition(false, ref m_FrontBodyPositionKneedown, dCameraAngle);

            resultKneedown.Reset();
            int gender = ((m_Gender == 2) ? 1 : 0);
            int gene = GetGene();
            resultKneedown.Calc(gender, gene, bodyAngleKneedown, ResultActionScriptMgr);
        }

        public void CalcSideBodyResultData(ref SideBodyResultData resultSide, double dCameraAngle,
                        ResultActionScriptMgr ResultActionScriptMgr)
        {
            SideBodyAngle bodyAngleSide = new SideBodyAngle();
            bodyAngleSide.CalcByPosition(ref m_SideBodyPosition, dCameraAngle);

            resultSide.Reset();
            int gender = ((m_Gender == 2) ? 1 : 0);
            int gene = GetGene();
            resultSide.Calc(gender, gene, bodyAngleSide, ResultActionScriptMgr);
        }

        public void CalcBodyBalanceResultData(
                ResultData result,
                double dCameraAngle,
                ResultActionScriptMgr ResultActionScriptMgr,
                ResultActionScriptMgr ResultActionScriptMgrSide)
        {
            FrontBodyAngle bodyAngleStanding = new FrontBodyAngle();
            bodyAngleStanding.CalcByPosition(true, ref m_FrontBodyPositionStanding, dCameraAngle);

            FrontBodyAngle bodyAngleKneedown = new FrontBodyAngle();
            bodyAngleKneedown.CalcByPosition(false, ref m_FrontBodyPositionKneedown, dCameraAngle);

            SideBodyAngle bodyAngleSide = new SideBodyAngle();
            bodyAngleSide.CalcByPosition(ref m_SideBodyPosition, dCameraAngle);


            result.Reset();
            int gender = ((m_Gender == 2) ? 1 : 0);
            int gene = GetGene();
            result.Calc(
                gender,
                gene,
                bodyAngleStanding,
                bodyAngleKneedown,
                bodyAngleSide,
                ResultActionScriptMgr,
                ResultActionScriptMgrSide);
        }

        /*public int  CalcYumgamiPoint( 
                double dCameraAngle,
                 ResultActionScriptMgr ResultActionScriptMgr,
                 ResultActionScriptMgr ResultActionScriptMgrSide ) 		
        {
            ResultData result = new ResultData();
            CalcBodyBalanceResultData( 
                result, 
                dCameraAngle,
                ResultActionScriptMgr,
                ResultActionScriptMgrSide );
            return result.CalcYugamiPoint();
        }

        public int  CalcYumgamiPointRank( 
                double dCameraAngle, 
                ResultActionScriptMgr ResultActionScriptMgr,
                ResultActionScriptMgr ResultActionScriptMgrSide ) 		
        {
            ResultData result;
            CalcBodyBalanceResultData( 
                result, 
                dCameraAngle,
                ResultActionScriptMgr,
                ResultActionScriptMgrSide );
            return result.CalcYugamiPointRank();
        }*/


        public string GetID()
        {
            return m_ID;
        }

        public void SetID(string pchID)
        {
            m_ID = pchID;
        }

        public string GetName()
        {
            return m_Name;
        }

        public void SetName(string pchName)
        {
            m_Name = pchName;
        }

        public int GetGender()
        {
            return m_Gender;
        }

        public void SetGender(int iGender)
        {
            m_Gender = iGender;
        }

        public double GetHeight()
        {
            return m_Height;
        }

        public string GetMeasurementTime()
        {
            return m_Time;
        }

        public void SetMeasurementTime(string pchMeasurementTime)
        {
            m_Time = pchMeasurementTime;
        }


        public void SetHeight(float fHeight)
        {
            m_Height = fHeight;
        }

        public string GetComment()
        {
            return m_Comment;
        }

        public void SetComment(string pchComment)
        {
            m_Comment = pchComment;
        }

        public int GetBenchmarkDistance()
        {
            return m_iBenchmarkDistance;
        }

        public void SetBenchmarkDistance(int iBenchmarkDistance)
        {
            m_iBenchmarkDistance = iBenchmarkDistance;
        }

        public int GetVersion()
        {
            return version;
        }

        public void GetStandingFrontBodyPosition(ref FrontBodyPosition FrontBodyPositionStanding)
        {
            FrontBodyPositionStanding = m_FrontBodyPositionStanding;
        }

        public void GetKneedownFrontBodyPosition(ref FrontBodyPosition FrontBodyPositionKneedown)
        {
            FrontBodyPositionKneedown = m_FrontBodyPositionKneedown;
        }
        public void GetSideBodyPosition(ref SideBodyPosition SideBodyPosition)
        {

            SideBodyPosition = m_SideBodyPosition;
        }

        /*public void GetSideBodyPosition(SideBodyPosition SideBodyPosition ) const
        {
            SideBodyPosition = m_SideBodyPosition;
        }*/

        public void SetStandingFrontBodyPosition(FrontBodyPosition FrontBodyPositionStanding)
        {
            m_FrontBodyPositionStanding = FrontBodyPositionStanding;
        }

        public void SetKneedownFrontBodyPosition(FrontBodyPosition FrontBodyPositionKneedown)
        {
            m_FrontBodyPositionKneedown = FrontBodyPositionKneedown;
        }

        public void SetSideBodyPosition(SideBodyPosition SideBodyPosition)
        {
            m_SideBodyPosition = SideBodyPosition;
        }

        public void InitSideBodyPosition(int width, int height, ref CCalibInf CalibInf, ref CCalcPostureProp CalcPostureProp)
        {
            m_SideBodyPosition.Init(width, height, CalibInf, CalcPostureProp);
            m_SideBodyPosition.InitUpperPostion(width, height, CalibInf);
        }

        public void CalcBodyBalanceResultData(
                ref ResultData result,
                double dCameraAngle,

         ref ResultActionScriptMgr ResultActionScriptMgr,
         ref ResultActionScriptMgr ResultActionScriptMgrSide,
         int iJudgeMode)
        {

            FrontBodyAngle bodyAngleStanding = new FrontBodyAngle();
            bodyAngleStanding.CalcByPosition(true, ref m_FrontBodyPositionStanding, dCameraAngle);
            // Added by akuroda 20070809 to calculate upper body angle from side view
            //#if defined(PEDX)
            //bodyAngleStanding.CalcSideBodyByPosition( ref m_BodyPositionSide, dCameraAngle );
            //#endif

            FrontBodyAngle bodyAngleKneedown = new FrontBodyAngle();
            bodyAngleKneedown.CalcByPosition(false, ref m_FrontBodyPositionKneedown, dCameraAngle);

            SideBodyAngle bodyAngleSide = new SideBodyAngle();
            bodyAngleSide.CalcByPosition(ref m_SideBodyPosition, dCameraAngle);


            result.Reset();

            int Gender = ((m_Gender == 2) ? 1 : 0);
            int gene = GetGene();
            result.Calc(
            Gender,
            gene,
            bodyAngleStanding,
            bodyAngleKneedown,
            bodyAngleSide,
            ResultActionScriptMgr,
            ResultActionScriptMgrSide,
            iJudgeMode);
        }

        /*  public int GetID()
          {
              return m_ID;
          }

          public void SetID(int pchID)
          {
              m_ID = pchID;
          }

          public string GetName()
          {
              return m_Name;
          }

          public void SetName(string pchName)
          {
              m_Name = pchName;
          }

          public int GetSex()
          {
              return m_Sex;
          }

          public void SetSex(int iSex)
          {
              m_Sex = iSex;
          }

          public double GetHeight()
          {
              return m_Height;
          }

          public string GetMeasurementTime()
          {
              return m_Time;
          }

          public void SetMeasurementTime(string pchMeasurementTime)
          {
              m_Time = pchMeasurementTime;
          }*/


        /* public void SetHeight(float fHeight)
         {
             m_Height = fHeight;
         }*/

        /*  public string GetComment()
          {
              return m_Comment;
          }*/

        /*  public void SetComment(string pchComment)
          {
              m_Comment = pchComment;
          }*/

        /*  public int GetBenchmarkDistance()
          {
              return m_iBenchmarkDistance;
          }*/

        /* public void SetBenchmarkDistance(int iBenchmarkDistance)
         {
             m_iBenchmarkDistance = iBenchmarkDistance;
         }*/

        /*  public int GetVersion()
          {
              return version;
          }*/

        /*  public void GetStandingFrontBodyPosition(FrontBodyPosition FrontBodyPositionStanding)
          {
              FrontBodyPositionStanding = m_FrontBodyPositionStanding;
          }*/

        /* public void GetKneedownFrontBodyPosition(FrontBodyPosition FrontBodyPositionKneedown)
         {
             FrontBodyPositionKneedown = m_FrontBodyPositionKneedown;
         }*/

        /*public void GetSideBodyPosition(SideBodyPosition SideBodyPosition ) const
        {
            SideBodyPosition = m_SideBodyPosition;
        }*/

        /*  public void SetStandingFrontBodyPosition(FrontBodyPosition FrontBodyPositionStanding)
          {
              m_FrontBodyPositionStanding = FrontBodyPositionStanding;
          }*/

        /* public void SetKneedownFrontBodyPosition(FrontBodyPosition FrontBodyPositionKneedown)
         {
             m_FrontBodyPositionKneedown = FrontBodyPositionKneedown;
         }*/

        /*public void SetSideBodyPosition( CSideBodyPosition &SideBodyPosition )
        {
            m_SideBodyPosition = SideBodyPosition;
        }*/
        /*
        void CData::InitSideBodyPosition(int width, int height, const CCalibInf &CalibInf, const CCalcPostureProp &CalcPostureProp )
        {
            m_SideBodyPosition.Init(width, height, CalibInf, CalcPostureProp);
            m_SideBodyPosition.InitUpperPostion(width, height, CalibInf);
        }                                                                                                                                                                                                  
        */

        public int CalcDataSizeOfStatementBlock()
        {
            string strTmp = string.Empty;
            string strBirthYear = string.Empty;
            string strBirthMonth = string.Empty;
            string strBirthDay = string.Empty;

            int iRet = 0;
            iRet += sizeof(int);	// ƒuƒƒbƒN‘S‘Ì‚ÌƒTƒCƒY.
            iRet += CalcDataSizeOfStringAssignmentStatement("UserID", GetID()) + sizeof(int);
            iRet += CalcDataSizeOfStringAssignmentStatement("UserName", GetName()) + sizeof(int);

            GetDoB(ref strBirthYear, ref strBirthMonth, ref strBirthDay);
            iRet += CalcDataSizeOfStringAssignmentStatement("UserBirthYear", strBirthYear) + sizeof(int);
            iRet += CalcDataSizeOfStringAssignmentStatement("UserBirthMonth", strBirthMonth) + sizeof(int);
            iRet += CalcDataSizeOfStringAssignmentStatement("UserBirthDay", strBirthDay) + sizeof(int);
            //strTmp.Format( "%d", GetGender() );
            iRet += CalcDataSizeOfStringAssignmentStatement("UserSex", GetGender().ToString()) + sizeof(int);
            //strTmp.Format( "%.1f", GetHeight() );
            iRet += CalcDataSizeOfStringAssignmentStatement("UserHeight", GetHeight().ToString()) + sizeof(int);
            iRet += CalcDataSizeOfStringAssignmentStatement("MeasurementTime", GetMeasurementTime()) + sizeof(int);
            iRet += CalcDataSizeOfStringAssignmentStatement("Comment", GetComment()) + sizeof(int);
            //strTmp.Format( "%d", GetBenchmarkDistance() );
            iRet += CalcDataSizeOfStringAssignmentStatement("BenchmarkDistance", GetBenchmarkDistance().ToString()) + sizeof(int);
            iRet += m_FrontBodyPositionStanding.CalcDataSizeOfStandingStatementBlock();
            iRet += m_FrontBodyPositionKneedown.CalcDataSizeOfKneedownStatementBlock();
            iRet += m_SideBodyPosition.CalcDataSizeOfStatementBlock();
            return iRet;
        }
        public int CalcDataSizeOfStatementBlock(JointEditDoc GetDocument)
        {
            int iRet = 0;
            iRet += CalcDataSizeOfImageAssignmentStatementAfterCompression("StandingImage",
                GetDocument.m_FrontStandingImageBytes) + sizeof(int);
            iRet += CalcDataSizeOfImageAssignmentStatementAfterCompression("KneedownImage",
                GetDocument.m_FrontKneedownImageBytes) + sizeof(int);
            iRet += CalcDataSizeOfImageAssignmentStatementAfterCompression("SideImage",
                GetDocument.m_SideImageBytes) + sizeof(int);

            return iRet;

        }
        private int CalcDataSizeOfImageAssignmentStatementAfterCompression(string pchSymbolName, byte[] pbyteImage)
        {
            byte[] byte_compressed = Compress(pbyteImage, false);
            int iEncodedImageSize = byte_compressed.Length;
            int iRet = CalcDataSizeOfImageAssignmentStatement(pchSymbolName, iEncodedImageSize);
            return iRet;
        }
        int CalcDataSizeOfImageAssignmentStatement(string pchSymbolName, int iImageSize)
        {

            int iRet = 0;
            //iRet += sizeof( int );			// Statement‘S‘Ì‚ÌƒTƒCƒY.
            iRet += sizeof(char); // ƒIƒyƒR[ƒh‚h‚c.
            iRet += sizeof(char); // Symbol‚Ì•¶Žš—ñ’·‚³.
            iRet += pchSymbolName.Length; // Symbol‚ÌŽÀ‘Ì.
            iRet += sizeof(int);                // ‰æ‘œƒTƒCƒY.
            iRet += iImageSize;                 // ‰æ‘œ‚ÌŽÀ‘Ì.
            return iRet;
        }


        public int CalcDataSizeOfStringAssignmentStatement(string pchSymbolName, string pchValue)
        {
            int iRet = 0;
            //iRet += sizeof( int );			// Statement‘S‘Ì‚ÌƒTƒCƒY.
            iRet += sizeof(char);   // ƒIƒyƒR[ƒh‚h‚c.
            iRet += sizeof(char);   // Symbol‚Ì•¶Žš—ñ’·.
            iRet += pchSymbolName.Length; // Symbol‚ÌŽÀ‘Ì.
            iRet += sizeof(ushort);	// ’l‚Ì•¶Žš—ñ’·.
            if (pchValue != null)
                iRet += pchValue.Length;            // ’l‚ÌŽÀ‘Ì.
            return iRet;
        }
        int WriteStringAssignmentStatement(char[] pbyteOut, int iOffset, int iSize, string pchSymbolName, string pchValue)
        {
            int iValueSize;
            int iStatementSize = CalcDataSizeOfStringAssignmentStatement(pchSymbolName, pchValue);
            iOffset = WriteIntDataToMemory(pbyteOut, iOffset, iSize, iStatementSize);
            iOffset = WriteUnsignedCharDataToMemory(pbyteOut, iOffset, iSize, 1);
            int iSymbolSize = pchSymbolName.Length;
            iOffset = WriteUnsignedCharDataToMemory(pbyteOut, iOffset, iSize, iSymbolSize);
            iOffset = WriteStringDataToMemory(pbyteOut, iOffset, iSize, pchSymbolName);
            if (pchValue != null)
                iValueSize = pchValue.Length;
            else
                iValueSize = 0;
            iOffset = WriteUnsignedShortDataToMemory(pbyteOut, iOffset, iSize, iValueSize);
            iOffset = WriteStringDataToMemory(pbyteOut, iOffset, iSize, pchValue);
            return iOffset;
        }
        int WriteUnsignedShortDataToMemory(char[] pbyteOut, int iOffset, int iSize, int iValue)
        {
            if (iOffset < iSize)
                pbyteOut[iOffset++] = Convert.ToChar((iValue >> 0) & 0xFF);
            if (iOffset < iSize)
                pbyteOut[iOffset++] = Convert.ToChar((iValue >> 8) & 0xFF);
            return iOffset;
        }
        public int WriteShortDataToMemory(char[] pbyteOut, int iOffset, int iSize, int iValue)
        {
            if (iOffset < iSize)
                pbyteOut[iOffset++] = Convert.ToChar((iValue >> 0) & 0xFF);
            if (iOffset < iSize)
                pbyteOut[iOffset++] = Convert.ToChar((iValue >> 8) & 0xFF);
            return iOffset;
        }
        public int WriteStringDataToMemory(char[] pbyteOut, int iOffset, int iSize, string pchValue)
        {

            int i = 0;

            if (pchValue != null)
            {
                while (i < pchValue.Length)
                {

                    if (iOffset < iSize)
                        pbyteOut[iOffset++] = pchValue[i++];
                }
            }
            return iOffset;
        }
        public int WriteUnsignedCharDataToMemory(char[] pbyteOut, int iOffset, int iSize, int iValue)
        {
            if (iOffset < iSize)
                pbyteOut[iOffset++] = Convert.ToChar((iValue >> 0) & 0xFF);
            return iOffset;
        }
        public int WriteIntDataToMemory(char[] pbyteOut, int iOffset, int iSize, int iValue)
        {
            if (iOffset < iSize)
                pbyteOut[iOffset++] = Convert.ToChar((iValue >> 0) & 0xFF);
            if (iOffset < iSize)
                pbyteOut[iOffset++] = Convert.ToChar((iValue >> 8) & 0xFF);
            if (iOffset < iSize)
                pbyteOut[iOffset++] = Convert.ToChar((iValue >> 16) & 0xFF);
            if (iOffset < iSize)
                pbyteOut[iOffset++] = Convert.ToChar((iValue >> 24) & 0xFF);
            return iOffset;
        }



        public int WriteStatementBlock(char[] pbyteOut, int iOffset, int iSize)
        {
            string strTmp = string.Empty;
            string strBirthYear = string.Empty;
            string strBirthMonth = string.Empty;
            string strBirthDay = string.Empty;


            if (iOffset < iSize)
            {
                //int iBlockSize = CalcDataSizeOfStatementBlock();
                //iOffset += WriteIntDataToMemory( pbyteOut + iOffset, iBlockSize );
                iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserID", GetID());
            }

            if (iOffset < iSize)
                iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserName", GetName());

            if (iOffset < iSize)
            {

                GetDoB(ref strBirthYear, ref strBirthMonth, ref strBirthDay);
                iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserBirthYear", strBirthYear);
            }

            if (iOffset < iSize)
                iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserBirthMonth", strBirthMonth);

            if (iOffset < iSize)
                iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserBirthDay", strBirthDay);

            if (iOffset < iSize)
            {
                //strTmp.Format( "%d", GetSex() );
                iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserSex", GetGender().ToString());
            }

            if (iOffset < iSize)
            {
                //strTmp.Format("%.1f", GetHeight());
                iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "UserHeight", GetHeight().ToString());
            }

            if (iOffset < iSize)
            {
                iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "MeasurementTime", GetMeasurementTime());
            }

            if (iOffset < iSize)
            {
                iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "Comment", GetComment());
            }

            if (iOffset < iSize)
            {
                //strTmp.Format("%d", GetBenchmarkDistance());
                iOffset = WriteStringAssignmentStatement(pbyteOut, iOffset, iSize, "BenchmarkDistance", GetBenchmarkDistance().ToString());
            }

            if (iOffset < iSize)
            {

                iOffset = m_FrontBodyPositionStanding.WriteStandingStatementBlock(pbyteOut, iOffset, iSize);
            }

            if (iOffset < iSize)
            {
                iOffset = m_FrontBodyPositionKneedown.WriteKneedownStatementBlock(pbyteOut, iOffset, iSize);
            }

            if (iOffset < iSize)
            {
                //GetSideBodyPosition(ref m_SideBodyPosition);
                iOffset = m_SideBodyPosition.WriteStatementBlock(pbyteOut, iOffset, iSize);
            }


            return iOffset;

        }
        public int WriteStatementBlock(char[] pbyteOut, int iOffset, int iSize,
           JointEditDoc jointEditDoc)
        {

            iOffset = WriteImageAssignmentStatement
             (pbyteOut, iOffset, iSize, "StandingImage",
             Compress(jointEditDoc.m_FrontStandingImageBytes,true));

            iOffset = WriteImageAssignmentStatement(pbyteOut,
                iOffset, iSize, "KneedownImage",
                Compress(jointEditDoc.m_FrontKneedownImageBytes, true));

            iOffset = WriteImageAssignmentStatement(pbyteOut,
                iOffset, iSize, "SideImage",
                Compress(jointEditDoc.m_SideImageBytes, true));
            return iOffset;
        }

        int WriteImageAssignmentStatement(char[] pbyteOut, int iOffset, int iSize,
            string pchSymbolName, byte[] pbyteImage)
        {
            int iStatementSize = CalcDataSizeOfImageAssignmentStatement(pchSymbolName,
                pbyteImage.Length);
            iOffset = WriteIntDataToMemory(pbyteOut, iOffset, iSize, iStatementSize);
            iOffset = WriteUnsignedCharDataToMemory(pbyteOut, iOffset, iSize, 3);
            int iSymbolSize = pchSymbolName.Length;
            iOffset = WriteUnsignedCharDataToMemory(pbyteOut, iOffset, iSize, iSymbolSize);
            iOffset = WriteStringDataToMemory(pbyteOut, iOffset, iSize, pchSymbolName);
            iOffset = WriteIntDataToMemory(pbyteOut, iOffset, iSize, pbyteImage.Length);
            iOffset = WriteImageDataToMemory(pbyteOut, iOffset, iSize, pbyteImage, pbyteImage.Length);
            return iOffset;
        }
        int WriteImageDataToMemory(char[] pbyteOut, int iOffset, int iSize,
            byte[] pbyteImage, int iImageSize)
        {

            int i = 0;
            for (i = 0; i < iImageSize; i++)
            {
                //if (iOffset < iSize) return iOffset;
                pbyteOut[iOffset++] = Convert.ToChar(pbyteImage[i]);
            }
            return iOffset;
        }
        public byte[] Compress(byte[] data,bool SwapFlag)
        {
            var jpegQuality = 50;

            Byte[] outputBytes;

            Image<Bgr, byte> test = new Image<Bgr, byte>(1024, 1280);
            test.Bytes = data;
            Bitmap test1 = test.ToBitmap();
            if(SwapFlag)
                test1 = SwapRedandBlueChannels(test1);
            var jpegEncoder = ImageCodecInfo.GetImageDecoders()
              .First(c => c.FormatID == ImageFormat.Jpeg.Guid);
            var encoderParameters = new EncoderParameters(1);
            encoderParameters.Param[0] = new EncoderParameter
                (System.Drawing.Imaging.Encoder.Quality, jpegQuality);

            using (var outputStream = new MemoryStream())
            {
                test1.Save(outputStream, jpegEncoder, encoderParameters);
                outputBytes = outputStream.ToArray();
            }

            return outputBytes;
        }
        private Bitmap SwapRedandBlueChannels(Bitmap OriginalImage)
        {
            var imageAttr = new ImageAttributes();
            imageAttr.SetColorMatrix(new ColorMatrix(
                                         new[]
                                             {
                                                 new[] {0.0F, 0.0F, 1.0F, 0.0F, 0.0F},
                                                 new[] {0.0F, 1.0F, 0.0F, 0.0F, 0.0F},
                                                 new[] {1.0F, 0.0F, 0.0F, 0.0F, 0.0F},
                                                 new[] {0.0F, 0.0F, 0.0F, 1.0F, 0.0F},
                                                 new[] {0.0F, 0.0F, 0.0F, 0.0F, 1.0F}
                                             }
                                         ));
            var temp = new Bitmap(OriginalImage.Width, OriginalImage.Height);
            GraphicsUnit pixel = GraphicsUnit.Pixel;
            using (Graphics g = Graphics.FromImage(temp))
            {
                g.DrawImage(OriginalImage, Rectangle.Round(OriginalImage.GetBounds(ref pixel)),
                    0, 0, OriginalImage.Width, OriginalImage.Height,
                            GraphicsUnit.Pixel, imageAttr);
            }
            return temp;
        }
        /* int CalcDataSizeOfImageAssignmentStatement(string pchSymbolName, int iImageSize)
         {

             int iRet = 0;
             //iRet += sizeof( int );			// Statement‘S‘Ì‚ÌƒTƒCƒY.
             iRet += sizeof(char); // ƒIƒyƒR[ƒh‚h‚c.
             iRet += sizeof(char); // Symbol‚Ì•¶Žš—ñ’·‚³.
             iRet += pchSymbolName.Length; // Symbol‚ÌŽÀ‘Ì.
             iRet += sizeof(int);                // ‰æ‘œƒTƒCƒY.
             iRet += iImageSize;                 // ‰æ‘œ‚ÌŽÀ‘Ì.
             return iRet;
         }*/
        /*
        int CData::ReadStatementBlock( const unsigned char* pbyteIn)
        {
            int iBlockSize = 0;
            int iOffset = 0;
            iOffset += ReadIntDataFromMemory(pbyteIn + iOffset, iBlockSize);
            while (iOffset < iBlockSize)
            {
                int iStatementSize = 0;
                iOffset += ReadIntDataFromMemory(pbyteIn + iOffset, iStatementSize);
                int iOpeCode = 0;
                iOffset += ReadUnsignedCharDataFromMemory(pbyteIn + iOffset, iOpeCode);
                int iSymbolSize = 0;
                iOffset += ReadUnsignedCharDataFromMemory(pbyteIn + iOffset, iSymbolSize);
                const char* pchSymbol = ( const char* )(pbyteIn + iOffset);
                iOffset += iSymbolSize;
                if (iOpeCode == 1)
                {
                    // StringAssignmentStatement.
                    int iValueSize = 0;
                    iOffset += ReadUnsignedShortDataFromMemory(pbyteIn + iOffset, iValueSize);
                    const char* pchValue = ( const char* )(pbyteIn + iOffset);
                    iOffset += iValueSize;
                    ExecuteStringAssignmentStatement(pchSymbol, iSymbolSize, pchValue, iValueSize);
                }
                else if (iOpeCode == 2)
                {
                    // ShortAssignmentStatement.
                    int iValue = 0;
                    iOffset += ReadShortDataFromMemory(pbyteIn + iOffset, iValue);
                    ExecuteIntegerAssignmentStatement(pchSymbol, iSymbolSize, iValue);
                }
                else if (iOpeCode == 3)
                {
                    // ImageAssignmentStatement.
                    int iImageSize = 0;
                    iOffset += ReadIntDataFromMemory(pbyteIn + iOffset, iImageSize);
                    const unsigned char* pbyteImage = pbyteIn + iOffset;
                    iOffset += iImageSize;
                    ExecuteImageAssignmentStatement(pchSymbol, iSymbolSize, pbyteImage, iImageSize);
                }
            }
            return iOffset;
        }
    */
        public int ExecuteIntegerAssignmentStatement(string pchSymbolName, int iSymbolNameLength, int iValue)
        {
            if (m_FrontBodyPositionStanding.EXecuteStandingIntegerAssignmentStatement(pchSymbolName, iSymbolNameLength, iValue) == 1)
            {
                return 1;
            }
            if (m_FrontBodyPositionKneedown.EXecuteKneedownIntegerAssignmentStatement(pchSymbolName, iSymbolNameLength, iValue) == 1)
            {
                return 1;
            }
            if (m_SideBodyPosition.ExecuteIntegerAssignmentStatement(pchSymbolName, iSymbolNameLength, iValue) == 1)
            {
                return 1;
            }
            return 0;
        }

        int CompareSymbol(string pchSymbol1, int iSymbol1Length, string pchSymbol2)
        {

            int i = 0;
            for (i = 0; i < iSymbol1Length; i++)
            {
                if (pchSymbol1[i] != pchSymbol2[i])
                {
                    return 0;
                }
                if (pchSymbol2[i] == '\0')
                {
                    return 0;
                }
            }
            if (pchSymbol2[i] != '\0')
            {
                return 0;
            }
            return 1;
        }
        void AssignToString(ref string strDest, string pchValue, int iValueLength)
        {

            char[] pchDest = strDest.ToCharArray();
            int i = 0;
            for (i = 0; i < iValueLength; i++)
            {
                pchDest[i] = pchValue[i];
            }
            pchDest[i] = '\0';

        }
        public int ExecuteStringAssignmentStatement(string pchSymbolName, int iSymbolNameLength, string pchValue, int iValueLength)
        {

            if (pchSymbolName == "UserID")
            {
                // AssignToString(ref m_ID, pchValue, iValueLength);
                m_ID = pchValue;
                return 1;
            }
            if (pchSymbolName == "UserName")
            {
                //AssignToString(ref m_Name, pchValue, iValueLength);
                m_Name = pchValue;
                //Added by Sumit For GSP-1341   ---START
                if(UtilityGlobal.YGAFilePathForName.Length>0)
                {
                    m_Name = UtilityGlobal.GetPatientName(UtilityGlobal.YGAFilePathForName);
                }
                //Added by Sumit For GSP-1341   ---END
                return 1;
            }
            if (pchSymbolName == "UserBirthYear")
            {
                //string strTmp = string.Empty;
                //AssignToString(ref strTmp, pchValue, iValueLength);
                m_iBirthYear = Convert.ToInt32(pchValue);
                return 1;
            }
            if (pchSymbolName == "UserBirthMonth")
            {
                //string strTmp = string.Empty;
                //AssignToString(ref strTmp, pchValue, iValueLength);
                m_iBirthMonth = Convert.ToInt32(pchValue);
                return 1;
            }
            if (pchSymbolName == "UserBirthDay")
            {
                //string strTmp = string.Empty;
                //AssignToString(ref strTmp, pchValue, iValueLength);
                m_iBirthDay = Convert.ToInt32(pchValue);
                return 1;
            }
            if (pchSymbolName == "UserSex")
            {
                //string strTmp = string.Empty;
                //AssignToString(ref strTmp, pchValue, iValueLength);
                m_Gender = Convert.ToInt32(pchValue);
                return 1;
            }
            if (pchSymbolName == "UserHeight")
            {
                //string strTmp = string.Empty;
                //AssignToString(ref strTmp, pchValue, iValueLength);
                m_Height = Convert.ToDouble(pchValue);
                return 1;
            }
            if (pchSymbolName == "MeasurementTime")
            {
                //AssignToString(ref m_Time, pchValue, iValueLength);
                m_Time = pchValue;
                return 1;
            }
            if (pchSymbolName == "Comment")
            {
                //AssignToString(ref m_Comment, pchValue, iValueLength);
                m_Comment = pchValue;
                return 1;
            }
            if (pchSymbolName == "BenchmarkDistance")
            {
                //string strTmp = string.Empty;
                //AssignToString(ref strTmp, pchValue, iValueLength);
                m_iBenchmarkDistance = Convert.ToInt32(pchValue);
                return 1;
            }
            return 0;
        }

        public int ExecuteImageAssignmentStatement(string pchSymbolName, int iSymbolNameLength, string pbyteImage, int iImageSize)
        {
            return 0;
        }

    }
}
