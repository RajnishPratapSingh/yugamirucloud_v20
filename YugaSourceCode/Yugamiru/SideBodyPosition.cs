﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Yugamiru
{
    public class SideBodyPosition
    {
        public int POSTUREPATTERNID_NONE = 0;
        public int POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD = 1;   /* ”L”w{”½‚è˜ */
        public int POSTUREPATTERNID_STOOP = 2; /* ”L”w */
        public int POSTUREPATTERNID_BEND_BACKWARD = 3;/* ”½‚è˜ */
        public int POSTUREPATTERNID_FLATBACK = 4; /* ƒtƒ‰ƒbƒgƒoƒbƒN */
        public int POSTUREPATTERNID_NORMAL = 5; /* —‘z‚ÌŽp¨ */
        public Point m_ptHip = new Point(0, 0);

        public Point m_ptKnee = new Point(0, 0);

        public Point m_ptAnkle = new Point(0, 0);

        public Point m_ptRightBelt = new Point(0, 0);

        public Point m_ptLeftBelt = new Point(0, 0);

        public Point m_ptShoulder = new Point(0, 0);

        public Point m_ptEar = new Point(0, 0);

        public Point m_ptChin = new Point(0, 0);

        public Point m_ptGlabella = new Point(0, 0);

        public Point m_ptBenchmark1 = new Point(0, 0);

        public Point m_ptBenchmark2 = new Point(0, 0);

        public Point m_ptKneeRightBelt = new Point(0, 0);

        public Point m_ptKneeLeftBelt = new Point(0, 0);

        public Point m_ptAnkleRightBelt = new Point(0, 0);

        public Point m_ptAnkleLeftBelt = new Point(0, 0);

      


        public SideBodyPosition()
        {

        }

        public SideBodyPosition(SideBodyPosition rSrc)
        {
         
            m_ptHip = rSrc.m_ptHip;

            m_ptKnee = rSrc.m_ptKnee;

            m_ptAnkle = rSrc.m_ptAnkle;

            m_ptRightBelt = rSrc.m_ptRightBelt;

            m_ptLeftBelt = rSrc.m_ptLeftBelt;

            m_ptShoulder = rSrc.m_ptShoulder;

            m_ptEar = rSrc.m_ptEar;

            m_ptChin = rSrc.m_ptChin;

            m_ptGlabella = rSrc.m_ptGlabella;

            m_ptBenchmark1 = rSrc.m_ptBenchmark1;

            m_ptBenchmark2 = rSrc.m_ptBenchmark2;

        }



        /*
                        CSideBodyPosition::~CSideBodyPosition( void )
                {
                }

                CSideBodyPosition &CSideBodyPosition::operator=( const CSideBodyPosition &rSrc )
                {
                    m_ptHip			= rSrc.m_ptHip;
                    m_ptKnee		= rSrc.m_ptKnee;
                    m_ptAnkle		= rSrc.m_ptAnkle;
                    m_ptRightBelt	= rSrc.m_ptRightBelt;
                    m_ptLeftBelt	= rSrc.m_ptLeftBelt;
                    m_ptShoulder	= rSrc.m_ptShoulder;
                    m_ptEar			= rSrc.m_ptEar;
                    m_ptChin		= rSrc.m_ptChin;
                    m_ptGlabella	= rSrc.m_ptGlabella;
                    m_ptBenchmark1	= rSrc.m_ptBenchmark1;
                    m_ptBenchmark2	= rSrc.m_ptBenchmark2;
                    return *this;
                }*/

        public void Init(int width, int height, CCalibInf CalibInf, CCalcPostureProp CalcPostureProp)
        {
            int middleX = (int)CalibInf.GetFPLcenterX();
            int beltzoffset = (int)(width * 0.05);
            int kneezoffset = (int)(width * 0.02);

            m_ptRightBelt.X = middleX - beltzoffset;
            m_ptLeftBelt.X = middleX + beltzoffset;
            m_ptAnkle.X = middleX;
            m_ptHip.X = middleX;
            m_ptKnee.X = middleX + kneezoffset;

            m_ptAnkle.Y = height - (int)CalibInf.GetRightAnkleposY();
            m_ptHip.Y = m_ptAnkle.Y - m_ptAnkle.Y * 38 / 100;
            m_ptKnee.Y = m_ptAnkle.Y - (m_ptAnkle.Y - m_ptHip.Y) * 4 / 10;
            // compute BELT coordinates from hips coordinates
            int legLength = Math.Abs(m_ptHip.Y - m_ptAnkle.Y);
            m_ptRightBelt.Y = (int)(m_ptHip.Y - legLength * CalcPostureProp.GetBelt2HipRatio());
            m_ptLeftBelt.Y = m_ptRightBelt.Y;

            m_ptBenchmark1.X = m_ptAnkle.X - beltzoffset;
            m_ptBenchmark1.Y = m_ptAnkle.Y;
            m_ptBenchmark2.X = m_ptAnkle.X + beltzoffset;
            m_ptBenchmark2.Y = m_ptAnkle.Y;

            m_ptKneeRightBelt.X = m_ptKnee.X - beltzoffset / 2;
            m_ptKneeRightBelt.Y = m_ptKnee.Y;
            m_ptKneeLeftBelt.X = m_ptKnee.X + beltzoffset / 2;
            m_ptKneeLeftBelt.Y = m_ptKnee.Y;
            m_ptAnkleRightBelt.X = m_ptAnkle.X - beltzoffset / 2;
            m_ptAnkleRightBelt.Y = m_ptAnkle.Y;
            m_ptAnkleLeftBelt.X = m_ptAnkle.X + beltzoffset / 2;
            m_ptAnkleLeftBelt.Y = m_ptAnkle.Y;

            Adjust(width, height);
        }

        public bool InitUpperPostion(int width, int height, CCalibInf CalibInf)
        {
            m_ptShoulder.X = m_ptHip.X;
            m_ptEar.X = m_ptHip.X;
            m_ptChin.X = m_ptHip.X;
            m_ptGlabella.X = m_ptHip.X;
            int leglength = Math.Abs(m_ptHip.Y - m_ptAnkle.Y);
            m_ptShoulder.Y = (int)((m_ptHip.Y - (int)(leglength * 0.75)));
            m_ptEar.Y = (int)((m_ptHip.Y - (int)(leglength * 1.0)));
            m_ptChin.Y = (int)((m_ptHip.Y - (int)(leglength * 0.9)));
            m_ptGlabella.Y = (int)((m_ptHip.Y - (int)(leglength * 1.1)));

            Adjust(width, height);
            return true;
        }

        public void Adjust(int width, int height)
        {
            if (m_ptHip.X < 0)
            {
                m_ptHip.X = 0;
            }
            if (m_ptHip.X >= width)
            {
                m_ptHip.X = width - 1;
            }
            if (m_ptHip.Y < 0)
            {
                m_ptHip.Y = 0;
            }
            if (m_ptHip.Y >= height)
            {
                m_ptHip.Y = height - 1;
            }
            if (m_ptKnee.X < 0)
            {
                m_ptKnee.X = 0;
            }
            if (m_ptKnee.X >= width)
            {
                m_ptKnee.X = width - 1;
            }
            if (m_ptKnee.Y < 0)
            {
                m_ptKnee.Y = 0;
            }
            if (m_ptKnee.Y >= height)
            {
                m_ptKnee.Y = height - 1;
            }
            if (m_ptAnkle.X < 0)
            {
                m_ptAnkle.X = 0;
            }
            if (m_ptAnkle.X >= width)
            {
                m_ptAnkle.X = width - 1;
            }
            if (m_ptAnkle.Y < 0)
            {
                m_ptAnkle.Y = 0;
            }
            if (m_ptAnkle.Y >= height)
            {
                m_ptAnkle.Y = height - 1;
            }
            if (m_ptRightBelt.X < 0)
            {
                m_ptRightBelt.X = 0;
            }
            if (m_ptRightBelt.X >= width)
            {
                m_ptRightBelt.X = width - 1;
            }
            if (m_ptRightBelt.Y < 0)
            {
                m_ptRightBelt.Y = 0;
            }
            if (m_ptRightBelt.Y >= height)
            {
                m_ptRightBelt.Y = height - 1;
            }
            if (m_ptLeftBelt.X < 0)
            {
                m_ptLeftBelt.X = 0;
            }
            if (m_ptLeftBelt.X >= width)
            {
                m_ptLeftBelt.X = width - 1;
            }
            if (m_ptLeftBelt.Y < 0)
            {
                m_ptLeftBelt.Y = 0;
            }
            if (m_ptLeftBelt.Y >= height)
            {
                m_ptLeftBelt.Y = height - 1;
            }
            if (m_ptShoulder.X < 0)
            {
                m_ptShoulder.X = 0;
            }
            if (m_ptShoulder.X >= width)
            {
                m_ptShoulder.X = width - 1;
            }
            if (m_ptShoulder.Y < 0)
            {
                m_ptShoulder.Y = 0;
            }
            if (m_ptShoulder.Y >= height)
            {
                m_ptShoulder.Y = height - 1;
            }
            if (m_ptEar.X < 0)
            {
                m_ptEar.X = 0;
            }
            if (m_ptEar.X >= width)
            {
                m_ptEar.X = width - 1;
            }
            if (m_ptEar.Y < 0)
            {
                m_ptEar.Y = 0;
            }
            if (m_ptEar.Y >= height)
            {
                m_ptEar.Y = height - 1;
            }
            if (m_ptChin.X < 0)
            {
                m_ptChin.X = 0;
            }
            if (m_ptChin.X >= width)
            {
                m_ptChin.X = width - 1;
            }
            if (m_ptChin.Y < 0)
            {
                m_ptChin.Y = 0;
            }
            if (m_ptChin.Y >= height)
            {
                m_ptChin.Y = height - 1;
            }
            if (m_ptGlabella.X < 0)
            {
                m_ptGlabella.X = 0;
            }
            if (m_ptGlabella.X >= width)
            {
                m_ptGlabella.X = width - 1;
            }
            if (m_ptGlabella.Y < 0)
            {
                m_ptGlabella.Y = 0;
            }
            if (m_ptGlabella.Y >= height)
            {
                m_ptGlabella.Y = height - 1;
            }
            if (m_ptBenchmark1.X < 0)
            {
                m_ptBenchmark1.X = 0;
            }
            if (m_ptBenchmark1.X >= width)
            {
                m_ptBenchmark1.X = width - 1;
            }
            if (m_ptBenchmark1.Y < 0)
            {
                m_ptBenchmark1.Y = 0;
            }
            if (m_ptBenchmark1.Y >= height)
            {
                m_ptBenchmark1.Y = height - 1;
            }
            if (m_ptBenchmark2.X < 0)
            {
                m_ptBenchmark2.X = 0;
            }
            if (m_ptBenchmark2.X >= width)
            {
                m_ptBenchmark2.X = width - 1;
            }
            if (m_ptBenchmark2.Y < 0)
            {
                m_ptBenchmark2.Y = 0;
            }
            if (m_ptBenchmark2.Y >= height)
            {
                m_ptBenchmark2.Y = height - 1;
            }
        }

        // ‘¤–Ê‚ÌˆÊ’u‚É‘Î‚µ‚Ä‚Ì‚Ý—LŒø.
        public int CalcPosturePattern(
                int stoop_min_angle,
                int flatback_max_angle,
                int bend_backward_min_angle,
                double dCameraAngle)
        {
            int shoulder_ear_angle = (int)(Math.Atan2((double)(m_ptEar.X - m_ptShoulder.X),
                                                 (double)-(m_ptEar.Y - m_ptShoulder.Y)) * 180.0 / 3.141592 - dCameraAngle);

            int pelvic_angle = (int)(Math.Atan2((double)-(m_ptLeftBelt.Y - m_ptRightBelt.Y),
                                             (double)(m_ptLeftBelt.X - m_ptRightBelt.X)) * 180 / 3.141592 - dCameraAngle);

            if ((shoulder_ear_angle >= stoop_min_angle) && (pelvic_angle >= bend_backward_min_angle))
            {
                /* ”L”w{”½‚è˜ */
                return POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD;
            }
            if (shoulder_ear_angle >= stoop_min_angle)
            {
                /* ”L”w */
                return POSTUREPATTERNID_STOOP;
            }
            if (pelvic_angle >= bend_backward_min_angle)
            {
                /* ”½‚è˜ */
                return POSTUREPATTERNID_BEND_BACKWARD;
            }
            if (pelvic_angle <= flatback_max_angle)
            {
                /* ƒtƒ‰ƒbƒgƒoƒbƒN */
                return POSTUREPATTERNID_FLATBACK;
            }
            /* —‘z‚ÌŽp¨ */
            return POSTUREPATTERNID_NORMAL;
        }

        public int GetHipY()
        {
            return m_ptHip.Y;
        }

        public int GetHipX()
        {
            return m_ptHip.X;
        }

        public int GetKneeY()
        {
            return m_ptKnee.Y;
        }

        public int GetKneeX()
        {
            return m_ptKnee.X;
        }

        public int GetAnkleY()
        {
            return m_ptAnkle.Y;
        }

        public int GetAnkleX()
        {
            return m_ptAnkle.X;
        }

        public int GetRightBeltY()
        {
            return m_ptRightBelt.Y;
        }

        public int GetRightBeltX()
        {
            return m_ptRightBelt.X;
        }

        public int GetLeftBeltY()
        {
            return m_ptLeftBelt.Y;
        }

        public int GetLeftBeltX()
        {
            return m_ptLeftBelt.X;
        }

        public int GetShoulderY()
        {
            return m_ptShoulder.Y;
        }

        public int GetShoulderX()
        {
            return m_ptShoulder.X;
        }

        public int GetEarY()
        {
            return m_ptEar.Y;
        }

        public int GetEarX()
        {
            return m_ptEar.X;
        }

        public int GetChinY()
        {
            return m_ptChin.Y;
        }

        public int GetChinX()
        {
            return m_ptChin.X;
        }

        public int GetGlabellaY()
        {
            return m_ptGlabella.Y;
        }

        public int GetGlabellaX()
        {
            return m_ptGlabella.X;
        }

        public int GetBenchmark1X()
        {
            return m_ptBenchmark1.X;
        }

        public int GetBenchmark1Y()
        {
            return m_ptBenchmark1.Y;
        }

        public int GetBenchmark2X()
        {
            return m_ptBenchmark2.X;
        }

        public int GetBenchmark2Y()
        {
            return m_ptBenchmark2.Y;
        }

        public void GetHipPosition(ref Point ptPos)
        {
            ptPos = m_ptHip;
        }

        public void GetKneePosition(ref Point ptPos)
        {
            ptPos = m_ptKnee;
        }

        public void GetAnklePosition(ref Point ptPos)
        {
            ptPos = m_ptAnkle;
        }

        public void GetRightBeltPosition(ref Point ptPos)
        {
            ptPos = m_ptRightBelt;
        }

        public void GetLeftBeltPosition(ref Point ptPos)
        {
            ptPos = m_ptLeftBelt;
        }

        public void GetShoulderPosition(ref Point ptPos)
        {
            ptPos = m_ptShoulder;
        }

        public void GetEarPosition(ref Point ptPos)
        {
            ptPos = m_ptEar;
        }

        public void GetChinPosition(ref Point ptPos)
        {
            ptPos = m_ptChin;
        }

        public void GetGlabellaPosition(ref Point ptPos)
        {
            ptPos = m_ptGlabella;
        }

        public void GetBenchmark1Position(ref Point ptPos)
        {
            ptPos = m_ptBenchmark1;
        }

        public void GetBenchmark2Position(ref Point ptPos)
        {
            ptPos = m_ptBenchmark2;
        }
        public void GetKneeRightBeltPosition(Point ptPos)
        {

            ptPos = m_ptKneeRightBelt;
        }

        public void GetKneeLeftBeltPosition(Point ptPos)
        {
            ptPos = m_ptKneeLeftBelt;
        }

        public void GetAnkleRightBeltPosition(Point ptPos)
        {
            ptPos = m_ptAnkleRightBelt;
        }
        public void GetAnkleLeftBeltPosition(Point ptPos)
        {

            ptPos = m_ptAnkleLeftBelt;
        }

        public void SetHipPosition(Point ptPos)
        {
            m_ptHip = ptPos;
        }

        public void SetKneePosition(Point ptPos)
        {
            m_ptKnee = ptPos;
        }

        public void SetAnklePosition(Point ptPos)
        {
            m_ptAnkle = ptPos;
        }

        public void SetRightBeltPosition(Point ptPos)
        {
            m_ptRightBelt = ptPos;
        }

        public void SetLeftBeltPosition(Point ptPos)
        {
            m_ptLeftBelt = ptPos;
        }

        public void SetShoulderPosition(Point ptPos)
        {
            m_ptShoulder = ptPos;
        }

        public void SetEarPosition(Point ptPos)
        {
            m_ptEar = ptPos;
        }

        public void SetChinPosition(Point ptPos)
        {
            m_ptChin = ptPos;
        }

        public void SetGlabellaPosition(Point ptPos)
        {
            m_ptGlabella = ptPos;
        }

        public void SetBenchmark1Position(Point ptPos)
        {
            m_ptBenchmark1 = ptPos;
        }

        public void SetBenchmark2Position(Point ptPos)
        {
            m_ptBenchmark2 = ptPos;
        }
        public void SetKneeRightBeltPosition(Point ptPos)
        {
            m_ptKneeRightBelt = ptPos;
        }

        public void SetKneeLeftBeltPosition(Point ptPos)
        {
            m_ptKneeLeftBelt = ptPos;
        }

        public void SetAnkleRightBeltPosition(Point ptPos)
        {
            m_ptAnkleRightBelt = ptPos;
        }

        public void SetAnkleLeftBeltPosition(Point ptPos)
        {
            m_ptAnkleLeftBelt = ptPos;
        }
       
        int CalcDataSizeOfShortAssignmentStatement(string pchSymbolName)
        {

            int iRet = 0;
            //iRet += sizeof( int );			// Statement‘S‘Ì‚ÌƒTƒCƒY.
            iRet += sizeof(char);   // ƒIƒyƒR[ƒh‚h‚c.
            iRet += sizeof(char);   // Symbol‚Ì•¶Žš—ñ’·‚³.
            iRet += pchSymbolName.Length; // Symbol‚ÌŽÀ‘Ì.
            iRet += sizeof(ushort); // ’l‚ÌŽÀ‘Ì.
            return iRet;
        }

        public int CalcDataSizeOfStatementBlock()
        {
            int iRet = 0;
            iRet += CalcDataSizeOfShortAssignmentStatement("SideHipX") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideHipY") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideKneeX") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideKneeY") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideAnkleX") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideAnkleY") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideRightBeltX") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideRightBeltY") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideLeftBeltX") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideLeftBeltY") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideShoulderX") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideShoulderY") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideEarX") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideEarY") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideChinX") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideChinY") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideGlabellaX") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideGlabellaY") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideBenchmark1X") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideBenchmark1Y") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideBenchmark2X") + sizeof(int);
            iRet += CalcDataSizeOfShortAssignmentStatement("SideBenchmark2Y") + sizeof(int);
            return iRet;
        }

        public int WriteStatementBlock(char[] pbyteOut, int iOffset, int iSize)
        {
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideHipX", GetHipX());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideHipY", GetHipY());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideKneeX", GetKneeX());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideKneeY", GetKneeY());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideAnkleX", GetAnkleX());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideAnkleY", GetAnkleY());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideRightBeltX", GetRightBeltX());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideRightBeltY", GetRightBeltY());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideLeftBeltX", GetLeftBeltX());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideLeftBeltY", GetLeftBeltY());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideShoulderX", GetShoulderX());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideShoulderY", GetShoulderY());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideEarX", GetEarX());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideEarY", GetEarY());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideChinX", GetChinX());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideChinY", GetChinY());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideGlabellaX", GetGlabellaX());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideGlabellaY", GetGlabellaY());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideBenchmark1X", GetBenchmark1X());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideBenchmark1Y", GetBenchmark1Y());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideBenchmark2X", GetBenchmark2X());
            iOffset = WriteShortAssignmentStatement(pbyteOut, iOffset, iSize, "SideBenchmark2Y", GetBenchmark2Y());
            return iOffset;
        }
        int WriteShortAssignmentStatement(char[] pbyteOut, int iOffset, int iSize, string pchSymbolName, int iValue)
        {
            MyData temp_data = new MyData();
            int iStatementSize = CalcDataSizeOfShortAssignmentStatement(pchSymbolName);
            iOffset = temp_data.WriteIntDataToMemory(pbyteOut, iOffset, iSize, iStatementSize);
            iOffset = temp_data.WriteUnsignedCharDataToMemory(pbyteOut, iOffset, iSize, 2);
            int iSymbolSize = pchSymbolName.Length;
            iOffset = temp_data.WriteUnsignedCharDataToMemory(pbyteOut, iOffset, iSize, iSymbolSize);
            iOffset = temp_data.WriteStringDataToMemory(pbyteOut, iOffset, iSize, pchSymbolName);
            iOffset = temp_data.WriteShortDataToMemory(pbyteOut, iOffset, iSize, iValue);
            return iOffset;
        }

        int CompareSymbol(string pchSymbol1, int iSymbol1Length, string pchSymbol2)
        {

            int i = 0;
            for (i = 0; i < iSymbol1Length; i++)
            {
                if (pchSymbol1[i] != pchSymbol2[i])
                {
                    return 0;
                }
                if (pchSymbol2[i] == '\0')
                {
                    return 0;
                }
            }
            if (pchSymbol2[i] != '\0')
            {
                return 0;
            }
            return 1;
        }
        public int ExecuteIntegerAssignmentStatement(string pchSymbolName, int iSymbolNameLength, int iValue)
        {
            if (pchSymbolName == "SideHipX")
            {
                m_ptHip.X = iValue;
                return 1;
            }
            if (pchSymbolName == "SideHipY")
            {
                m_ptHip.Y = iValue;
                return 1;
            }
            if (pchSymbolName == "SideKneeX")
            {
                m_ptKnee.X = iValue;
                return 1;
            }
            if (pchSymbolName == "SideKneeY")
            {
                m_ptKnee.Y = iValue;
                return 1;
            }
            if (pchSymbolName == "SideAnkleX")
            {
                m_ptAnkle.X = iValue;
                return 1;
            }
            if (pchSymbolName == "SideAnkleY")
            {
                m_ptAnkle.Y = iValue;
                return 1;
            }
            if (pchSymbolName == "SideRightBeltX")
            {
                m_ptRightBelt.X = iValue;
                return 1;
            }
            if (pchSymbolName == "SideRightBeltY")
            {
                m_ptRightBelt.Y = iValue;
                return 1;
            }
            if (pchSymbolName == "SideLeftBeltX")
            {
                m_ptLeftBelt.X = iValue;
                return 1;
            }
            if (pchSymbolName == "SideLeftBeltY")
            {
                m_ptLeftBelt.Y = iValue;
                return 1;
            }
            if (pchSymbolName == "SideShoulderX")
            {
                m_ptShoulder.X = iValue;
                return 1;
            }
            if (pchSymbolName == "SideShoulderY")
            {
                m_ptShoulder.Y = iValue;
                return 1;
            }
            if (pchSymbolName == "SideEarX")
            {
                m_ptEar.X = iValue;
                return 1;
            }
            if (pchSymbolName == "SideEarY")
            {
                m_ptEar.Y = iValue;
                return 1;
            }
            if (pchSymbolName == "SideChinX")
            {
                m_ptChin.X = iValue;
                return 1;
            }
            if (pchSymbolName == "SideChinY")
            {
                m_ptChin.Y = iValue;
                return 1;
            }
            if (pchSymbolName == "SideGlabellaX")
            {
                m_ptGlabella.X = iValue;
                return 1;
            }
            if (pchSymbolName == "SideGlabellaY")
            {
                m_ptGlabella.Y = iValue;
                return 1;
            }
            if (pchSymbolName == "SideBenchmark1X")
            {
                m_ptBenchmark1.X = iValue;
                return 1;
            }
            if (pchSymbolName == "SideBenchmark1Y")
            {
                m_ptBenchmark1.Y = iValue;
                return 1;
            }
            if (pchSymbolName == "SideBenchmark2X")
            {
                m_ptBenchmark2.X = iValue;
                return 1;
            }
            if (pchSymbolName == "SideBenchmark2Y")
            {
                m_ptBenchmark2.Y = iValue;
                return 1;
            }
            return 0;
        }
        public int CalcCenterOfGravityX() 
{
    //m_ptChin, m_ptGlabella‚Í—LŒø‚È’l‚ª“ü‚ç‚È‚¢‚Ì‚Å–³Ž‹.
    // m_ptRightBelt, m_ptLeftBelt, m_ptBenchmark1, m_ptBenchmark2, m_ptKneeRightBelt, m_ptKneeLeftBelt;
    // m_ptAnkleRightBelt, m_ptAnkleLeftBelt‚à–³Ž‹,

        double dHeadX = m_ptEar.X;
        double dBodyX = m_ptShoulder.X * 0.507 + m_ptHip.X* 0.493;
        double dUpperArmX = m_ptShoulder.X;
        double dForeArmX = m_ptShoulder.X;
        double dHandX = m_ptShoulder.X;
        double dThighX = m_ptHip.X * 0.525 + m_ptKnee.X * 0.475;
        double dShankX = m_ptKnee.X* 0.594 + m_ptAnkle.X * 0.406;
        double dFootX = m_ptAnkle.X;

        double dCenterOfGravityX = 0.0;
        dCenterOfGravityX += dHeadX*  6.9 * 1 / 100.0;
	dCenterOfGravityX += dBodyX* 48.9 * 1 / 100.0;
	dCenterOfGravityX += dUpperArmX*  2.7 * 2 / 100.0;
	dCenterOfGravityX += dForeArmX*  1.6 * 2 / 100.0;
	dCenterOfGravityX += dHandX*  0.6 * 2 / 100.0;
	dCenterOfGravityX += dThighX* 11.0 * 2 / 100.0;
	dCenterOfGravityX += dShankX*  5.1 * 2 / 100.0;
	dCenterOfGravityX += dFootX*  1.1 * 2 / 100.0;

	return (int) dCenterOfGravityX;
    }


}
}
