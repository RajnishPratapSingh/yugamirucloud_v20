﻿namespace Yugamiru
{
    partial class IDD_BALANCELABO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IDC_SETTING_BTN = new System.Windows.Forms.PictureBox();
            this.IDC_CloseBtn = new System.Windows.Forms.PictureBox();
            this.IDC_AnalysisBtn = new System.Windows.Forms.PictureBox();
            this.IDC_MeasurementBtn = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_SETTING_BTN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_CloseBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_AnalysisBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_MeasurementBtn)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // IDC_SETTING_BTN
            // 
            this.IDC_SETTING_BTN.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_SETTING_BTN.Location = new System.Drawing.Point(754, 4);
            this.IDC_SETTING_BTN.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IDC_SETTING_BTN.Name = "IDC_SETTING_BTN";
            this.IDC_SETTING_BTN.Size = new System.Drawing.Size(151, 56);
            this.IDC_SETTING_BTN.TabIndex = 7;
            this.IDC_SETTING_BTN.TabStop = false;
            this.IDC_SETTING_BTN.Click += new System.EventHandler(this.IDC_SETTING_BTN_Click);
            // 
            // IDC_CloseBtn
            // 
            this.IDC_CloseBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_CloseBtn.Location = new System.Drawing.Point(518, 4);
            this.IDC_CloseBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IDC_CloseBtn.Name = "IDC_CloseBtn";
            this.IDC_CloseBtn.Size = new System.Drawing.Size(151, 56);
            this.IDC_CloseBtn.TabIndex = 6;
            this.IDC_CloseBtn.TabStop = false;
            this.IDC_CloseBtn.Click += new System.EventHandler(this.IDC_CloseBtn_Click);
            // 
            // IDC_AnalysisBtn
            // 
            this.IDC_AnalysisBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_AnalysisBtn.Location = new System.Drawing.Point(267, 4);
            this.IDC_AnalysisBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IDC_AnalysisBtn.Name = "IDC_AnalysisBtn";
            this.IDC_AnalysisBtn.Size = new System.Drawing.Size(151, 56);
            this.IDC_AnalysisBtn.TabIndex = 5;
            this.IDC_AnalysisBtn.TabStop = false;
            this.IDC_AnalysisBtn.Click += new System.EventHandler(this.IDC_AnalysisBtn_Click);
            // 
            // IDC_MeasurementBtn
            // 
            this.IDC_MeasurementBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_MeasurementBtn.Location = new System.Drawing.Point(27, 4);
            this.IDC_MeasurementBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IDC_MeasurementBtn.Name = "IDC_MeasurementBtn";
            this.IDC_MeasurementBtn.Size = new System.Drawing.Size(151, 56);
            this.IDC_MeasurementBtn.TabIndex = 4;
            this.IDC_MeasurementBtn.TabStop = false;
            this.IDC_MeasurementBtn.Click += new System.EventHandler(this.IDC_MeasurementBtn_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.IDC_MeasurementBtn);
            this.panel1.Controls.Add(this.IDC_CloseBtn);
            this.panel1.Controls.Add(this.IDC_SETTING_BTN);
            this.panel1.Controls.Add(this.IDC_AnalysisBtn);
            this.panel1.Location = new System.Drawing.Point(5, 134);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(938, 64);
            this.panel1.TabIndex = 8;
            // 
            // IDD_BALANCELABO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1068, 322);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "IDD_BALANCELABO";
            this.Text = "IDD_BALANCELABO";
            this.Load += new System.EventHandler(this.IDD_BALANCELABO_Load);
            this.SizeChanged += new System.EventHandler(this.IDD_BALANCELABO_SizeChanged);
            this.VisibleChanged += new System.EventHandler(this.IDD_BALANCELABO_VisibleChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.IDD_BALANCELABO_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IDD_BALANCELABO_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.IDC_SETTING_BTN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_CloseBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_AnalysisBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_MeasurementBtn)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox IDC_SETTING_BTN;
        private System.Windows.Forms.PictureBox IDC_CloseBtn;
        private System.Windows.Forms.PictureBox IDC_AnalysisBtn;
        private System.Windows.Forms.PictureBox IDC_MeasurementBtn;
        private System.Windows.Forms.Panel panel1;
    }
}