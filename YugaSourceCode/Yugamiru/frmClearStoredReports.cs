﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Net;

using System.Globalization;

namespace Yugamiru
{
    public partial class frmClearStoredReports : Form
    {
        SQLiteConnection cn;
        public bool wasCancelled = true;
        bool isCancelled = true;

        string db_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
             "\\gsport\\Yugamiru cloud\\database\\Yugamiru.sqlite";
        //string deleteAddressDatabase = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
        //     "\\gsport\\database";
        //string deleteAddressGsport = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
        //     "\\gsport";

        public frmClearStoredReports()
        {
            InitializeComponent();
            string languageConfig_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
             "\\gsport\\Yugamiru cloud\\LanguageConfig.txt";
            string languageSettingContent = System.IO.File.ReadAllText(languageConfig_file);
            if (languageSettingContent.Contains(@"""" + "en" + @""""))
            {
                CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(@"en");
            }
            else if (languageSettingContent.ToUpper().Contains(@"""" + "JA-JP" + @""""))
            {
                CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("ja-JP");
            }

            //this.label1.Text = "Yugamiru Data Loss Warning";
            //this.label1.Text = Properties.Resources.INFO_RELEASE_DELETE;

            //this.label2.Text = "Uninstalling Yugamiru Cloud will remove all saved patient reports from this PC. S" +
            //"ee the details below.";
            this.label2.Text = Properties.Resources.INFO_RELEASE_DELETE;

            //this.label4.Text = "Total number of reports will be removed from PC";
            this.label4.Text = Properties.Resources.TOTAL_REPORTS_WILL_REMOVED;

            //this.label5.Text = "Number of reports not saved on cloud";
            this.label5.Text = Properties.Resources.NUMBER_NOT_SAVED_REPORTS;

            //this.label6.Text = "Number of reports saved on cloud";
            this.label6.Text = Properties.Resources.NUMBER_SAVED_REPORTS;

            this.btnSaveToCloud.Text =Properties.Resources.SAVE_TO_CLOUD_NOW;

            this.btnContinueRelease.Text = Properties.Resources.CONTINUE_KEY_RELEASE;
        }

        private void btnSaveToCloud_Click(object sender, EventArgs e)
        {
            uploadRecords();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SetCounts();
        }

        public void SetCounts()
        {
            SQLiteDataAdapter ad;
            DataTable dtTotal = new DataTable();
            DataTable dtSavedToCloud = new DataTable();
            DataTable dtNotSavedOnCloud = new DataTable();

            try
            {
                cn = new SQLiteConnection("Data Source=" + db_file);
                using (SQLiteCommand cmd = new SQLiteCommand(cn))
                {
                    //Total-----------------
                    cn.Open();
                    //cmd = cn.CreateCommand();
                    cmd.CommandText = "select count(*) from PatientDetails";  //set the passed query
                    using (ad = new SQLiteDataAdapter(cmd))
                    {
                        ad.Fill(dtTotal);
                    }
                    cn.Close();
                    if (dtTotal.Rows.Count > 0)
                    {
                        string cnt = dtTotal.Rows[0][0].ToString();
                        lblTotalReportsCount.Text = cnt;
                    }
                    //--------------------

                    //Saved -----------------
                    cn.Open();
                    //cmd = cn.CreateCommand();
                    cmd.CommandText = "select count(*) from PatientDetails where lut is NULL OR length(lut)=0;";  //set the passed query
                    using (ad = new SQLiteDataAdapter(cmd))
                    {
                        ad.Fill(dtSavedToCloud);
                    }
                    cn.Close();
                    if (dtSavedToCloud.Rows.Count > 0)
                    {
                        string cnt = dtSavedToCloud.Rows[0][0].ToString();
                        lblCloudReportsCount.Text = cnt;
                    }
                    //--------------------


                    //Not Saved
                    cn.Open();
                    //cmd = cn.CreateCommand();
                    cmd.CommandText = "select count(*) from PatientDetails where lut is NOT NULL AND length(lut)>0;";  //set the passed query
                    using (ad = new SQLiteDataAdapter(cmd))
                    {
                        ad.Fill(dtNotSavedOnCloud);
                    }
                    cn.Close();
                    if (dtNotSavedOnCloud.Rows.Count > 0)
                    {
                        string cnt = dtNotSavedOnCloud.Rows[0][0].ToString();
                        lblNotSavedReportsCount.Text = cnt;
                        if (cnt == "0")
                            btnSaveToCloud.Enabled = false;
                    }
                }
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
            }            
        }

        void uploadRecords()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (IsInternetConnected())
                {
                    var a=CloudManager.GlobalItems.ActivationKey;
                    CloudManager.StartUpdateToServer.StartSending();
                    Cursor.Current = Cursors.Default;
                    //MessageBox.Show("Reports uploaded to Yugamiru Cloud successfully.", "Yugamiru");
                    MessageBox.Show(Properties.Resources.NUMBER_REPORTS_UPLOADED, "Yugamiru");
                    SetCounts();
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                    //MessageBox.Show("No internet connection. Please connect to internet and retry.", "Yugamiru");                    
                    MessageBox.Show(Properties.Resources.NO_NET_CONNECT_RETRY, "Yugamiru");
                }
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                //MessageBox.Show("Error:Records cannot be uploaded at this time.","Yugamiru",MessageBoxButtons.OK,MessageBoxIcon.Error);
                MessageBox.Show(Properties.Resources.ERROR_CANNOT_UPLOAD, "Yugamiru", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public static bool IsInternetConnected()
        {

            using (var client = new WebClient())
            {
                try
                {
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {                        
                        return true;
                    }
                }
                catch (Exception ex1)
                {
                    //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                    return false;
                }
            }

        }

        private void btnContinueKeyRelease_Click(object sender, EventArgs e)
        {            
            try
            {
               if(lblNotSavedReportsCount.Text.Trim()!="0")
                {
                    //DialogResult doContinue = MessageBox.Show("Warning: If you continue you will lose " + lblTotalReportsCount.Text + " patient reports permanently." 
                    //    + Environment.NewLine + "Do you want to continue?", "Yuganiru",
                    //    MessageBoxButtons.YesNo);

                    DialogResult doContinue = MessageBox.Show(Properties.Resources.WARNING_KEY_RELEASE_DATA_DELETE.Replace("N", lblTotalReportsCount.Text)
                        + Environment.NewLine +Properties.Resources.DO_WANT_CONTINUE, "Yuganiru",
                        MessageBoxButtons.YesNo);
                    if(doContinue==DialogResult.Yes)
                    {
                        DeleteDB();
                    }
                    else
                    {
                        return;
                    }
                    
                }
                else
                {
                    DeleteDB();
                }
            }
            catch
            {

            }
            isCancelled = false;
            wasCancelled = false;
            this.Close();
        }

        void DeleteDB()
        {
            try
            {
                string qry = "delete from FrontBodyPositionKneedown;" +
                             "delete from FrontBodyPositionStanding; " +
                             "delete from SideBodyPosition; " +
                             "delete from PatientDetails; ";
                CDatabase cdb = new CDatabase();
                cdb.ExecuteQuery(qry);

            }
            catch (Exception ex)
            {

            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(isCancelled)
            {
                wasCancelled = true;
            }
            else
            {
                wasCancelled = false;
            }
        }

        private void btnContinueRelease_Click(object sender, EventArgs e)
        {
            btnContinueKeyRelease_Click(null,null);
        }
    }
}
