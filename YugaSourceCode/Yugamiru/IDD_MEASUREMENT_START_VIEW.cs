﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.IO;
using static Yugamiru.stretchDIBbits;

namespace Yugamiru
{
    public partial class IDD_MEASUREMENT_START_VIEW : Form
    {
        bool m_ShootBtnFlag = true;
        bool m_RotateFlag = false;
        bool m_ReturnFlag = false;
        bool m_SearchFlag = false;
        enum MY_STATE
        {
            MY_STATE_STANDING = 0,
            MY_STATE_KNEE_DOWN,
            MY_STATE_SIDE,
            MY_STATE_END
        };

        public string imagepath; // added by rohini for GSP-1173
        bool image_status = true; //added by rohini for GTL#53 
        ImageClipWnd m_ImageClipWnd = new ImageClipWnd();
        ImagePreviewWnd m_ImagePreviewWnd = new ImagePreviewWnd();

        PictureBox picturebox1 = new PictureBox();

        Bitmap bmBack1;
        JointEditDoc m_JointEditDoc;
        Image<Bgr,byte> m_currentImage;

        public IDD_MEASUREMENT_START_VIEW(JointEditDoc GetDocument)
        {
            InitializeComponent();
            m_JointEditDoc = GetDocument;

            GetDocument.CreateBmpInfo(1280 * 1024 * 3, 1024, 1280);

            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_on;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;


            IDC_ID.Text = GetDocument.GetDataID().ToString();
            IDC_Name.Text = GetDocument.GetDataName();

            //Added by Sumit GSP-1318 ----START
            //string str = "-";
            //if (GetDocument.GetDataGender() == 1) str = "M";
            //if (GetDocument.GetDataGender() == 2) str = "F";
            //IDC_Gender.Text = str;            
            string str = "-";
            if (m_JointEditDoc.GetDataGender() == 1)
            {
                str = Yugamiru.Properties.Resources.MALE;
                if (str == "Male")
                    str = "M";
                else if (str == "男")
                    str = "男";
                else if (str == "남성")
                    str = "남성";
            }

            if (m_JointEditDoc.GetDataGender() == 2)
            {
                str = Yugamiru.Properties.Resources.FEMALE;
                if (str == "Female")
                    str = "F";
                else if (str == "女")
                    str = "女";
                else if (str == "여성")
                    str = "여성";
            }
            IDC_Gender.Text = str;   // «•Ê
                                     //Added by Sumit GSP-1318  ----END

            
            string year = string.Empty, month = string.Empty, day = string.Empty;
            GetDocument.GetDataDoB(ref year, ref month, ref day);
            str = month + "." + day + " " + year;
            //Updated Sumit for GTL#130  ---START
            IDC_DoB.Text = str;
            if (day != "0" && day != "00")
            {
                DateTime dtdb = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));
                if (!Properties.Resources.CURRENT_LANGUAGE.Trim().ToUpper().Contains("Japanese".ToUpper()))
                {
                    //IDC_DoB.Text = dtdb.ToString("dd MMM yyyy");
                    //IDC_DoB.Text = dtdb.ToString("dd") + " " + dtdb.ToString("MMM") + " " + dtdb.ToString("yyyy");
                    IDC_DoB.Text = dtdb.ToString("dd") + " " + UtilityGlobal.GetShortMonthName(dtdb.Month) + " " + dtdb.ToString("yyyy");
                }
                else
                {                    
                    //Updated by Sumit for GTL#158   ---START
                    //IDC_DoB.Text = dtdb.ToString("yyyy") + " " + dtdb.ToString("MM") + " " + dtdb.ToString("dd");
                    //1980年05月01日
                    IDC_DoB.Text = dtdb.ToString("yyyy") + "年" + dtdb.ToString("MM") + "月" + dtdb.ToString("dd") + "日";
                    //Updated by Sumit for GTL#158   ---END
                }
            }
            //Updated Sumit for GTL#130  ---END

           
            if (GetDocument.GetDataHeight() == 0)
                str = "-";  // g’·
            else
                str = GetDocument.GetDataHeight().ToString(); // g’·

            IDC_Height.Text = str;
            string strInstitutionName = string.Empty;
            GetDocument.GetInstitutionName(strInstitutionName);



            bmBack1 = Yugamiru.Properties.Resources.Mainpic3;
            picturebox1.Size = new Size(bmBack1.Size.Width, bmBack1.Size.Height);
            picturebox1.BackColor = Color.Transparent;
            this.Controls.Add(picturebox1);
            picturebox1.Image = bmBack1;
            IDC_NextBtn.Visible = false;


            switch (GetDocument.GetMeasurementStartViewMode())
            {
                case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                    SetSideStandingMode();
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                    SetFrontStandingMode();
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                    SetFrontKneedownMode();
                    break;
                default:
                    break;
            }

        }
        public void reload()
        {
            IDC_ID.Text = m_JointEditDoc.GetDataID().ToString();
            IDC_Name.Text = m_JointEditDoc.GetDataName();

            string str = "-";
            if (m_JointEditDoc.GetDataGender() == 1)
            {
                str = Yugamiru.Properties.Resources.MALE;
                if (str == "Male")
                    str = "M";
                else if (str == "男")
                    str = "男";
                else if (str == "남성")
                    str = "남성";
            }

            if (m_JointEditDoc.GetDataGender() == 2)
            {
                str = Yugamiru.Properties.Resources.FEMALE;
                if (str == "Female")
                    str = "F";
                else if (str == "女")
                    str = "女";
                else if (str == "여성")
                    str = "여성";
            }


            IDC_Gender.Text = str;
            string year = string.Empty, month = string.Empty, day = string.Empty;
            m_JointEditDoc.GetDataDoB(ref year, ref month, ref day);
            str = month + "." + day + " " + year;

            IDC_DoB.Text = str;

            //Added by Sumit GSP-1208   --------START
            string sYear = "";
            string sMonth = "";
            string sDay = "";
            m_JointEditDoc.GetDataDoB(ref sYear, ref sMonth, ref sDay);

            //Updated Sumit for GTL#130   ---START
            //DateTime dtdbout = new DateTime(Convert.ToInt32(sYear), Convert.ToInt32(sMonth), Convert.ToInt32(sDay));
            //IDC_DoB.Text = dtdbout.ToString("dd MMM yyyy");
            //IDC_DoB.Text = str;
            if (sDay != "0" && sDay != "00")
            {
                DateTime dtdb = new DateTime(Convert.ToInt32(sYear), Convert.ToInt32(sMonth), Convert.ToInt32(sDay));
                if (!Properties.Resources.CURRENT_LANGUAGE.Trim().ToUpper().Contains("Japanese".ToUpper()))
                {
                    //IDC_DoB.Text = dtdb.ToString("dd MMM yyyy");
                    //IDC_DoB.Text = dtdb.ToString("dd") + " " + dtdb.ToString("MMM") + " " + dtdb.ToString("yyyy");
                    IDC_DoB.Text = dtdb.ToString("dd") + " " +UtilityGlobal.GetShortMonthName(dtdb.Month) + " " + dtdb.ToString("yyyy");
                }
                else
                {                    
                    //Updated by Sumit for GTL#158   ---START
                    //IDC_DoB.Text = dtdb.ToString("yyyy") + " " + dtdb.ToString("MM") + " " + dtdb.ToString("dd");
                    //1980年05月01日
                    IDC_DoB.Text = dtdb.ToString("yyyy") + "年" + dtdb.ToString("MM") + "月" + dtdb.ToString("dd") + "日";
                    //Updated by Sumit for GTL#158   ---END
                }
            }
            //Updated Sumit for GTL#130   ---END


            //Added by Sumit GSP-1208   --------END

            if (m_JointEditDoc.GetDataHeight() == 0)
                str = "-";  // g’·
            else
                str = m_JointEditDoc.GetDataHeight().ToString(); // g’·

            IDC_Height.Text = str;
        }
        public void DisposeControls()
        {
            this.picturebox1.Image.Dispose();

            this.IDC_BackBtn.Image.Dispose();
            this.IDC_NextBtn.Image.Dispose();
            this.IDC_RotateBtn.Image.Dispose();
            this.IDC_SearchBtn.Image.Dispose();
            this.IDC_ShootBtn.Image.Dispose();

            Yugamiru.Properties.Resources.Mainpic3.Dispose();

            Yugamiru.Properties.Resources.imagecopy_on.Dispose();
            Yugamiru.Properties.Resources.imageload_on.Dispose();
            Yugamiru.Properties.Resources.imagerotation_on.Dispose();
            Yugamiru.Properties.Resources.gobackgreen_on.Dispose();
            Yugamiru.Properties.Resources.gonextgreen_on.Dispose();

            Yugamiru.Properties.Resources.SpeechFrontStanding.Dispose();

            this.Dispose();
            this.Close();

        }

        public void IDD_MEASUREMENT_START_VIEW_SizeChanged(object sender, EventArgs e)
        {
            
            picturebox1.Left = (this.ClientSize.Width - picturebox1.Width) / 2;
            picturebox1.Top = (this.ClientSize.Height - picturebox1.Height)/2;

            IDC_ID.Location = new Point(230 + 10, 40 + picturebox1.Top);
            IDC_ID.Size = new Size(90, 24);
            IDC_ID.Font = new Font("HP Simplified Light", 10);

            IDC_Name.Location = new Point(150 + 230 + 10, 40 + picturebox1.Top);
            IDC_Name.Size = new Size(167, 24);
            IDC_Name.Font = new Font("HP Simplified Light", 10);

            IDC_Gender.Location = new Point(300 + 230 + 20, 40 + picturebox1.Top);
            IDC_Gender.Size = new Size(34, 24);
            IDC_Gender.Font = new Font("HP Simplified Light", 10);

            IDC_DoB.Location = new Point(450 + 230 - 50, 40 + picturebox1.Top);
            IDC_DoB.Size = new Size(126, 24);
            IDC_DoB.Font = new Font("HP Simplified Light", 10);

            IDC_Height.Location = new Point(600 + 230 - 30, 40 + picturebox1.Top);
            IDC_Height.Size = new Size(50, 24);
            IDC_Height.Font = new Font("HP Simplified Light", 10);

            IDC_BackBtn.Location = new Point(220, 630 + picturebox1.Top);
            IDC_BackBtn.Size = new Size(112, 42);
            IDC_SearchBtn.Location = new Point(IDC_BackBtn.Left + IDC_BackBtn.Width + 20, 630 + picturebox1.Top);
            IDC_SearchBtn.Size = new Size(112, 42);
            IDC_RotateBtn.Location = new Point(IDC_SearchBtn.Left + IDC_SearchBtn.Width + 20, 630 + picturebox1.Top);
            IDC_RotateBtn.Size = new Size(112, 42);
            IDC_NextBtn.Location = new Point(900, 630 + picturebox1.Top);
            IDC_NextBtn.Size = new Size(112, 42);
            IDC_ShootBtn.Location = new Point(600 + 26, 250 + picturebox1.Top);
            IDC_ShootBtn.Size = new Size(112, 42);


            ImagePrevWnd.Top = 130 + picturebox1.Top;
            ImagePrevWnd.Size = new Size(384, 480);

            ImageClipWnd.Top = 130 + picturebox1.Top;
            ImageClipWnd.Size = new Size(384, 480);
            int str_length = 74;// 74 --> default width of label for 10 characters

            //updated by sumit GTL-131   ---START
            //IDC_ID.Left = (this.Width - str_length) / 2 - 400; 
            //IDC_Name.Left = IDC_ID.Left + 126;
            //IDC_Gender.Left = IDC_Name.Left + 170 + 6 + 2;
            //IDC_DoB.Left = IDC_Gender.Left + 56 + 8;
            IDC_ID.Left = (this.Width - str_length) / 2 - 400 - 20;
            IDC_Name.Left = IDC_ID.Left + 126-2;
            IDC_Gender.Left = IDC_Name.Left + 170 + 6 + 2 + 17 - 3;
            IDC_DoB.Left = IDC_Gender.Left + 56 + 8;
            //updated by sumit GTL-131   ---END

            //updated by sumit GTL-131   ---START
            {
                //IDC_Name.BackColor = Color.YellowGreen; //For testing only
                //IDC_ID.BackColor = Color.YellowGreen; //For testing only
                //IDC_DoB.BackColor = Color.YellowGreen; //For testing only
                //IDC_Gender.BackColor = Color.YellowGreen; //For testing only
                //IDC_Height.BackColor = Color.YellowGreen; //For testing only
            }
            //updated by sumit GTL-131   ---END

            IDC_Height.Left = IDC_DoB.Left + 180;

            //Case added by Rajnish for GSP-1412 after new resource images --------------Start
            if (Properties.Resources.CURRENT_LANGUAGE.Trim().ToUpper().Contains("english".ToUpper()))
            {
                //pictureBox2.Left = IDC_Height.Left + 124;//Commented by Rajnish for GTL #357
                pictureBox2.Left = IDC_Height.Left + 145;//Modified by Rajnish for GSP-1412
                pictureBox2.Top = 22 + picturebox1.Top;
            }
            else
            {
                //pictureBox2.Left = IDC_Height.Left + 124;//Commented by Rajnish for GTL #357
                pictureBox2.Left = IDC_Height.Left + 133;//Added by Rajnish for GTL #357
                pictureBox2.Top = 32 + picturebox1.Top;
            }
            //Case added by Rajnish for GSP-1412 after new resource images --------------END

            IDC_ShootBtn.Left = (this.Width - IDC_ShootBtn.Width) / 2;
            IDC_ShootBtn.Top = (this.Height - IDC_ShootBtn.Height) / 2;//+ picturebox1.Top;
            ImageClipWnd.Left = (this.Width - ImageClipWnd.Width) / 2 - 264;
            ImagePrevWnd.Left = ImageClipWnd.Left + ImageClipWnd.Width + IDC_ShootBtn.Width + 30;

            IDC_BackBtn.Left = ImageClipWnd.Left - 10;
            IDC_SearchBtn.Left = IDC_BackBtn.Left + IDC_BackBtn.Width + 34;
            IDC_RotateBtn.Left = IDC_SearchBtn.Left + IDC_SearchBtn.Width + 34;
            IDC_NextBtn.Left = ImagePrevWnd.Left + ImagePrevWnd.Width - IDC_NextBtn.Width;

            

            m_ImagePreviewWnd.m_iOffscreenWidth = ImagePrevWnd.Width;
            m_ImagePreviewWnd.m_iOffscreenHeight = ImagePrevWnd.Height;

            m_ImageClipWnd.m_iOffscreenWidth = ImagePrevWnd.Width;
            m_ImageClipWnd.m_iOffscreenHeight = ImagePrevWnd.Height;

        }
       

        public void SetSideStandingMode()
        {
            
            pictureBox2.Image = Yugamiru.Properties.Resources.SpeechSideStanding;
            pictureBox2.Width = pictureBox2.Image.Width;
            pictureBox2.Height = pictureBox2.Image.Height;

            if (m_JointEditDoc.m_SideImageBytes == null)
            {
                image_status = true; //added by rohini for GTL#53 
                m_JointEditDoc.m_Image_Path = null; // added by rohini for GSP-1173
                //string strScoresheetFolderPath;
                //m_JointEditDoc.GetScoresheetFolderPath(strScoresheetFolderPath);
                Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.sokui);
                EmguCVImage = EmguCVImage.Resize(384, 480, Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.SetSideClipImage(EmguCVImage.ToBitmap());
                m_ImageClipWnd.SetBackgroundBitmap(EmguCVImage);
                m_ImagePreviewWnd.SetBackgroundBitmap(new Image<Bgr, Byte>(Yugamiru.Properties.Resources.sokui));
            }
            else
            {
                image_status = false; //added by rohini for GTL#53 
                m_ImageClipWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_SideImageBytes);
                Image<Bgr, byte> EmguCVSideImage = new Image<Bgr, byte>(1024,1280);
                EmguCVSideImage.Bytes = m_JointEditDoc.m_SideImageBytes;

                m_JointEditDoc.SetSideClipImage(EmguCVSideImage.ToBitmap());
                m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_SideImageBytes);
                //Added by SUMIT GSP-1237----------START
                string m = "-1";
                string d = "-2";
                string y = "-3";
                m_JointEditDoc.GetDataDoB(ref y, ref m, ref d);
                m_JointEditDoc.SetDataDoB(y, m, d);

                //Updated Sumit for GTL#130   ---START
                if (d != "0" && d != "00")
                {
                    DateTime dtdb1 = new DateTime(Convert.ToInt32(y), Convert.ToInt32(m), Convert.ToInt32(d));
                    //this.IDC_DoB.Text = dtdb.ToString("dd MMM yyyy");
                    if (!Properties.Resources.CURRENT_LANGUAGE.Trim().ToUpper().Contains("Japanese".ToUpper()))
                    {
                        //IDC_DoB.Text = dtdb1.ToString("dd MMM yyyy");
                        //IDC_DoB.Text = dtdb1.ToString("dd") + " " + dtdb1.ToString("MMM") + " " + dtdb1.ToString("yyyy");
                        IDC_DoB.Text = dtdb1.ToString("dd") + " " + UtilityGlobal.GetShortMonthName(dtdb1.Month) + " " + dtdb1.ToString("yyyy");
                    }
                    else
                    {
                        //Updated by Sumit for GTL#158   ---START
                        //IDC_DoB.Text = dtdb1.ToString("yyyy") + " " + dtdb1.ToString("MM") + " " + dtdb1.ToString("dd");
                        //1980年05月01日
                        IDC_DoB.Text = dtdb1.ToString("yyyy") + "年" + dtdb1.ToString("MM") + "月" + dtdb1.ToString("dd") + "日";
                        //Updated by Sumit for GTL#158   ---END
                    }
                }
                //Updated Sumit for GTL#130   ---END

                int genderCode = m_JointEditDoc.GetDataGender();
                if (genderCode == 1)
                {
                    this.IDC_Gender.Text = "M";
                }
                else if (genderCode == 2)
                {
                    this.IDC_Gender.Text = "F";
                }
                else
                {
                    this.IDC_Gender.Text = "-";
                }

                //Added by Sumit GSP-1318  ----START
                string str = "-";
                if (m_JointEditDoc.GetDataGender() == 1)
                {
                    str = Yugamiru.Properties.Resources.MALE;
                    if (str == "Male")
                        str = "M";
                    else if (str == "男")
                        str = "男";
                    else if (str == "남성")
                        str = "남성";
                }

                if (m_JointEditDoc.GetDataGender() == 2)
                {
                    str = Yugamiru.Properties.Resources.FEMALE;
                    if (str == "Female")
                        str = "F";
                    else if (str == "女")
                        str = "女";
                    else if (str == "여성")
                        str = "여성";
                }
                IDC_Gender.Text = str;   // «•Ê
                                         //Added by Sumit GSP-1318  ----END

                this.IDC_Height.Text = Convert.ToInt16(m_JointEditDoc.GetDataHeight()).ToString();
                //Added by SUMIT GSP-1237----------END

                //Added By Rohini for GSP-1173-----------START
                if (File.Exists(@"C:\ProgramData\gsport\Yugamiru cloud\treeLocation.txt"))
                {
                    imagepath = File.ReadAllText(@"C:\ProgramData\gsport\Yugamiru cloud\treeLocation.txt");
                    m_JointEditDoc.m_Image_Path = imagepath; 
                }
                //Added By Rohini for GSP-1173-----------END
                //iCmdShowNextButtonOnNewInput = Constants.SW_SHOW;
            }
            //m_JointEditDoc.AllocSideImage(m_ImageClipWnd.m_pbyteBits);//new line
            m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING);

            ImageClipWnd.Invalidate();
            ImagePrevWnd.Invalidate();

            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        if(m_JointEditDoc.m_SideImageBytes == null)
                        IDC_NextBtn.Visible = false;
                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {

                        if (m_JointEditDoc.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_NONE)
                            IDC_BackBtn.Visible = true;
                        else
                        {
                            IDC_BackBtn.Visible = false;
                            //Added by sumit GSP-1081 -----------START
                            //Added by sumit GSP-1237 -----------START
                            //IDC_ID.Text = UtilityGlobal.ID_NAME_Handler.GetIDForApp();
                            //IDC_Name.Text = UtilityGlobal.ID_NAME_Handler.GetNameForApp();                            
                            if (UtilityGlobal.ID_NAME_Handler != null && UtilityGlobal.ID_NAME_Handler.GetIDForApp() != ""
                                 && UtilityGlobal.ID_NAME_Handler.GetNameForApp() != "")
                            {
                                IDC_ID.Text = UtilityGlobal.ID_NAME_Handler.GetIDForApp();
                                IDC_Name.Text = UtilityGlobal.ID_NAME_Handler.GetNameForApp();
                            }
                            else
                            {
                                IDC_ID.Text = m_JointEditDoc.GetDataID();
                                IDC_Name.Text = m_JointEditDoc.GetDataName();
                            }
                            //Added by sumit GSP-1237 -----------END

                            //Added by sumit GSP-1081 -----------END
                        }
                        IDC_NextBtn.Visible = true;                        
                    }
                    break;
                default:
                    break;
            }
            //Added by Sumit GSP-1208   --------START
            string sYear = "";
            string sMonth = "";
            string sDay = "";
            m_JointEditDoc.GetDataDoB(ref sYear, ref sMonth, ref sDay);
            if (sYear == "0")
                return;
            //Updated Sumit for GTL#130   ---START
            //DateTime dtdbout = new DateTime(Convert.ToInt32(sYear), Convert.ToInt32(sMonth), Convert.ToInt32(sDay));
            //IDC_DoB.Text = dtdbout.ToString("dd MMM yyyy");
            if (sDay != "0" && sDay != "00")
            {
                DateTime dtdb = new DateTime(Convert.ToInt32(sYear), Convert.ToInt32(sMonth), Convert.ToInt32(sDay));
                if (!Properties.Resources.CURRENT_LANGUAGE.Trim().ToUpper().Contains("Japanese".ToUpper()))
                {
                    //IDC_DoB.Text = dtdb.ToString("dd MMM yyyy");
                    //IDC_DoB.Text = dtdb.ToString("dd") + " " + dtdb.ToString("MMM") + " " + dtdb.ToString("yyyy");
                    IDC_DoB.Text = dtdb.ToString("dd") + " " + UtilityGlobal.GetShortMonthName(dtdb.Month) + " " + dtdb.ToString("yyyy");
                }
                else
                {                    
                    //Updated by Sumit for GTL#158   ---START
                    //IDC_DoB.Text = dtdb.ToString("yyyy") + " " + dtdb.ToString("MM") + " " + dtdb.ToString("dd");
                    //1980年05月01日
                    IDC_DoB.Text = dtdb.ToString("yyyy") + "年" + dtdb.ToString("MM") + "月" + dtdb.ToString("dd") + "日";
                    //Updated by Sumit for GTL#158   ---END
                }
            }
            //Updated Sumit for GTL#130   ---END
            //Added by Sumit GSP-1208   --------END
            //updated Using by Sumit GTL#118----START            
            System.Threading.Thread.Sleep(1);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.WaitForFullGCComplete();
            GC.Collect();
            //updated Using by Sumit GTL#118----END
        }

        public void SetFrontStandingMode()
        {

            //int iCmdShowNextButtonOnNewInput = Constants.SW_HIDE;
            
            pictureBox2.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
            pictureBox2.Width = pictureBox2.Image.Width;
            pictureBox2.Height = pictureBox2.Image.Height;

            if (m_JointEditDoc.m_FrontStandingImageBytes == null)
            {
                image_status = true; //added by rohini for GTL#53 
                Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.ritui);
                EmguCVImage = EmguCVImage.Resize(384, 480, Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.SetStandClipImage(EmguCVImage.ToBitmap());
                m_ImageClipWnd.SetBackgroundBitmap(EmguCVImage);
                m_ImagePreviewWnd.SetBackgroundBitmap(new Image<Bgr, Byte>(Yugamiru.Properties.Resources.ritui));
            }
            else
            {
                image_status = false; //added by rohini for GTL#53 
                m_ImageClipWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontStandingImageBytes);
                m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontStandingImageBytes);

                Image<Bgr, byte> EmguCVStandImage = new Image<Bgr, byte>(1024, 1280);
                EmguCVStandImage.Bytes = m_JointEditDoc.m_FrontStandingImageBytes;

                m_JointEditDoc.SetStandClipImage(EmguCVStandImage.ToBitmap());
                //Added By Rohini for GSP-1173-----------START
                if (File.Exists(@"C:\ProgramData\gsport\Yugamiru cloud\treeLocation.txt"))
                {
                    imagepath = File.ReadAllText(@"C:\ProgramData\gsport\Yugamiru cloud\treeLocation.txt");
                    m_JointEditDoc.m_Image_Path = imagepath; 
                }
                //Added By Rohini for GSP-1173-----------END
                //iCmdShowNextButtonOnNewInput = Constants.SW_SHOW;
            }
            //m_JointEditDoc.AllocStandingImage(m_ImageClipWnd.m_pbyteBits);//new line
            m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING);
            ImageClipWnd.Invalidate();
            ImagePrevWnd.Invalidate();
            
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {

                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                       // if (m_JointEditDoc.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_NONE)
                        //    IDC_BackBtn.Visible = true;
                        //else
                            IDC_BackBtn.Visible = true;
                        IDC_BackBtn.Image = Properties.Resources.gobackgreen_up;
                        IDC_NextBtn.Visible = true;

                    }
                    break;
                default:
                    break;
            }
            //updated Using by Sumit GTL#118----START            
            System.Threading.Thread.Sleep(1);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.WaitForFullGCComplete();
            GC.Collect();
            //updated Using by Sumit GTL#118----END
        }


        public void SetFrontKneedownMode()
        {
            //int iCmdShowNextButtonOnNewInput = Constants.SW_HIDE;
            
            pictureBox2.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;
            pictureBox2.Width = pictureBox2.Image.Width;
            pictureBox2.Height = pictureBox2.Image.Height;

            if (m_JointEditDoc.m_FrontKneedownImageBytes == null)
            {
                image_status = true; //added by rohini for GTL#53 
                Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(Yugamiru.Properties.Resources.kutui);
                EmguCVImage = EmguCVImage.Resize(384, 480, Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.SetKneeClipImage(EmguCVImage.ToBitmap());
                m_ImageClipWnd.SetBackgroundBitmap(EmguCVImage);
                m_ImagePreviewWnd.SetBackgroundBitmap(new Image<Bgr, Byte>(Yugamiru.Properties.Resources.kutui));

            }
            else
            {
                image_status = false; //added by rohini for GTL#53 
                m_ImageClipWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontKneedownImageBytes);
                m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontKneedownImageBytes);

                Image<Bgr, byte> EmguCVKneedownImage = new Image<Bgr, byte>(1024, 1280);
                EmguCVKneedownImage.Bytes = m_JointEditDoc.m_FrontKneedownImageBytes;

                m_JointEditDoc.SetKneeClipImage(EmguCVKneedownImage.ToBitmap());
                //Added By Rohini for GSP-1173-----------START
                if (File.Exists(@"C:\ProgramData\gsport\Yugamiru cloud\treeLocation.txt"))
                {
                    imagepath = File.ReadAllText(@"C:\ProgramData\gsport\Yugamiru cloud\treeLocation.txt");
                    m_JointEditDoc.m_Image_Path = imagepath; 
                }

                //Added By Rohini for GSP-1173-----------END
                //iCmdShowNextButtonOnNewInput = Constants.SW_SHOW;
            }
            //m_JointEditDoc.AllocKneedownImage(m_ImageClipWnd.m_pbyteBits);//new line
            m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN);
            ImageClipWnd.Invalidate();
            ImagePrevWnd.Invalidate();

            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {

                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        //if (m_JointEditDoc.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_NONE)
                            //IDC_BackBtn.Visible = true;
                       // else
                            IDC_BackBtn.Visible = true;

                        IDC_BackBtn.Image = Properties.Resources.gobackgreen_up;
                        IDC_NextBtn.Visible = true;

                    }
                    break;
                default:
                    break;
            }
            //updated Using by Sumit GTL#118----START            
            System.Threading.Thread.Sleep(1);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.WaitForFullGCComplete();
            GC.Collect();
            //updated Using by Sumit GTL#118----END
        }


        private void ImageClipWnd_SizeChanged(object sender, EventArgs e)
        {
        }

        private void ImageClipWnd_Paint(object sender, PaintEventArgs e)
        {

            if (m_RotateFlag)
                m_ImageClipWnd.RotateClockwiseBackgroundBitmap(e.Graphics);

            m_ImageClipWnd.UpdateOffscreen(e.Graphics);
        }

        private void ImageClipWnd_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_ImageClipWnd.m_iMouseCaptureMode > 0)
            {
                int iSelectionFrameCenterX = (e.X - m_ImageClipWnd.m_iDestRectUpperLeftCornerX) *
                    m_ImageClipWnd.m_iBackgroundWidth / m_ImageClipWnd.m_iDestRectWidth;
                if (iSelectionFrameCenterX < 0)
                {
                    iSelectionFrameCenterX = 0;
                }
                if (iSelectionFrameCenterX >= m_ImageClipWnd.m_iBackgroundWidth)
                {
                    iSelectionFrameCenterX = m_ImageClipWnd.m_iBackgroundWidth;
                }
                m_ImageClipWnd.m_iSelectionFrameUpperLeftCornerX = iSelectionFrameCenterX -
                    m_ImageClipWnd.m_iSelectionFrameWidth / 2;
                ImageClipWnd.Invalidate();





            }

        }

        private void ImageClipWnd_MouseUp(object sender, MouseEventArgs e)
        {
            m_ImageClipWnd.m_iMouseCaptureMode = 0;
        }

        private void ImageClipWnd_MouseDown(object sender, MouseEventArgs e)
        {
            m_ImageClipWnd.m_iMouseCaptureMode = 1;
            int iSelectionFrameCenterX = (e.X - m_ImageClipWnd.m_iDestRectUpperLeftCornerX) *
                m_ImageClipWnd.m_iBackgroundWidth / m_ImageClipWnd.m_iDestRectWidth;
            if (iSelectionFrameCenterX < 0)
            {
                iSelectionFrameCenterX = 0;
            }
            if (iSelectionFrameCenterX >= m_ImageClipWnd.m_iBackgroundWidth)
            {
                iSelectionFrameCenterX = m_ImageClipWnd.m_iBackgroundWidth;
            }
            m_ImageClipWnd.m_iSelectionFrameUpperLeftCornerX = iSelectionFrameCenterX -
                m_ImageClipWnd.m_iSelectionFrameWidth / 2;

        }

        private void IDC_ShootBtn_Click(object sender, EventArgs e)
        {
            // added by rohini for GSP-1173 START
            if (image_status == true) //added by rohini for GTL#53 
            {
                if (m_JointEditDoc.m_Image_Path == null || m_JointEditDoc.m_Image_Path.Trim().Length == 0)
                {
                    //MessageBox.Show("Please select the image","Yugamiru");
                    MessageBox.Show(Properties.Resources.PLEASE_SELECT_IMAGE, "Yugamiru");
                    return;

                }
            }
            //Added By Rohini for GSP-1173-----------END
            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_down;
            m_Clickmode = "ENTER";
            t.Interval = 100;
            t.Tick += new EventHandler(timer_tick);
            t.Start();

            IDC_NextBtn.Visible = true;
            m_ShootBtnFlag = true;
            ImagePrevWnd.Invalidate();

            byte[] pbyteBits = new byte[1024 * 1280 * 3];
            Bitmap bmp = new Bitmap(ImagePrevWnd.ClientSize.Width, ImagePrevWnd.ClientSize.Height);
            ImagePrevWnd.DrawToBitmap(bmp, ImagePrevWnd.ClientRectangle);
            Image<Bgr, byte> EmguCVPreviewImage = new Image<Bgr, byte>(bmp);
            EmguCVPreviewImage = EmguCVPreviewImage.Resize(1024, 1280, Emgu.CV.CvEnum.Inter.Linear);
            pbyteBits = EmguCVPreviewImage.Bytes;

            switch (m_JointEditDoc.GetMeasurementStartViewMode())
            {
                case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                    m_JointEditDoc.SetSideClipImage(EmguCVPreviewImage.ToBitmap());
                    m_JointEditDoc.AllocSideImage(pbyteBits);                   
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                    m_JointEditDoc.SetStandClipImage(EmguCVPreviewImage.ToBitmap());
                    m_JointEditDoc.AllocStandingImage(pbyteBits);                    
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                    m_JointEditDoc.SetKneeClipImage(EmguCVPreviewImage.ToBitmap());
                    m_JointEditDoc.AllocKneedownImage(pbyteBits);
                    break;
                default:
                    break;

            }

            
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_up;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
            
            
            
            //Added by Sumit GTL#118----START           
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.WaitForFullGCComplete();
            GC.Collect();
            //Added by Sumit GTL#118----END
        }

        private void IDC_SearchBtn_Click(object sender, EventArgs e)
        {
            //Added by Sumit GTL#118----START
            
            System.Threading.Thread.Sleep(1000);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.WaitForFullGCComplete();
            GC.Collect();
            //Added by Sumit GTL#118----END


            m_Clickmode = "OPEN";
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_down;
            t.Interval = 100;
            t.Tick += new EventHandler(timer_tick);
            t.Start();

            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_up;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;

            //try catch finally , wait cursor added by sumit on 22-Mar-18

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
                m_SearchFlag = true;
                m_ShootBtnFlag = false;
                //Added USING block Sumit GSP-1143----START
                //IMAGE_FILE_SELECT m_OpenDialog = new IMAGE_FILE_SELECT(m_JointEditDoc);
                try
                {
                    using (IMAGE_FILE_SELECT m_OpenDialog = new IMAGE_FILE_SELECT(m_JointEditDoc))
                    {                        
                        if (m_OpenDialog.ShowDialog() == DialogResult.OK)
                        {                            
                            // New feature implemented for Auto-rotate of image
                            //Added Guess FIx Sumit GSP-1143----START
                            if (m_JointEditDoc.m_Image_Path == null || m_JointEditDoc.m_Image_Path.Trim().Length == 0 || !File.Exists(m_JointEditDoc.m_Image_Path.Trim()))
                            {
                                return;
                            }
                            //Added Guess FIx Sumit GSP-1143----END
                            #region ImageProcessing
                            imagepath = m_JointEditDoc.m_Image_Path; // added by rohini for GSP-1173                           

                            //updated Using by Sumit GTL#118----START
                            //Image src_Image = new Bitmap(m_JointEditDoc.m_Image_Path);                            
                            //CorrectOrientation(ref src_Image);
                            //m_currentImage = new Image<Bgr, byte>(new Bitmap(src_Image));
                            {
                                Image src_Image = new Bitmap(m_JointEditDoc.m_Image_Path);
                                CorrectOrientation(ref src_Image);
                                m_currentImage = new Image<Bgr, byte>(new Bitmap(src_Image));
                                src_Image.Dispose();
                            }
                            //updated Using by Sumit GTL#118----END
                            if (m_currentImage.Width % 2 != 0) // check for odd number
                            {
                                if (((m_currentImage.Width - 1) / 2) % 2 != 0)
                                    m_currentImage = m_currentImage.Resize(m_currentImage.Width - 3, m_currentImage.Height - 2, Emgu.CV.CvEnum.Inter.Linear);
                                else
                                    m_currentImage = m_currentImage.Resize(m_currentImage.Width - 1, m_currentImage.Height, Emgu.CV.CvEnum.Inter.Linear);
                            }
                            else if ((m_currentImage.Width / 2) % 2 != 0) // else check fraction of width is odd number
                                m_currentImage = m_currentImage.Resize(m_currentImage.Width - 2, m_currentImage.Height - 1, Emgu.CV.CvEnum.Inter.Linear);
                            //m_JointEditDoc.SetFlipImage(m_currentImage.ToBitmap());                
                            switch (m_JointEditDoc.GetMeasurementStartViewMode())
                            {
                                case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                                    m_JointEditDoc.SetSideClipImage(m_currentImage.ToBitmap());
                                    break;
                                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                                    m_JointEditDoc.SetStandClipImage(m_currentImage.ToBitmap());
                                    break;
                                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                                    m_JointEditDoc.SetKneeClipImage(m_currentImage.ToBitmap());
                                    break;
                                default:
                                    break;
                            }

                            m_ImageClipWnd.SetBackgroundBitmap(m_currentImage);//m_JointEditDoc.m_Image_Path));

                            ImageClipWnd.Invalidate();
#endregion
                        }
                        //Added by Sumit GTL#118----START
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        GC.WaitForFullGCComplete();
                        GC.Collect();
                        //Added by Sumit GTL#118----END
                    }
                    //Added USING block Sumit GSP-1143----END
                    //Added by Sumit GTL#118----START
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.WaitForFullGCComplete();
                    GC.Collect();
                    //Added by Sumit GTL#118----END
                }
                //Added Specific Exception Catch by Sumit GTL#118----START
                catch (OutOfMemoryException exmy)
                {
#if DEBUG
MessageBox.Show(exmy.Message + Environment.NewLine + exmy.StackTrace);
#endif
                    MessageBox.Show(Properties.Resources.HD_IMAGE_ER_MESSAGE, "Yugamiru");
                    m_JointEditDoc.GetMainScreen().DeleteScoresheetImagesInFolder();
                    Environment.Exit(0);
                }
                //Added Specific Exception Catch by Sumit GTL#118----END
                catch (Exception ex)
                {
                    //Updated by Sumit GTL#118----START
                    //MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
#if DEBUG
                    MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
#endif
                    MessageBox.Show(Properties.Resources.HD_IMAGE_ER_MESSAGE, "Yugamiru");
                    m_JointEditDoc.GetMainScreen().DeleteScoresheetImagesInFolder();
                    Environment.Exit(0);                    
                    //Updated by Sumit GTL#118----END
                }
            }
            finally { Cursor.Current = Cursors.Default; }
        }

        private void CorrectOrientation(ref Image image)
        {
            if (image == null) return;
            int orientationId = 0x0112;
            if (image.PropertyIdList.Contains(orientationId))
            {
                var orientation = (int)image.GetPropertyItem(orientationId).Value[0];
                var rotateFlip = RotateFlipType.RotateNoneFlipNone;
                switch (orientation)
                {
                    case 1: rotateFlip = RotateFlipType.RotateNoneFlipNone; break;
                    case 2: rotateFlip = RotateFlipType.RotateNoneFlipX; break;
                    case 3: rotateFlip = RotateFlipType.Rotate180FlipNone; break;
                    case 4: rotateFlip = RotateFlipType.Rotate180FlipX; break;
                    case 5: rotateFlip = RotateFlipType.Rotate90FlipX; break;
                    case 6: rotateFlip = RotateFlipType.Rotate90FlipNone; break;
                    case 7: rotateFlip = RotateFlipType.Rotate270FlipX; break;
                    case 8: rotateFlip = RotateFlipType.Rotate270FlipNone; break;
                    default: rotateFlip = RotateFlipType.RotateNoneFlipNone; break;
                }
                if (rotateFlip != RotateFlipType.RotateNoneFlipNone)
                {
                    image.RemovePropertyItem(orientationId);                   
                }
            }
        }

        Timer t = new Timer();
        static string m_Clickmode = "NONE";

        void timer_tick(object sender, EventArgs e)
        {
            switch(m_Clickmode)
            {
                case "RETURN":
                    IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_on;
                    break;
                case "OPEN":
                    IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_on;
                    break;
                case "ROTATE":
                    IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_on;
                    break;
                case "ENTER":
                    //Added by Sumit GSP-1261 -----START
                    m_JointEditDoc.GetMainScreen().isSavePending = true;
                    //Added by Sumit GSP-1261 -----END
                    IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_on;
                    break;
                case "NEXT":
                    //Added by Sumit GSP-1261 -----START
                    //m_JointEditDoc.GetMainScreen().isSavePending = true;
                    //Added by Sumit GSP-1261 -----END
                    IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_on;
                    break;
            }
            t.Stop();

        }

        private void IDC_BackBtn_Click(object sender, EventArgs e)
        {
            m_Clickmode = "RETURN";
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            t.Interval = 100;
            t.Tick += new EventHandler(timer_tick);
            t.Start();

            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_up;
            
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;

            m_JointEditDoc.m_Image_Path = null; // added by rohini for GSP-1173

            m_ReturnFlag = true;
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                                // ŒÂlî•ñ‰æ–Ê‚É‘JˆÚ‚·‚é.
                                m_JointEditDoc.SetMeasurementStartViewMode
                                    (Constants.MEASUREMENTSTARTVIEWMODE_NONE);
                                m_JointEditDoc.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
                                //m_JointEditDoc.ChangeToMeasurementView();
                                /*  CloseForm(EventArgs.Empty);
                                  this.Close();
                                  DisposeControls();*/
                                this.Visible = false;
                                m_JointEditDoc.GetMeasurementDlg().Visible = true;
                                m_JointEditDoc.GetMeasurementDlg().RefreshForm();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                                IDC_NextBtn.Visible = true;
                                SetSideStandingMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                                IDC_NextBtn.Visible = true;
                                SetFrontStandingMode();
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                                // ‚±‚±‚É‚­‚é‚±‚Æ‚Í‚È‚¢.
                                m_JointEditDoc.SetMeasurementStartViewMode
                                    (Constants.MEASUREMENTSTARTVIEWMODE_NONE);
                                m_JointEditDoc.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
                                //m_JointEditDoc.ChangeToMeasurementView();
                                /*  CloseForm(EventArgs.Empty);
                                  this.Close();
                                  DisposeControls();*/
                                this.Visible = false;
                                m_JointEditDoc.GetMeasurementDlg().Visible = true;
                                m_JointEditDoc.GetMeasurementDlg().RefreshForm();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                                m_JointEditDoc.SetMeasurementStartViewMode
                                    (Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING);
                                SetSideStandingMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                                m_JointEditDoc.SetMeasurementStartViewMode
                                    (Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING);
                                SetFrontStandingMode();
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }

            
            

        }


        public event EventHandler closeForm; // creating event handler - step1
        public void CloseForm(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = closeForm;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_NextBtn_Click(object sender, EventArgs e)
        {

            //m_ReturnFlag = false;

            /*  byte[] pbyteBits = new byte[1024 * 1280 * 3];
              Bitmap bmp = new Bitmap(ImagePrevWnd.ClientSize.Width, ImagePrevWnd.ClientSize.Height);
              ImagePrevWnd.DrawToBitmap(bmp, ImagePrevWnd.ClientRectangle);
              Image<Bgr, byte> EmguCVPreviewImage = new Image<Bgr, byte>(bmp);
              EmguCVPreviewImage = EmguCVPreviewImage.Resize(1024, 1280, Emgu.CV.CvEnum.Inter.Linear);
              pbyteBits = EmguCVPreviewImage.Bytes;
              */
            m_JointEditDoc.m_Image_Path = null; // added by rohini for GSP-1173
            m_Clickmode = "NEXT";
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_down;
            t.Interval = 100;
            t.Tick += new EventHandler(timer_tick);
            t.Start();
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                               /* if (m_ShootBtnFlag)
                                {
                                    m_JointEditDoc.SetSideClipImage(EmguCVPreviewImage.ToBitmap());
                                    m_JointEditDoc.AllocSideImage(pbyteBits);
                                }*/
                                
                                SetFrontStandingMode();
                                IDC_NextBtn.Visible = false;
                                if (m_JointEditDoc.m_FrontStandingImageBytes != null)
                                    IDC_NextBtn.Visible = true;
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                              /*  if (m_ShootBtnFlag)
                                {
                                    m_JointEditDoc.SetStandClipImage(EmguCVPreviewImage.ToBitmap());
                                    m_JointEditDoc.AllocStandingImage(pbyteBits);
                                }*/
                                SetFrontKneedownMode();
                                IDC_NextBtn.Visible = false;
                                if (m_JointEditDoc.m_FrontKneedownImageBytes != null)
                                    IDC_NextBtn.Visible = true;
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                               /* if (m_ShootBtnFlag)
                                {
                                    m_JointEditDoc.SetKneeClipImage(EmguCVPreviewImage.ToBitmap());
                                    m_JointEditDoc.AllocKneedownImage(pbyteBits);
                                }*/
                                m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP);
                                m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_NONE);                                
                                this.Visible = false;
                                m_JointEditDoc.m_Image_Path = imagepath; // added by rohini for GSP-1173
                                m_JointEditDoc.GetJointEditView().Visible = true;
                                m_JointEditDoc.GetJointEditView().RefreshForm();
                                
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        switch (m_JointEditDoc.GetMeasurementStartViewMode())
                        {
                            case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                                //m_JointEditDoc.SetSideClipImage(EmguCVPreviewImage.ToBitmap());
                                //m_JointEditDoc.AllocSideImage(pbyteBits);
                                SetFrontStandingMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                                //m_JointEditDoc.SetStandClipImage(EmguCVPreviewImage.ToBitmap());
                                //m_JointEditDoc.AllocStandingImage(pbyteBits);
                                SetFrontKneedownMode();
                                break;
                            case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                                // Œ‹‰Ê‰æ–Ê‚É‘JˆÚ‚·‚é.
                                //GetDocument()->ChangeToResultView();
                                //m_JointEditDoc.SetKneeClipImage(EmguCVPreviewImage.ToBitmap());
                                //m_JointEditDoc.AllocKneedownImage(pbyteBits);
                                m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_NONE);
                                FunctionToDisplayResultViewFromCrouchedView(EventArgs.Empty);
                                this.Visible = false;
                                //this.Close();
                                //DisposeControls();

                                break;
                            default:
                                break;

                        }
                    }
                    break;
                default:
                    break;

            }
            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_on;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            
            m_ShootBtnFlag = false;
        }


        public event EventHandler EventToStartNextScreen; // creating event handler - step1
        public void CloseFormToStartNextScreen(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToStartNextScreen;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        public event EventHandler EventfromCrouchedViewtoResultView;
        public void FunctionToDisplayResultViewFromCrouchedView(EventArgs e)
        {
            EventHandler eventHandler = EventfromCrouchedViewtoResultView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }

        }

        private void ImagePrevWnd_Paint(object sender, PaintEventArgs e)
        {

            int iSelectedBitmapWidth = m_ImageClipWnd.GetSelectedBitmapWidth();
            int iSelectedBitmapHeight = m_ImageClipWnd.GetSelectedBitmapHeight();
            int iSelectedBitmapSize = m_ImageClipWnd.CalcSelectedBitmapSize();
            byte[] pbyteBits = new byte[1024 * 1280 * 3];


            if (m_ShootBtnFlag && m_SearchFlag)
                m_ImageClipWnd.GetSelectedBitmap(e.Graphics, ref pbyteBits);
            else
            {
                
                switch(m_JointEditDoc.GetMeasurementStartViewMode())
                {
                    case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                        if(m_JointEditDoc.m_SideImageBytes != null)
                            m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_SideImageBytes);
                        break;
                    case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                        if(m_JointEditDoc.m_FrontStandingImageBytes != null)
                            m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontStandingImageBytes);
                        break;
                    case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                        if(m_JointEditDoc.m_FrontKneedownImageBytes != null)
                            m_ImagePreviewWnd.SetBackgroundBitmap(1024, 1280, m_JointEditDoc.m_FrontKneedownImageBytes);
                        break;
                    default:
                        break;
                }
                
                m_ImagePreviewWnd.UpdateOffscreen(e.Graphics);
            }
                  
            
        }

        private void IDC_RotateBtn_Click(object sender, EventArgs e)
        {
            m_Clickmode = "ROTATE";
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_down;
            t.Interval = 100;
            t.Tick += new EventHandler(timer_tick);
            t.Start();
            Bitmap bmp = null;

            //if (m_ReturnFlag)
            {
                switch (m_JointEditDoc.GetMeasurementStartViewMode())
                {
                    case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                        bmp = m_JointEditDoc.GetSideClipImage();
                        break;
                    case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                        bmp = m_JointEditDoc.GetStandClipImage();
                        break;
                    case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                        bmp = m_JointEditDoc.GetKneeClipImage();
                        break;
                    default:
                        break;
                }

            }
            //else
            {
                // bmp = m_JointEditDoc.GetFlipImage();
            }
            int iDestImageWidth = bmp.Height;//m_ImageClipWnd.m_iBackgroundHeight;
            int iDestImageHeight = bmp.Width;//m_ImageClipWnd.m_iBackgroundWidth;


            bmp.RotateFlip(RotateFlipType.Rotate270FlipXY);

            Image<Bgr, Byte> EmguCVImage = new Image<Bgr, Byte>(bmp);
            /*
            int iDestImageWidth = m_ImageClipWnd.m_iBackgroundHeight;
            int iDestImageHeight =m_ImageClipWnd.m_iBackgroundWidth;
            */
            //if (m_JointEditDoc.m_SideImageBytes == null)
            m_ImageClipWnd.SetBackgroundBitmap(iDestImageWidth, iDestImageHeight, EmguCVImage.Bytes);
            /*else
                m_ImageClipWnd.SetBackgroundBitmap(iDestImageWidth, iDestImageHeight, m_JointEditDoc.m_SideImageBytes);
                */
            ImageClipWnd.Invalidate();

            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
        }

        private void IDD_MEASUREMENT_START_VIEW_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawImage(bmBack1, (this.Width - bmBack1.Width) / 2, 0, bmBack1.Width, bmBack1.Height);
        }

        private void IDD_MEASUREMENT_START_VIEW_Resize(object sender, EventArgs e)
        {
            //MessageBox.Show("hi");
            //updated Using by Sumit GTL#118----START            
            System.Threading.Thread.Sleep(1);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.WaitForFullGCComplete();
            GC.Collect();
            //updated Using by Sumit GTL#118----END
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
        public void RefreshForm()
        {
            IDC_SearchBtn.Focus(); //added by rohini for GTL#50 
            m_ShootBtnFlag = true;
            m_SearchFlag = true;
            picturebox1.Image = Yugamiru.Properties.Resources.Mainpic3;
            IDC_ShootBtn.Image = Yugamiru.Properties.Resources.imagecopy_up;
            IDC_SearchBtn.Image = Yugamiru.Properties.Resources.imageload_up;
            IDC_RotateBtn.Image = Yugamiru.Properties.Resources.imagerotation_on;
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
            
            m_JointEditDoc.GetMainScreen().RefreshMenuStrip(false);
            
            switch (m_JointEditDoc.GetMeasurementStartViewMode())
            {
                case Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING:
                    pictureBox2.Image = Yugamiru.Properties.Resources.SpeechSideStanding;
                    SetSideStandingMode();
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING:
                    pictureBox2.Image = Yugamiru.Properties.Resources.SpeechFrontStanding;
                    SetFrontStandingMode();
                    break;
                case Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN:
                    pictureBox2.Image = Yugamiru.Properties.Resources.SpeechFrontKneedown;
                    SetFrontKneedownMode();
                    break;
                default:
                    break;


            }
            
        }
        //Added By Suhana For GSP 865
        private void IDD_MEASUREMENT_START_VIEW_DragDrop(object sender, DragEventArgs e)
        {
#region sideimage(drag and drop)
            if ((m_JointEditDoc.m_SideImageBytes == null) || (m_JointEditDoc.m_SideImageBytes != null))
            {

                string[] droppedFiles = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string file in droppedFiles)
                {
                    Image Img = null;
                    //var Img = Image.FromFile(file);
                    Img = Image.FromFile(file);
                    ImageConverter _imageConverter = new ImageConverter();
                    byte[] xZByte = (byte[])_imageConverter.ConvertTo(Img, typeof(byte[]));

                    //File.WriteAllBytes(Application.StartupPath + @"\storeSideImg.jpg", xZByte);
                    //FileStream fileStream = new FileStream(Application.StartupPath + @"\storeSideImg.jgetpg", FileMode.Open, FileAccess.Read);
                    File.WriteAllBytes(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)+ "\\gsport\\Yugamiru cloud\\storeSideImg.jpg", xZByte);
                    FileStream fileStream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)+"\\gsport\\Yugamiru cloud\\storeSideImg.jpg",FileMode.Open,FileAccess.Read);

                    m_JointEditDoc.m_Image_Path = fileStream.Name;
                    fileStream.Close();
                    m_currentImage = new Image<Bgr, byte>(m_JointEditDoc.m_Image_Path);
                    if (m_currentImage.Width % 2 != 0) // check for odd number
                    {
                        if (((m_currentImage.Width - 1) / 2) % 2 != 0)
                            m_currentImage = m_currentImage.Resize(m_currentImage.Width - 3, m_currentImage.Height - 2, Emgu.CV.CvEnum.Inter.Linear);
                        else
                            m_currentImage = m_currentImage.Resize(m_currentImage.Width - 1, m_currentImage.Height, Emgu.CV.CvEnum.Inter.Linear);
                    }
                    else if ((m_currentImage.Width / 2) % 2 != 0) // else check fraction of width is odd number
                        m_currentImage = m_currentImage.Resize(m_currentImage.Width - 2, m_currentImage.Height - 1, Emgu.CV.CvEnum.Inter.Linear);

                    m_JointEditDoc.SetSideClipImage(m_currentImage.ToBitmap());
                    m_ImageClipWnd.SetBackgroundBitmap(m_currentImage);
                    File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\gsport\\Yugamiru cloud\\storeSideImg.jpg");
                    ImageClipWnd.Invalidate();


                }
            }
#endregion
#region standimage(drag and drop)
            if ((m_JointEditDoc.m_FrontStandingImageBytes == null) || (m_JointEditDoc.m_FrontStandingImageBytes != null))
            {

                string[] droppedFiles = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string file in droppedFiles)
                {
                    Image Img = null;
                    //var Img = Image.FromFile(file);
                    Img = Image.FromFile(file);
                    ImageConverter _imageConverter = new ImageConverter();
                    byte[] xZByte = (byte[])_imageConverter.ConvertTo(Img, typeof(byte[]));
                    //File.WriteAllBytes(Application.StartupPath + @"\storeStandingImgs.jpg", xZByte);
                    //FileStream fileStream = new FileStream(Application.StartupPath + @"\storeStandingImgs.jpg", FileMode.Open, FileAccess.Read);
                    File.WriteAllBytes(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\gsport\\Yugamiru cloud\\storeStandingImgs.jpg", xZByte);
                    FileStream fileStream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\gsport\\Yugamiru cloud\\storeStandingImgs.jpg", FileMode.Open, FileAccess.Read);
                    m_JointEditDoc.m_Image_Path = fileStream.Name;
                    fileStream.Close();
                    m_currentImage = new Image<Bgr, byte>(m_JointEditDoc.m_Image_Path);
                    if (m_currentImage.Width % 2 != 0) // check for odd number
                    {
                        if (((m_currentImage.Width - 1) / 2) % 2 != 0)
                            m_currentImage = m_currentImage.Resize(m_currentImage.Width - 3, m_currentImage.Height - 2, Emgu.CV.CvEnum.Inter.Linear);
                        else
                            m_currentImage = m_currentImage.Resize(m_currentImage.Width - 1, m_currentImage.Height, Emgu.CV.CvEnum.Inter.Linear);
                    }
                    else if ((m_currentImage.Width / 2) % 2 != 0) // else check fraction of width is odd number
                        m_currentImage = m_currentImage.Resize(m_currentImage.Width - 2, m_currentImage.Height - 1, Emgu.CV.CvEnum.Inter.Linear);

                    m_JointEditDoc.SetKneeClipImage(m_currentImage.ToBitmap());
                    m_ImageClipWnd.SetBackgroundBitmap(m_currentImage);
                    File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\gsport\\Yugamiru cloud\\storeStandingImgs.jpg");
                    ImageClipWnd.Invalidate();

                }
            }
#endregion
#region kneeimage(drag and drop)
            if ((m_JointEditDoc.m_FrontKneedownImageBytes == null) || (m_JointEditDoc.m_FrontKneedownImageBytes != null))
            {

                string[] droppedFiles = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string file in droppedFiles)
                {
                    Image Img = null;
                    //var Img = Image.FromFile(file);
                    Img = Image.FromFile(file);
                    ImageConverter _imageConverter = new ImageConverter();
                    byte[] xZByte = (byte[])_imageConverter.ConvertTo(Img, typeof(byte[]));
                    //File.WriteAllBytes(Application.StartupPath + @"\storeKneedownImgs.jpg", xZByte);
                    //FileStream fileStream = new FileStream(Application.StartupPath + @"\storeKneedownImgs.jpg", FileMode.Open, FileAccess.Read);
                    File.WriteAllBytes(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\gsport\\Yugamiru cloud\\storeKneedownImgs.jpg", xZByte);
                    FileStream fileStream = new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\gsport\\Yugamiru cloud\\storeKneedownImgs.jpg", FileMode.Open, FileAccess.Read);

                    m_JointEditDoc.m_Image_Path = fileStream.Name;
                    fileStream.Close();
                    m_currentImage = new Image<Bgr, byte>(m_JointEditDoc.m_Image_Path);
                    if (m_currentImage.Width % 2 != 0) // check for odd number
                    {
                        if (((m_currentImage.Width - 1) / 2) % 2 != 0)
                            m_currentImage = m_currentImage.Resize(m_currentImage.Width - 3, m_currentImage.Height - 2, Emgu.CV.CvEnum.Inter.Linear);
                        else
                            m_currentImage = m_currentImage.Resize(m_currentImage.Width - 1, m_currentImage.Height, Emgu.CV.CvEnum.Inter.Linear);
                    }
                    else if ((m_currentImage.Width / 2) % 2 != 0) // else check fraction of width is odd number
                        m_currentImage = m_currentImage.Resize(m_currentImage.Width - 2, m_currentImage.Height - 1, Emgu.CV.CvEnum.Inter.Linear);

                    m_JointEditDoc.SetStandClipImage(m_currentImage.ToBitmap());
                    m_ImageClipWnd.SetBackgroundBitmap(m_currentImage);
                    File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\gsport\\Yugamiru cloud\\storeKneedownImgs.jpg");
                    ImageClipWnd.Invalidate();

                }
            }
#endregion
        }
        //Added By Suhana For GSP 865
        //Added By Suhana For GSP 865
        private void IDD_MEASUREMENT_START_VIEW_DragEnter(object sender, DragEventArgs e)
        {
            if ((m_JointEditDoc.m_SideImageBytes == null) || (m_JointEditDoc.m_SideImageBytes != null))
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
                {
                    e.Effect = DragDropEffects.All;
                }
            }

            if ((m_JointEditDoc.m_FrontStandingImageBytes == null) || (m_JointEditDoc.m_FrontStandingImageBytes != null))
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
                {
                    e.Effect = DragDropEffects.All;
                }
            }
            if ((m_JointEditDoc.m_FrontKneedownImageBytes == null) || (m_JointEditDoc.m_FrontKneedownImageBytes != null))
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
                {
                    e.Effect = DragDropEffects.All;
                }
            }
        }
        //Added By Suhana For GSP 865
    }

}
