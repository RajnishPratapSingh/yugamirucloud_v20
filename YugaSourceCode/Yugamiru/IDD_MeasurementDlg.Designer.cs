﻿namespace Yugamiru
{
    partial class IDD_MeasurementDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IDD_MeasurementDlg));
            this.IDC_NextBtn = new System.Windows.Forms.PictureBox();
            this.IDC_BackBtn = new System.Windows.Forms.PictureBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.IDC_COMBO_GENDER = new System.Windows.Forms.ComboBox();
            this.IDC_COMBO_MONTH = new System.Windows.Forms.ComboBox();
            this.IDC_BirthYear = new System.Windows.Forms.NumericUpDown();
            this.IDC_Height = new System.Windows.Forms.NumericUpDown();
            this.IDC_ID = new System.Windows.Forms.RichTextBox();
            this.IDC_Name = new System.Windows.Forms.RichTextBox();
            this.cross_btn_ID = new System.Windows.Forms.Button();
            this.cross_btn_Name = new System.Windows.Forms.Button();
            this.IDC_COMBO_DAY = new System.Windows.Forms.ComboBox();
            this.IDC_dtp_DOB = new System.Windows.Forms.DateTimePicker();
            this.txtHeight = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_NextBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BackBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BirthYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Height)).BeginInit();
            this.SuspendLayout();
            // 
            // IDC_NextBtn
            // 
            this.IDC_NextBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_NextBtn.Location = new System.Drawing.Point(417, 404);
            this.IDC_NextBtn.Name = "IDC_NextBtn";
            this.IDC_NextBtn.Size = new System.Drawing.Size(100, 50);
            this.IDC_NextBtn.TabIndex = 0;
            this.IDC_NextBtn.TabStop = false;
            this.IDC_NextBtn.Click += new System.EventHandler(this.IDC_NextBtn_Click);
            // 
            // IDC_BackBtn
            // 
            this.IDC_BackBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_BackBtn.Location = new System.Drawing.Point(62, 404);
            this.IDC_BackBtn.Name = "IDC_BackBtn";
            this.IDC_BackBtn.Size = new System.Drawing.Size(100, 50);
            this.IDC_BackBtn.TabIndex = 1;
            this.IDC_BackBtn.TabStop = false;
            this.IDC_BackBtn.Click += new System.EventHandler(this.IDC_BackBtn_Click);
            // 
            // IDC_COMBO_GENDER
            // 
            this.IDC_COMBO_GENDER.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_COMBO_GENDER.DropDownHeight = 100;
            this.IDC_COMBO_GENDER.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_COMBO_GENDER.FormattingEnabled = true;
            this.IDC_COMBO_GENDER.IntegralHeight = false;
            this.IDC_COMBO_GENDER.Location = new System.Drawing.Point(298, 161);
            this.IDC_COMBO_GENDER.Name = "IDC_COMBO_GENDER";
            this.IDC_COMBO_GENDER.Size = new System.Drawing.Size(121, 37);
            this.IDC_COMBO_GENDER.TabIndex = 2;
            this.IDC_COMBO_GENDER.SelectedIndexChanged += new System.EventHandler(this.IDC_COMBO_GENDER_SelectedIndexChanged);
            this.IDC_COMBO_GENDER.SelectedValueChanged += new System.EventHandler(this.IDC_COMBO_GENDER_SelectedValueChanged);
            this.IDC_COMBO_GENDER.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IDC_COMBO_GENDER_KeyDown);
            this.IDC_COMBO_GENDER.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IDC_COMBO_GENDER_KeyPress);
            this.IDC_COMBO_GENDER.KeyUp += new System.Windows.Forms.KeyEventHandler(this.IDC_COMBO_GENDER_KeyUp);
            // 
            // IDC_COMBO_MONTH
            // 
            this.IDC_COMBO_MONTH.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_COMBO_MONTH.DropDownHeight = 100;
            this.IDC_COMBO_MONTH.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_COMBO_MONTH.FormattingEnabled = true;
            this.IDC_COMBO_MONTH.IntegralHeight = false;
            this.IDC_COMBO_MONTH.ItemHeight = 29;
            this.IDC_COMBO_MONTH.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.IDC_COMBO_MONTH.Location = new System.Drawing.Point(437, 221);
            this.IDC_COMBO_MONTH.Name = "IDC_COMBO_MONTH";
            this.IDC_COMBO_MONTH.Size = new System.Drawing.Size(38, 37);
            this.IDC_COMBO_MONTH.TabIndex = 4;
            this.IDC_COMBO_MONTH.Visible = false;
            this.IDC_COMBO_MONTH.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IDC_COMBO_MONTH_KeyPress);
            // 
            // IDC_BirthYear
            // 
            this.IDC_BirthYear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_BirthYear.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_BirthYear.Location = new System.Drawing.Point(298, 221);
            this.IDC_BirthYear.Maximum = new decimal(new int[] {
            2019,
            0,
            0,
            0});
            this.IDC_BirthYear.Minimum = new decimal(new int[] {
            1800,
            0,
            0,
            0});
            this.IDC_BirthYear.Name = "IDC_BirthYear";
            this.IDC_BirthYear.Size = new System.Drawing.Size(120, 37);
            this.IDC_BirthYear.TabIndex = 3;
            this.IDC_BirthYear.Value = new decimal(new int[] {
            1980,
            0,
            0,
            0});
            this.IDC_BirthYear.Visible = false;
            this.IDC_BirthYear.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            this.IDC_BirthYear.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IDC_BirthYear_KeyDown);
            this.IDC_BirthYear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IDC_BirthYear_KeyPress);
            this.IDC_BirthYear.KeyUp += new System.Windows.Forms.KeyEventHandler(this.IDC_BirthYear_KeyUp);
            // 
            // IDC_Height
            // 
            this.IDC_Height.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_Height.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.IDC_Height.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_Height.Location = new System.Drawing.Point(299, 291);
            this.IDC_Height.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.IDC_Height.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.IDC_Height.Name = "IDC_Height";
            this.IDC_Height.Size = new System.Drawing.Size(120, 33);
            this.IDC_Height.TabIndex = 5;
            this.IDC_Height.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.IDC_Height.ValueChanged += new System.EventHandler(this.IDC_Height_ValueChanged);
            this.IDC_Height.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IDC_Height_KeyPress);
            this.IDC_Height.KeyUp += new System.Windows.Forms.KeyEventHandler(this.IDC_Height_KeyUp);
            this.IDC_Height.Leave += new System.EventHandler(this.IDC_Height_Leave);
            // 
            // IDC_ID
            // 
            this.IDC_ID.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_ID.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_ID.Location = new System.Drawing.Point(298, 40);
            this.IDC_ID.Multiline = false;
            this.IDC_ID.Name = "IDC_ID";
            this.IDC_ID.Size = new System.Drawing.Size(123, 32);
            this.IDC_ID.TabIndex = 0;
            this.IDC_ID.Text = "";
            this.IDC_ID.TextChanged += new System.EventHandler(this.IDC_ID_TextChanged);
            this.IDC_ID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IDC_ID_KeyDown);
            this.IDC_ID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IDC_ID_KeyPress);
            // 
            // IDC_Name
            // 
            this.IDC_Name.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_Name.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_Name.Location = new System.Drawing.Point(298, 98);
            this.IDC_Name.Multiline = false;
            this.IDC_Name.Name = "IDC_Name";
            this.IDC_Name.Size = new System.Drawing.Size(93, 42);
            this.IDC_Name.TabIndex = 1;
            this.IDC_Name.Text = "";
            this.IDC_Name.TextChanged += new System.EventHandler(this.IDC_Name_TextChanged_1);
            this.IDC_Name.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IDC_Name_KeyPress);
            // 
            // cross_btn_ID
            // 
            this.cross_btn_ID.BackColor = System.Drawing.Color.Transparent;
            this.cross_btn_ID.Image = ((System.Drawing.Image)(resources.GetObject("cross_btn_ID.Image")));
            this.cross_btn_ID.Location = new System.Drawing.Point(389, 40);
            this.cross_btn_ID.Name = "cross_btn_ID";
            this.cross_btn_ID.Size = new System.Drawing.Size(32, 32);
            this.cross_btn_ID.TabIndex = 6;
            this.cross_btn_ID.UseVisualStyleBackColor = false;
            this.cross_btn_ID.Click += new System.EventHandler(this.cross_btn_ID_Click);
            // 
            // cross_btn_Name
            // 
            this.cross_btn_Name.BackColor = System.Drawing.Color.Transparent;
            this.cross_btn_Name.Image = ((System.Drawing.Image)(resources.GetObject("cross_btn_Name.Image")));
            this.cross_btn_Name.Location = new System.Drawing.Point(360, 98);
            this.cross_btn_Name.Name = "cross_btn_Name";
            this.cross_btn_Name.Size = new System.Drawing.Size(32, 42);
            this.cross_btn_Name.TabIndex = 7;
            this.cross_btn_Name.UseVisualStyleBackColor = true;
            this.cross_btn_Name.Click += new System.EventHandler(this.cross_btn_Name_Click);
            // 
            // IDC_COMBO_DAY
            // 
            this.IDC_COMBO_DAY.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_COMBO_DAY.DropDownHeight = 100;
            this.IDC_COMBO_DAY.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_COMBO_DAY.FormattingEnabled = true;
            this.IDC_COMBO_DAY.IntegralHeight = false;
            this.IDC_COMBO_DAY.ItemHeight = 29;
            this.IDC_COMBO_DAY.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31"});
            this.IDC_COMBO_DAY.Location = new System.Drawing.Point(508, 221);
            this.IDC_COMBO_DAY.MaxLength = 2;
            this.IDC_COMBO_DAY.Name = "IDC_COMBO_DAY";
            this.IDC_COMBO_DAY.Size = new System.Drawing.Size(38, 37);
            this.IDC_COMBO_DAY.TabIndex = 15;
            this.IDC_COMBO_DAY.Visible = false;
            this.IDC_COMBO_DAY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IDC_COMBO_DAY_KeyPress);
            // 
            // IDC_dtp_DOB
            // 
            this.IDC_dtp_DOB.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_dtp_DOB.CustomFormat = "dd MMM yyyy";
            this.IDC_dtp_DOB.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F);
            this.IDC_dtp_DOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.IDC_dtp_DOB.Location = new System.Drawing.Point(299, 196);
            this.IDC_dtp_DOB.Margin = new System.Windows.Forms.Padding(2);
            this.IDC_dtp_DOB.MinDate = new System.DateTime(1800, 1, 1, 0, 0, 0, 0);
            this.IDC_dtp_DOB.Name = "IDC_dtp_DOB";
            this.IDC_dtp_DOB.Size = new System.Drawing.Size(246, 37);
            this.IDC_dtp_DOB.TabIndex = 3;
            this.IDC_dtp_DOB.Value = new System.DateTime(1980, 1, 1, 0, 0, 0, 0);
            this.IDC_dtp_DOB.ValueChanged += new System.EventHandler(this.IDC_dtp_DOB_ValueChanged);
            // 
            // txtHeight
            // 
            this.txtHeight.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHeight.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F);
            this.txtHeight.Location = new System.Drawing.Point(299, 291);
            this.txtHeight.MaxLength = 3;
            this.txtHeight.Multiline = false;
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.txtHeight.Size = new System.Drawing.Size(106, 33);
            this.txtHeight.TabIndex = 4;
            this.txtHeight.Text = "100";
            this.txtHeight.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtHeight_MouseClick);
            this.txtHeight.TextChanged += new System.EventHandler(this.txtHeight_TextChanged);
            this.txtHeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtHeight_KeyDown);
            this.txtHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHeight_KeyPress);
            this.txtHeight.Leave += new System.EventHandler(this.txtHeight_Leave);
            // 
            // IDD_MeasurementDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(631, 504);
            this.Controls.Add(this.txtHeight);
            this.Controls.Add(this.IDC_dtp_DOB);
            this.Controls.Add(this.IDC_COMBO_DAY);
            this.Controls.Add(this.cross_btn_Name);
            this.Controls.Add(this.cross_btn_ID);
            this.Controls.Add(this.IDC_Name);
            this.Controls.Add(this.IDC_ID);
            this.Controls.Add(this.IDC_Height);
            this.Controls.Add(this.IDC_BirthYear);
            this.Controls.Add(this.IDC_COMBO_MONTH);
            this.Controls.Add(this.IDC_COMBO_GENDER);
            this.Controls.Add(this.IDC_BackBtn);
            this.Controls.Add(this.IDC_NextBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IDD_MeasurementDlg";
            this.Text = "IDD_MeasurementDlg";
            this.Load += new System.EventHandler(this.IDD_MeasurementDlg_Load);
            this.SizeChanged += new System.EventHandler(this.IDD_MeasurementDlg_SizeChanged);
            this.VisibleChanged += new System.EventHandler(this.IDD_MeasurementDlg_VisibleChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.IDD_MeasurementDlg_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IDD_MeasurementDlg_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.IDC_NextBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BackBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BirthYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Height)).EndInit();
            this.ResumeLayout(false);

        }

       

        #endregion

        private System.Windows.Forms.PictureBox IDC_NextBtn;
        private System.Windows.Forms.PictureBox IDC_BackBtn;
        private System.Windows.Forms.BindingSource bindingSource1;
        public System.Windows.Forms.ComboBox IDC_COMBO_GENDER;
        private System.Windows.Forms.ComboBox IDC_COMBO_MONTH;
        private System.Windows.Forms.NumericUpDown IDC_BirthYear;
        public System.Windows.Forms.NumericUpDown IDC_Height;
        //Edited By Sumit GSP-1081-------START
        //private System.Windows.Forms.RichTextBox IDC_ID;
        //private System.Windows.Forms.RichTextBox IDC_Name;
        public System.Windows.Forms.RichTextBox IDC_ID;
        public System.Windows.Forms.RichTextBox IDC_Name;
        //Edited By Sumit GSP-1081-------END
        private System.Windows.Forms.Button cross_btn_ID;
        private System.Windows.Forms.Button cross_btn_Name;
        private System.Windows.Forms.ComboBox IDC_COMBO_DAY;
        private System.Windows.Forms.DateTimePicker IDC_dtp_DOB;
        public System.Windows.Forms.RichTextBox txtHeight;
    }
}