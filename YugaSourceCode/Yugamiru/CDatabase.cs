﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    class CDatabase
    {
        private SQLiteConnection sqlite;

        public CDatabase(string FilePath)
        {
            SQLiteConnection.CreateFile(FilePath);
        }
        public CDatabase()
        {
            sqlite = new SQLiteConnection("Data Source=" + Constants.db_file);
        }

        public DataTable selectQuery(string query)
        {
            SQLiteDataAdapter ad;
            DataTable dt = new DataTable();

            try
            {
                //Added/Edited By Sumit GTL#37   ---START

                //SQLiteCommand cmd;
                //sqlite.Open();  //Initiate connection to the db
                //cmd = sqlite.CreateCommand();
                //cmd.CommandText = query;  //set the passed query
                //ad = new SQLiteDataAdapter(cmd);
                //ad.Fill(dt); //fill the datasource

                
                sqlite.Open();  //Initiate connection to the db
                using (SQLiteCommand cmd = sqlite.CreateCommand())
                {
                    cmd.CommandText = query;  //set the passed query
                    ad = new SQLiteDataAdapter(cmd);
                    ad.Fill(dt); //fill the datasource
                    sqlite.Close();
                }
                //Added/Edited By Sumit GTL#37   ---END
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                MessageBox.Show(ex.Message);
            }
            sqlite.Close();
            return dt;
        }
        public void RetrieveImage(string Query)
        {
            //Added/Edited By Sumit GTL#37   ---START
            #region Code_B4_GTL#37
            //SQLiteCommand cmd;
            //sqlite.Open();
            //cmd = sqlite.CreateCommand();
            //cmd.CommandText = Query;
            //string ImageName = string.Empty;
            //string Specific_Folder = Constants.path + Yugamiru.Properties.Resources.CURRENT_LANGUAGE;
            //using (SQLiteDataReader rdr = cmd.ExecuteReader())
            //{
            //    while (rdr.Read())
            //    {
            //        ImageName = rdr.GetString(0);
            //        File.WriteAllBytes(Specific_Folder + "\\" + ImageName, (byte[])rdr["ImageBlob"]);
            //    }
            //}

            //// byte[] data = (byte[])cmd.ExecuteScalar();
            //sqlite.Close();
            #endregion
                        
            sqlite.Open();
            using (SQLiteCommand cmd = sqlite.CreateCommand())
            {
                cmd.CommandText = Query;
                string ImageName = string.Empty;
                string Specific_Folder = Constants.path + Yugamiru.Properties.Resources.CURRENT_LANGUAGE;
                using (SQLiteDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        ImageName = rdr.GetString(0);
                        File.WriteAllBytes(Specific_Folder + "\\" + ImageName, (byte[])rdr["ImageBlob"]);
                    }
                }
                // byte[] data = (byte[])cmd.ExecuteScalar();
                sqlite.Close();
            }
            //Added/Edited By Sumit GTL#37   ---END
            /*try
            {
                using (MemoryStream ms = new MemoryStream(data))
                {
                    ms.Position = 0;
                    Bitmap bmp = new Bitmap(ms); 
                    return bmp;
                }
            }
           try
            {
                if (data != null)
                {
                    File.WriteAllBytes("woman2.jpg", data);
                }
                else
                {
                    Console.WriteLine("Binary data not read");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }*/



        }
        public void ExecuteQuery(string txtQuery)
        {
            try
            {
                //Added/Edited By Sumit GTL#37   ---START

                //SQLiteCommand cmd;
                //sqlite.Open();
                //cmd = sqlite.CreateCommand();
                //cmd.CommandText = txtQuery;
                //cmd.ExecuteNonQuery();
                //sqlite.Close();


                sqlite.Open();
                using (SQLiteCommand cmd = sqlite.CreateCommand())
                {
                    cmd.CommandText = txtQuery;
                    cmd.ExecuteNonQuery();
                    sqlite.Close();
                }

                //Added/Edited By Sumit GTL#37   ---END
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                MessageBox.Show(ex.Message);
            }

        }

        public void InsertImage(string txtQuery, byte[] data)
        {
            try
            {

                //Added/Edited By Sumit GTL#37   ---START  
                
                //SQLiteCommand cmd;
                //sqlite.Open();
                //cmd = sqlite.CreateCommand();
                //cmd.CommandText = txtQuery;
                //cmd.Prepare();
                //cmd.Parameters.Add("@img", DbType.Binary, data.Length);
                //cmd.Parameters["@img"].Value = data;
                //cmd.ExecuteNonQuery();
                //sqlite.Close();

                sqlite.Open();
                using (SQLiteCommand cmd = sqlite.CreateCommand())
                {
                    cmd.CommandText = txtQuery;
                    cmd.Prepare();
                    cmd.Parameters.Add("@img", DbType.Binary, data.Length);
                    cmd.Parameters["@img"].Value = data;
                    cmd.ExecuteNonQuery();
                    sqlite.Close();
                }

                //Added/Edited By Sumit GTL#37   ---END
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                MessageBox.Show(ex.Message);
            }

        }

    }
}
