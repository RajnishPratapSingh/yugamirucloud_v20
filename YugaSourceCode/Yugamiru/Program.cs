﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO;

namespace Yugamiru
{
    static class Program
    {

        //Added By Sumit GSP-365------------START
        static Mutex mutex = new Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}");
        /// Added by sumit GSP-365---------------END

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Code change by Meena - to catch unexpected exception
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(MyHandler);
            // end - by Meena

            #region Original By Meena Commented By Sumit for one instance GSP-365
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);

            //#region Commented
            ///*IDD_BALANCELABO_DIALOG StartingForm = new IDD_BALANCELABO_DIALOG();
            //StartingForm.Text = Yugamiru.Properties.Resources.YUGAMIRU_TITLE;
            //Application.Run(StartingForm); Commented by meena becoz of out of memory exception*/ // this is the main page
            //#endregion

            //SettingDataMgr SettingDataMgr = new SettingDataMgr();
            //if (SettingDataMgr.ReadFromFile(@"Resources\", "Languageconfig.txt"))
            //{
            //    string strValue = string.Empty;
            //    if (SettingDataMgr.GetValriableValue("LANGUAGE", ref strValue))
            //    {
            //        CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(strValue);
            //    }

            //}

            //Application.Run(new IDD_BALANCELABO_DIALOG());

            ////Application.Run(new JointEditView());
            ////Application.Run(new Form4());
            #endregion

            //Edited By Sumit GSP-365------------START



            /// 
            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                SettingDataMgr SettingDataMgr = new SettingDataMgr();
                string strValue = string.Empty;
                if  (!File.Exists(Constants.programdata_path+"Languageconfig.txt"))
                {
                    if (SettingDataMgr.ReadFromFile(@"Resources\", "Languageconfig.txt"))
                    {
                        if (SettingDataMgr.GetValriableValue("LANGUAGE", ref strValue))
                        {
                            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(strValue);
                        }                      

                    }
                    mutex.ReleaseMutex();
                }
                else if(SettingDataMgr.ReadFromFile(Constants.programdata_path, "Languageconfig.txt"))
                {
                    
                    if (SettingDataMgr.GetValriableValue("LANGUAGE", ref strValue))
                    {
                        CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(strValue);
                    }
                    mutex.ReleaseMutex();
                }
                //Added by sumit GSP-1056--------START
                try
                {
                    string sid = WebComCation.keyRequest.GetComputerID();
                }//REDOIN21-------PLUS Days Left Button Resizing
                catch(Exception eexx)
                {
                    Environment.Exit(1);
                }
                //GSP-1056--------END
                //Added by Sumit GSP-1221 GSP-1361    ------START
                //SetProcessDPIAware();//commented by Rajnish for GSP-1411
                //Added by Sumit GSP-1221 GSP-1361    ------END
                Application.Run(new IDD_BALANCELABO_DIALOG());                
            }
            else
            {
                // send our Win32 message to make the currently running instance
                // jump on top of all the other windows
                NativeMethods.PostMessage(
                    (IntPtr)NativeMethods.HWND_BROADCAST,
                    NativeMethods.WM_SHOWME,
                    IntPtr.Zero,
                    IntPtr.Zero);
            }



            /// Edit by sumit GSP-365---------------END
            /// 

            /// Code changes by Meena - to delete scoresheet images when there
            /// is any unexpected exception at domain level
            /// 
           
            void MyHandler(object sender, UnhandledExceptionEventArgs args)
            {
                
                string Specific_Folder = Constants.path + Yugamiru.Properties.Resources.CURRENT_LANGUAGE;

                System.IO.DirectoryInfo di = new DirectoryInfo(Specific_Folder);
                try
                {
                    if (Directory.GetFiles(Specific_Folder).Length > 0)
                    {
                        //--Added by Rajnish For GSp-360-----start
                        string[] filePaths = Directory.GetFiles(Specific_Folder);
                        foreach (string filePath in filePaths)
                            File.Delete(filePath);
                        //--Added by Rajnish For GSp-360-----END
                    }
                    //Array.ForEach(Directory.GetFiles(Specific_Folder), File.Delete);//Commented by Rajnish for GSp-1360

                    if (!File.Exists(Specific_Folder + "temp.txt"))
                    {
                        File.CreateText(Specific_Folder + "\\temp.txt").Dispose();
                    }
                }
                catch (IOException ex)
                {
                    //MessageBox.Show(ex.Message);
                    //file is currently locked
                    System.Threading.Thread.Sleep(1000);//Added by Sumit GSP-1047
                    if (Directory.GetFiles(Specific_Folder).Length > 0)
                    {
                        //--Added by Rajnish For GSp-360-----start
                        string[] filePaths = Directory.GetFiles(Specific_Folder);
                        foreach (string filePath in filePaths)
                            File.Delete(filePath);
                        //--Added by Rajnish For GSp-360-----END
                    }
                    //Array.ForEach(Directory.GetFiles(Specific_Folder), File.Delete);//Commented by Rajnish for GSp-1360
                    if (!File.Exists(Specific_Folder + "temp.txt"))
                    {
                        File.CreateText(Specific_Folder + "\\temp.txt").Dispose();
                    }
                }
                Exception e = (Exception)args.ExceptionObject;
                //MessageBox.Show("MyHandler caught : " + e.Message);

            }
            // end - by Meena

        }

        //Added by Sumit GSP-1221 GSP-1361    ------START
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool SetProcessDPIAware();
        //Added by Sumit GSP-1221 GSP-1361---------END
    }
    //
    // this class just wraps some Win32 stuff that we're going to use
    internal class NativeMethods
    {
        public const int HWND_BROADCAST = 0xffff;
        public static readonly int WM_SHOWME = RegisterWindowMessage("WM_SHOWME");
        [DllImport("user32")]
        public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);
        [DllImport("user32")]
        public static extern int RegisterWindowMessage(string message);
    }
}
