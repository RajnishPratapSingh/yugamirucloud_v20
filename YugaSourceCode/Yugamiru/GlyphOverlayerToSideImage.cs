﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Yugamiru
{
    public class GlyphOverlayerToSideImage
    {
        //#define ARROW_OFFSET	30
        //#define ARROW_LENGTH	50
        //#define ARROW_WIDTH		8

        SideBodyPosition m_SideBodyPosition;

        // ’†Sü•\Ž¦;
        bool m_bValidCenterLine;
        int m_uiCenterLineStyle;
        Color m_crCenterLineColor;
        int m_uiCenterLineWidth;
        int m_iCenterLineXPos;
        int m_iCenterLineTop;
        int m_iCenterLineBottom;

        bool m_bValidCentroidLine;
        int m_uiCentroidLineStyle;
        Color m_crCentroidLineColor;
        int m_uiCentroidLineWidth;
        int m_iCentroidLineXPos;
        int m_iCentroidLineTop;
        int m_iCentroidLineBottom;

        // ŠÖßŠÔƒ‰ƒCƒ“•\Ž¦.
        bool m_bValidJointConnectionLine;
        uint m_uiJointConnectionLineStyle;
        Color m_crJointConnectionLineColor;
        uint m_uiJointConnectionLineWidth;

        // ŠÖß“_ƒ}[ƒJ[ƒTƒCƒY
        int m_iMarkerSize;

        public void MyDrawLine( Graphics pDC, Point st, Point end, int style /* =PS_SOLID */, Color rgb /*=RGB(0,0,0)*/, int width /*=1*/)
{
//	MoveToEx(hDC, st.x, st.y, NULL);
//	LineTo(hDC, end.x, end.y);

        uint[] type = new uint[8];       
        Pen penNew = new Pen(rgb, width);

            pDC.DrawLine(penNew, st, end);
      
    }


   public GlyphOverlayerToSideImage()
        {

            m_SideBodyPosition = new SideBodyPosition();

            m_bValidCenterLine = false;

            m_uiCenterLineStyle = 0;

    m_crCenterLineColor = Color.Red;

            m_uiCenterLineWidth = 0;

            m_iCenterLineXPos = 0;

            m_iCenterLineTop = 0;

            m_iCenterLineBottom = 0;

            m_bValidJointConnectionLine = false;

            m_uiJointConnectionLineStyle = 0;

            m_crJointConnectionLineColor = Color.Yellow;

            m_uiJointConnectionLineWidth = 0;

            m_iMarkerSize = 0;
    
    }

   

public void SetCenterLineData(
    bool bValid, int uiStyle, Color crColor, int uiWidth,
    int iXPos, int iTop, int iBottom)
{
    m_bValidCenterLine = bValid;
    m_uiCenterLineStyle = uiStyle;
    m_crCenterLineColor = crColor;
    m_uiCenterLineWidth = uiWidth;
    m_iCenterLineXPos = iXPos;
    m_iCenterLineTop = iTop;
    m_iCenterLineBottom = iBottom;
}

public void SetJointConnectionLineData(bool bValid, uint uiStyle, Color crColor, uint uiWidth)
{
    m_bValidJointConnectionLine = bValid;
    m_uiJointConnectionLineStyle = uiStyle;
    m_crJointConnectionLineColor = crColor;
    m_uiJointConnectionLineWidth = uiWidth;
}

public void SetMarkerSize(int iMarkerSize)
{
    m_iMarkerSize = iMarkerSize;
}

public void SetSideBodyPosition( SideBodyPosition SideBodyPosition )
{
    m_SideBodyPosition = SideBodyPosition;
}

public void DrawCenterLine(Graphics pDC)
{
    if (m_bValidCenterLine)
    {
        Point ptStart = new Point(m_iCenterLineXPos, m_iCenterLineTop);
        Point ptEnd = new Point(m_iCenterLineXPos, m_iCenterLineBottom);
        MyDrawLine(pDC, ptStart, ptEnd,
            (int)m_uiCenterLineStyle, m_crCenterLineColor,(int) m_uiCenterLineWidth);
    }
}
        public void DrawCentroidLine(Graphics pDC)
        {
            if (m_bValidCentroidLine)
            {
                Point ptStart = new Point(m_iCentroidLineXPos, m_iCentroidLineTop );
                Point ptEnd = new Point(m_iCentroidLineXPos, m_iCentroidLineBottom );
                MyDrawLine(pDC, ptStart, ptEnd,
                    m_uiCentroidLineStyle, m_crCentroidLineColor, m_uiCentroidLineWidth);
            }
        }

        public void DrawMarker(Graphics pDC, Point ptCenter, Color col, bool bFill)
{
    Pen penNew = new Pen(col);
    SolidBrush brNew = new SolidBrush(col);
    
    if (bFill)
    {
        pDC.FillEllipse(brNew, ptCenter.X - m_iMarkerSize / 2, ptCenter.Y - m_iMarkerSize / 2, m_iMarkerSize, m_iMarkerSize);
    }
    else
    {
        pDC.DrawEllipse(penNew, ptCenter.X - m_iMarkerSize / 2, ptCenter.Y - m_iMarkerSize / 2, m_iMarkerSize, m_iMarkerSize);
    }
   
}

public void DrawLineBetweenMarkers(Graphics pDC, Point ptStart, Point ptEnd )
{
    if (m_bValidJointConnectionLine)
    {
        MyDrawLine(pDC, ptStart, ptEnd,
            (int)m_uiJointConnectionLineStyle,
            m_crJointConnectionLineColor,
            (int)m_uiJointConnectionLineWidth);
    }
}

public void Draw(Graphics pDC)
{
    // ’†Sü•`‰æ.
    DrawCenterLine(pDC);
    DrawCentroidLine(pDC);

            Point ptHip = new Point();
    Point ptKnee = new Point();
    Point ptAnkle = new Point();
    Point ptShoulder = new Point();
    Point ptEar = new Point();
            //CPoint	ptChin;
            //CPoint	ptGlabella;

    m_SideBodyPosition.GetHipPosition(ref ptHip);
    m_SideBodyPosition.GetKneePosition(ref ptKnee);
    m_SideBodyPosition.GetAnklePosition(ref ptAnkle);
    m_SideBodyPosition.GetShoulderPosition(ref ptShoulder);
    m_SideBodyPosition.GetEarPosition(ref ptEar);
    //m_SideBodyPosition.GetChinPosition( ptChin );
    //m_SideBodyPosition.GetGlabellaPosition( ptGlabella );

    // ŠÖßÚ‘±ü•`‰æ.
    //DrawLineBetweenMarkers( pDC, ptChin,		ptGlabella );
    DrawLineBetweenMarkers(pDC, ptShoulder, ptHip);
    DrawLineBetweenMarkers(pDC, ptHip, ptKnee);
    DrawLineBetweenMarkers(pDC, ptKnee, ptAnkle);
    DrawLineBetweenMarkers(pDC, ptShoulder, ptHip);

    // ŠÖßƒ}[ƒJ[•`‰æ.
    DrawMarker(pDC, ptHip,Color.FromArgb(255, 0, 0), true);
    DrawMarker(pDC, ptKnee, Color.FromArgb(0, 255, 0), true);
    DrawMarker(pDC, ptAnkle,Color.FromArgb(0, 0, 255), true);
    DrawMarker(pDC, ptShoulder,Color.FromArgb(200, 200, 0), true);
    DrawMarker(pDC, ptEar,Color.FromArgb(126, 0, 126), true);
    //DrawMarker( pDC, ptChin,          RGB(126,126,  0), TRUE );
    //DrawMarker( pDC, ptGlabella,      RGB(126,126,  0), TRUE );
}
        public void SetCentroidLineData(
        bool bValid, int uiStyle, Color crColor, int uiWidth,
        int iXPos, int iTop, int iBottom)
        {
            m_bValidCentroidLine = bValid;
            m_uiCentroidLineStyle = uiStyle;
            m_crCentroidLineColor = crColor;
            m_uiCentroidLineWidth = uiWidth;
            m_iCentroidLineXPos = iXPos;
            m_iCentroidLineTop = iTop;
            m_iCentroidLineBottom = iBottom;
        }

    }
}
