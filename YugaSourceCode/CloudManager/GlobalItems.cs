﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CloudManager
{
    public class GlobalItems
    {
        static string _ComputerID = string.Empty;
        static string _ActivationKey = string.Empty;
        static string _sqliteDBPath = @"C:\ProgramData\gsport\Yugamiru cloud\database\yugamiru.sqlite"; //@"E:\GSPORT\SQLDBTest\cpdb.sqlite";

        //Function Added By Sumit GSP-1357  ---------START
        static GlobalItems()
        {
            WebComCation.LicenseValidator.SetActivationKey();
            SetValues(WebComCation.keyRequest.GetComputerID(), WebComCation.LicenseValidator.ActivationKey);
        }
        //Function Added By Sumit GSP-1357  ---------END
        //Function Added By Sumit GSP-1357  ---------START
        public static void ReleaseKeyFromUnInstaller()
        {
            WebComCation.LicenseValidator.SetActivationKey();
            WebComCation.frmReleaseLicense frmR = new WebComCation.frmReleaseLicense();
            frmR.btnRelease_Click(null, null);
            //WebComCation.frmReleaseLicense.
        }
        //Function Added By Sumit GSP-1357  ---------END


        public static bool SetValues(string computerID,string activationKey)
        {
            //_ActivationKey = "4444kgjyftyfjvjhg"; it is for testing
            //_ComputerID= "55555hgjyfjhg";         it is for testing

            if (_ActivationKey.Length == 0 && _ComputerID.Length == 0)
            {
                _ActivationKey = activationKey;
                _ComputerID = computerID;
            }
            return true;
        }
        public static string ActivationKey
        {
            get
            {
                if(_ActivationKey.Length==0)
                {
                    throw new Exception("Activation key has not been initialize yet");
                }
                return _ActivationKey;
            }
        }
        public static string ComputerID
        {
            get
            {
                if (_ComputerID.Length == 0)
                {
                    throw new Exception("ComputerID has not been initialize yet");
                }
                return _ComputerID;
            }
        }
        public static string sqliteDBPath
        {
            get
            {
                return _sqliteDBPath;
            }
        }
    }
}
