﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    public class AppDataReader
    {
        private static string conString = "Data Source="+@"C:\ProgramData\gsport\Yugamiru cloud\database\yugamiru.sqlite";
        

        public static DataTable GetPatientDetails(string UniqueId)
        {
            DataTable dt = new DataTable();
            //Added/Edited By Sumit GTL#37   ---START

            //SQLiteCommand cmd;
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            //cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            //cmd.CommandText = "select * from PatientDetails where uniqueid=" + UniqueId + ";";
            //SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            //dt = new DataTable();
            //ad.Fill(dt); //fill the datasource
            //             //close connection
            //AppDBCon.Close();

            
            SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            using (SQLiteCommand cmd = AppDBCon.CreateCommand())// ("select uniqueid,lut from PatientDetails");
            {
                cmd.CommandText = "select * from PatientDetails where uniqueid=" + UniqueId + ";";
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                dt = new DataTable();
                ad.Fill(dt); //fill the datasource
                             //close connection
                AppDBCon.Close();
            }
            //Added/Edited By Sumit GTL#37   ---END

            //Added by Sumit on 06-Dec-2018 For GSP-944-------------START
            foreach (DataRow dr in dt.Rows)
            {
                //PatientId column
                dr["PatientId"] = SplCharHandler.GetCodedString(dr["PatientId"].ToString());
                //Name column 
                dr["Name"] = SplCharHandler.GetCodedString(dr["Name"].ToString());
                //Comment Column
                dr["Comment"] = SplCharHandler.GetCodedString(dr["Comment"].ToString());

            }
            //Added by Sumit on 06-Dec-2018 For GSP-944-------------END


            return dt;
        }
        public static DataTable GetFrontBodyPositionKneeDown(string UniqueId)
        {
            DataTable dt = new DataTable();
            //Added/Edited By Sumit GTL#37   ---START

            //SQLiteCommand cmd;
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            //cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            //cmd.CommandText = "select * from FrontBodyPositionKneeDown where uniqueid=" + UniqueId + ";";
            ////cmd.CommandText = "select imagebytes from FrontBodyPositionKneeDown where uniqueid=" + UniqueId + ";";
            //SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            //dt = new DataTable();
            //ad.Fill(dt); //fill the datasource
            //             //close connection
            //AppDBCon.Close();
                        
            SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            using (SQLiteCommand cmd = AppDBCon.CreateCommand())// ("select uniqueid,lut from PatientDetails");
            {
                cmd.CommandText = "select * from FrontBodyPositionKneeDown where uniqueid=" + UniqueId + ";";
                //cmd.CommandText = "select imagebytes from FrontBodyPositionKneeDown where uniqueid=" + UniqueId + ";";
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                dt = new DataTable();
                ad.Fill(dt); //fill the datasource
                             //close connection
                AppDBCon.Close();
            }
            //Added/Edited By Sumit GTL#37   ---END
            return dt;
        }
        public static DataTable GetFrontBodyPositionStanding(string UniqueId)
        {
            DataTable dt = new DataTable();
            //Added/Edited By Sumit GTL#37   ---START

            //SQLiteCommand cmd;
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            //cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            //cmd.CommandText = "select * from FrontBodyPositionStanding where uniqueid=" + UniqueId + ";";
            //SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            //dt = new DataTable();
            //ad.Fill(dt); //fill the datasource
            //             //close connection
            //AppDBCon.Close();
            
            SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            using (SQLiteCommand cmd = AppDBCon.CreateCommand())// ("select uniqueid,lut from PatientDetails");
            {
                cmd.CommandText = "select * from FrontBodyPositionStanding where uniqueid=" + UniqueId + ";";
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                dt = new DataTable();
                ad.Fill(dt); //fill the datasource
                             //close connection
                AppDBCon.Close();
            }
            //Added/Edited By Sumit GTL#37   ---END
            return dt;
        }
        public static DataTable GetSideBodyPosition(string UniqueId)
        {
            DataTable dt = new DataTable();
            //Added/Edited By Sumit GTL#37   ---START

            //SQLiteCommand cmd;
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            //cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            //cmd.CommandText = "select * from SideBodyPosition where uniqueid=" + UniqueId + ";";
            //SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            //dt = new DataTable();
            //ad.Fill(dt); //fill the datasource
            //             //close connection
            //AppDBCon.Close();            
            SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            using (SQLiteCommand cmd = AppDBCon.CreateCommand())// ("select uniqueid,lut from PatientDetails");
            {
                cmd.CommandText = "select * from SideBodyPosition where uniqueid=" + UniqueId + ";";
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                dt = new DataTable();
                ad.Fill(dt); //fill the datasource
                             //close connection
                AppDBCon.Close();
            }
            //Added/Edited By Sumit GTL#37   ---END
            return dt;
        }


        public DataTable ReadTableData(Tables tableName)
        {
            DataTable dt = new DataTable();

            //Added/Edited By Sumit GTL#37   ---START

            //SQLiteCommand cmd;
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            //cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            //cmd.CommandText = "select * from " + tableName.ToString() + ";";
            //SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            //dt = new DataTable();
            //ad.Fill(dt); //fill the datasource
            //             //close connection
            //AppDBCon.Close();
                        
            SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            using (SQLiteCommand cmd = AppDBCon.CreateCommand())// ("select uniqueid,lut from PatientDetails");
            {
                cmd.CommandText = "select * from " + tableName.ToString() + ";";
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                dt = new DataTable();
                ad.Fill(dt); //fill the datasource
                             //close connection
                AppDBCon.Close();
            }
            //Added/Edited By Sumit GTL#37   ---END
            return dt;
        }


        public static List<string> GetUniqueIDsNeedToUpdate()
        {
            DataTable dt = new DataTable();
            //Added/Edited By Sumit GTL#37   ---START

            //SQLiteCommand cmd;
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            //cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            //cmd.CommandText = "select uniqueid from PatientDetails where lut != \"\" ";
            //SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            //dt = new DataTable();
            //ad.Fill(dt); //fill the datasource
            //             //close connection
            //AppDBCon.Close();
                        
            SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            using (SQLiteCommand cmd = AppDBCon.CreateCommand())// ("select uniqueid,lut from PatientDetails");
            {
                cmd.CommandText = "select uniqueid from PatientDetails where lut != \"\" ";
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                dt = new DataTable();
                ad.Fill(dt); //fill the datasource
                             //close connection
                AppDBCon.Close();
            }
            //Added/Edited By Sumit GTL#37   ---END
            List<string> uniqueIDs = new List<string>();

            foreach (DataRow dr in dt.Rows)
            {
                uniqueIDs.Add(dr["uniqueid"].ToString());
            }

            return uniqueIDs;
        }
        public static int GetRecordCount()
        {
            int retval = -1;
            DataTable dt = new DataTable();

            //Added/Edited By Sumit GTL#37   ---START
            #region Code_B4_GTL#37
            //SQLiteCommand cmd;
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            //cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            //cmd.CommandText = "select count(uniqueid) from PatientDetails;";
            //SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            //dt = new DataTable();
            //ad.Fill(dt); //fill the datasource
            //             //close connection
            //AppDBCon.Close();
            #endregion

            SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            using (SQLiteCommand cmd = AppDBCon.CreateCommand())// ("select uniqueid,lut from PatientDetails");
            {
                cmd.CommandText = "select count(uniqueid) from PatientDetails;";
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                dt = new DataTable();
                ad.Fill(dt); //fill the datasource
                             //close connection
                AppDBCon.Close();
            }
            //Added/Edited By Sumit GTL#37   ---END
            if (dt!=null && dt.Rows.Count>0)
            {
                retval = System.Convert.ToInt16(dt.Rows[0][0].ToString());
            }
            else
            {
                retval = 0;
            }
            return retval;
        }
    }
}
