﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    class CloudUtility
    {
        /// <summary>
        /// Get most latest (LUT) Last Updated Timestamp
        /// </summary>
        /// <returns></returns>
        public static Cloud.PatientRecord MaxLatest(string sqliteDBPath=@"C:\ProgramData\gsport\Yugamiru cloud\database\yugamiru.sqlite", string tableName="PatientDetails")
        {
            Cloud.PatientRecord retVal = new Cloud.PatientRecord();
            //sqliteDBPath = "cpdb.sqlite";
            SQLiteConnection AppDBCon = new SQLiteConnection("Data Source=" + sqliteDBPath);
            try
            {
                //Added/Edited By Sumit GTL#37   ---START

                //SQLiteCommand cmd;
                //AppDBCon.Open();  //Initiate connection to the db
                //cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
                //cmd.CommandText = "select uniqueid,lut from PatientDetails;";
                //SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                //DataTable dt = new DataTable();
                //ad.Fill(dt); //fill the datasource
                ////close connection
                //AppDBCon.Close();

                DataTable dt = new DataTable();
                AppDBCon.Open();  //Initiate connection to the db
                using (SQLiteCommand cmd = AppDBCon.CreateCommand())// ("select uniqueid,lut from PatientDetails");
                {
                    cmd.CommandText = "select uniqueid,lut from PatientDetails;";
                    SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                    
                    ad.Fill(dt); //fill the datasource
                                 //close connection
                    AppDBCon.Close();
                }
                //Added/Edited By Sumit GTL#37   ---END

                List<Cloud.PatientRecord> patientRecords = new List<Cloud.PatientRecord>();
                foreach(DataRow dr in dt.Rows)
                {
                    Cloud.PatientRecord pr = new Cloud.PatientRecord(dr);                    
                    patientRecords.Add(pr);
                }
                Cloud.PatientRecord tempPr = null;
                int cnt = 0;
                foreach(Cloud.PatientRecord lpr in patientRecords)
                {
                    cnt++;
                    if(cnt==1 )
                    {
                        tempPr = lpr;
                    }
                    else if(lpr.LUT==tempPr.LUT ||  lpr.LUT>tempPr.LUT)
                    {
                        tempPr = lpr;
                    }
                }
                retVal = tempPr;             
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                // MessageBox.Show(ex.Message);
                throw ex;
            }
            catch (Exception ex)
            {
                //Add your exception code here.
                // MessageBox.Show(ex.Message);
                throw ex;
            }

            return retVal;
        }
    }
}
