﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;

namespace CloudManager
{
    public class ConvertTableToJSON
    {
        public Tables tableName = Tables.None;
        //public string TableName = "PatientDetails";
        public string activationKey = GlobalItems.ActivationKey;//"activationKeykgjyftyfjvjhg";//string.Empty;
        public string ComputerId = GlobalItems.ComputerID;//"ComputerIdhgjyfjhg";// string.Empty;
        public string UniqueId = "87";// string.Empty;
        public string lastUpdateTime = "140418152200";// string.Empty;
        public string UploadTime = "1734";// string.Empty;
        private List<string> columnNames;
        public String strJonToSend = string.Empty;

        public Dictionary<string, string> dcPatientDetails;

        /// <summary>
        /// Initializes an instance from existing row records
        /// </summary>
        /// <param name="dt">DataTable having one row only</param>
        public ConvertTableToJSON(DataTable dt,Tables TableName,RowNumToJSON whichRow=RowNumToJSON.FirstRow )
        {
            if (dt == null || dt.Columns.Count == 0 || dt.Rows.Count == 0)
            {
                throw new Exception("This Data row does not contains any records");
            }


            strJonToSend = "[{\"TableName\":\"" + TableName.ToString() + "\"," + Environment.NewLine;
            strJonToSend += "\"Computerid\":\"" + ComputerId + "\"," + Environment.NewLine;
            strJonToSend += "\"Activationkey\":\"" + activationKey + "\"," + Environment.NewLine;

            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.ToUpper() == "LUT")
                        continue;
                    //if (dc.ColumnName == "ImageBytes")
                    //{
                    //    string stringToBeEncoded = dr[dc.ColumnName].ToString(); //strJonToSend;//"some string";
                    //    byte[] byteArray = System.Text.ASCIIEncoding.ASCII.GetBytes(stringToBeEncoded);
                    //    string base64String = System.Convert.ToBase64String(byteArray);

                    //    //strJonToSend += "\"" + dc.ColumnName + "\":\"" + dr[dc.ColumnName] + "\"," + Environment.NewLine;
                    //    strJonToSend += "\"" + dc.ColumnName + "\":\"" + base64String + "\"," + Environment.NewLine;
                    //}
                    //else
                    {
                        strJonToSend += "\"" + dc.ColumnName + "\":\"" + dr[dc.ColumnName] + "\"," + Environment.NewLine;
                    }
                }

                //For testing one row at a time
                if (whichRow == RowNumToJSON.FirstRow)
                    break;
            }
            //strJonToSend=strJonToSend.Remove()
            strJonToSend = strJonToSend.Remove(strJonToSend.LastIndexOf(Environment.NewLine));
            strJonToSend = strJonToSend.Remove(strJonToSend.LastIndexOf(","));
            strJonToSend = strJonToSend + "}]";

            ////string h=Convert.ToBase64String(strJonToSend.)
            ////=========================================================
            //string stringToBeEncoded1 = strJonToSend;//"some string";
            //byte[] byteArray1 = System.Text.ASCIIEncoding.ASCII.GetBytes(stringToBeEncoded1);
            //string base64String1 = System.Convert.ToBase64String(byteArray1);
            //strJonToSend = base64String1;
            ////=========================================================
        }
        public String GetJSONPatientDetails()
        {
            return strJonToSend;
        }
        public String GetJson()
        {
            return strJonToSend;
        }
    }
}
