﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Data.SQLite;
using System.Data;
using System.Diagnostics;

namespace UninstallProcessController
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            string languageConfig_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
             "\\gsport\\Yugamiru cloud\\LanguageConfig.txt";
            string languageSettingContent = System.IO.File.ReadAllText(languageConfig_file);
            if (languageSettingContent.Contains(@"""" + "en" + @""""))
            {
                CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(@"en");
            }
            else if (languageSettingContent.Contains(@"""" + "ja-JP" + @""""))
            {
                CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("ja-JP");
            }

            //Added by Sumit GTL#122  ---START            
            try
            {
                Process[] ps = Process.GetProcesses();
                foreach (Process kp in ps)
                {
                    if (kp.ProcessName.ToUpper().Contains("YUGAMIRU") && !kp.ProcessName.ToUpper().Contains("CLOUD"))
                    {
                        //kp.CloseMainWindow();
                        kp.Kill();
                    }
                }
            }
            catch
            {
                
            }
            //Added by Sumit GTL#122  ---END

            //Condition added Sumit GTL#94 (NR Task)
            //Application.Run(new Form1());
            if (GetCount() > 0)
            {
                Application.Run(new Form1());
            }
            else
            {
                //Release the key and Delete from ProgramData now as going to uninstall
                ReleaseAndClean();
            }
        }

        /// <summary>
        /// Function added Sumit GTL#94   (NR Task)
        /// </summary>
        /// <returns></returns>
        public static int GetCount()
        {
            SQLiteDataAdapter ad;
            SQLiteConnection cn;
            int retval = 0;
            DataTable dtTotal = new DataTable();            
            string db_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
            "\\gsport\\Yugamiru cloud\\database\\Yugamiru.sqlite";
            try
            {
                cn = new SQLiteConnection("Data Source=" + db_file);
                using (SQLiteCommand cmd = new SQLiteCommand(cn))
                {
                    //Total-----------------
                    cn.Open();
                    //cmd = cn.CreateCommand();
                    cmd.CommandText = "select count(*) from PatientDetails";  //set the passed query
                    using (ad = new SQLiteDataAdapter(cmd))
                    {
                        ad.Fill(dtTotal);
                    }
                    cn.Close();
                    if (dtTotal.Rows.Count > 0)
                    {
                        try
                        {
                            retval = Convert.ToInt16(dtTotal.Rows[0][0].ToString().Trim());
                        }
                        catch { }
                     
                    }
                    //retval = dtTotal.Rows.Count;
                }
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
            }
            return retval;
        }

        /// <summary>
        /// Function added Sumit GTL#94   (NR Task)
        /// </summary>
        static void ReleaseAndClean()
        {
            string db_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
             "\\gsport\\Yugamiru cloud\\database\\Yugamiru.sqlite";
            string deleteAddressDatabase = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                 "\\gsport\\database";
            string deleteAddressGsport = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                 "\\gsport";

            //Release the key now as going to uninstall
            CloudManager.GlobalItems.ReleaseKeyFromUnInstaller();
            //isCancelled = false;
            //System.Threading.Thread.Sleep(1000);
            try
            {
                if (System.IO.File.Exists(db_file))
                    System.IO.File.Delete(db_file);
            }
            catch (Exception ex)
            {

            }

            try
            {
                if (System.IO.Directory.Exists(deleteAddressDatabase))
                    System.IO.Directory.Delete(deleteAddressDatabase, true);
            }
            catch
            {

            }
            //System.Threading.Thread.Sleep(100);
            try
            {
                if (System.IO.Directory.Exists(deleteAddressDatabase))
                    System.IO.Directory.Delete(deleteAddressDatabase, true);
            }
            catch
            {

            }
            try
            {
                if (System.IO.Directory.Exists(deleteAddressGsport))
                    System.IO.Directory.Delete(deleteAddressGsport, true);
            }
            catch
            {

            }
            //System.Threading.Thread.Sleep(100);
            try
            {
                if (System.IO.Directory.Exists(deleteAddressGsport))
                    System.IO.Directory.Delete(deleteAddressGsport, true);
            }
            catch
            {

            }            
        }
    }
}
