﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Net;
using System.Configuration.Install;

namespace UninstallProcessController
{
    public partial class Form1 : Form
    {
        SQLiteConnection cn;
        bool isCancelled = true;

        string db_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
             "\\gsport\\Yugamiru cloud\\database\\Yugamiru.sqlite";
        string deleteAddressDatabase = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
             "\\gsport\\database";
        string deleteAddressGsport = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
             "\\gsport";

        public Form1()
        {
            InitializeComponent();
            //this.label1.Text = "Yugamiru Data Loss Warning";
            this.label1.Text = Properties.Resources.DATA_LOSS_WARNING;

            //this.label2.Text = "Uninstalling Yugamiru Cloud will remove all saved patient reports from this PC. S" +
            //"ee the details below.";
            this.label2.Text = Properties.Resources.UNINSTALL_REMOVE_DATA;

            //this.label4.Text = "Total number of reports will be removed from PC";
            this.label4.Text = Properties.Resources.TOTAL_REPORTS_WILL_REMOVED;

            //this.label5.Text = "Number of reports not saved on cloud";
            this.label5.Text = Properties.Resources.NUMBER_NOT_SAVED_REPORTS;

            //this.label6.Text = "Number of reports saved on cloud";
            this.label6.Text = Properties.Resources.NUMBER_SAVED_REPORTS;

            this.btnSaveToCloud.Text = global::UninstallProcessController.Properties.Resources.SAVE_TO_CLOUD_NOW;
            //Adding SetCounts(); here by sumit GTL#94 (NR TASK)   ---START
            SetCounts();
            //Adding SetCounts(); here by sumit GTL#94 (NR TASK)   ---END

        }

        private void btnSaveToCloud_Click(object sender, EventArgs e)
        {
            uploadRecords();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Commenting SetCounts(); here by sumit GTL#95 (NR TASK)   ---START
            //SetCounts();
            //Commenting SetCounts(); here by sumit GTL#95 (NR TASK)   ---END
        }

        public void SetCounts()
        {
            SQLiteDataAdapter ad;
            DataTable dtTotal = new DataTable();
            DataTable dtSavedToCloud = new DataTable();
            DataTable dtNotSavedOnCloud = new DataTable();

            try
            {
                cn = new SQLiteConnection("Data Source=" + db_file);
                using (SQLiteCommand cmd = new SQLiteCommand(cn))
                {
                    //Total-----------------
                    cn.Open();
                    //cmd = cn.CreateCommand();
                    cmd.CommandText = "select count(*) from PatientDetails";  //set the passed query
                    using (ad = new SQLiteDataAdapter(cmd))
                    {
                        ad.Fill(dtTotal);
                    }
                    cn.Close();
                    if (dtTotal.Rows.Count > 0)
                    {
                        string cnt = dtTotal.Rows[0][0].ToString();
                        lblTotalReportsCount.Text = cnt;
                    }
                    //Added else condition by Sumit GTL#94 (NR Task)   ---START
                    else
                    {
                        CloudManager.GlobalItems.ReleaseKeyFromUnInstaller();
                        isCancelled = false;
                        this.Close();
                    }
                    //Added else condition by Sumit GTL#94 (NR Task)   ---END
                    //--------------------

                    //Saved -----------------
                    cn.Open();
                    //cmd = cn.CreateCommand();
                    cmd.CommandText = "select count(*) from PatientDetails where lut is NULL OR length(lut)=0;";  //set the passed query
                    using (ad = new SQLiteDataAdapter(cmd))
                    {
                        ad.Fill(dtSavedToCloud);
                    }
                    cn.Close();
                    if (dtSavedToCloud.Rows.Count > 0)
                    {
                        string cnt = dtSavedToCloud.Rows[0][0].ToString();
                        lblCloudReportsCount.Text = cnt;
                    }
                    
                    //--------------------


                    //Not Saved
                    cn.Open();
                    //cmd = cn.CreateCommand();
                    cmd.CommandText = "select count(*) from PatientDetails where lut is NOT NULL AND length(lut)>0;";  //set the passed query
                    using (ad = new SQLiteDataAdapter(cmd))
                    {
                        ad.Fill(dtNotSavedOnCloud);
                    }
                    cn.Close();
                    if (dtNotSavedOnCloud.Rows.Count > 0)
                    {
                        string cnt = dtNotSavedOnCloud.Rows[0][0].ToString();
                        lblNotSavedReportsCount.Text = cnt;
                        if (cnt == "0")
                            btnSaveToCloud.Enabled = false;
                    }
                }
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
            }            
        }

        void uploadRecords()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (IsInternetConnected())
                {
                    var a=CloudManager.GlobalItems.ActivationKey;
                    CloudManager.StartUpdateToServer.StartSending();
                    Cursor.Current = Cursors.Default;
                    //MessageBox.Show("Reports uploaded to Yugamiru Cloud successfully.", "Yugamiru");
                    MessageBox.Show(Properties.Resources.NUMBER_REPORTS_UPLOADED, "Yugamiru");
                    SetCounts();
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                    //MessageBox.Show("No internet connection. Please connect to internet and retry.", "Yugamiru");                    
                    MessageBox.Show(Properties.Resources.NO_NET_CONNECT_RETRY, "Yugamiru");
                }
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                //MessageBox.Show("Error:Records cannot be uploaded at this time.","Yugamiru",MessageBoxButtons.OK,MessageBoxIcon.Error);
                MessageBox.Show(Properties.Resources.ERROR_CANNOT_UPLOAD, "Yugamiru", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        public static bool IsInternetConnected()
        {

            using (var client = new WebClient())
            {
                try
                {
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {                        
                        return true;
                    }
                }
                catch (Exception ex1)
                {
                    //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                    return false;
                }
            }

        }

        private void btnContinueUninstall_Click(object sender, EventArgs e)
        {
            //Release the key now as going to uninstall
            try
            {
                CloudManager.GlobalItems.ReleaseKeyFromUnInstaller();
            }
            catch(Exception ex)
            {

            }
            //isCancelled = false;
            System.Threading.Thread.Sleep(1000);
            try
            {
                if (System.IO.File.Exists(db_file))
                    System.IO.File.Delete(db_file);
            }
            catch(Exception ex)
            {

            }

            try
            {
                if (System.IO.Directory.Exists(deleteAddressDatabase))
                    System.IO.Directory.Delete(deleteAddressDatabase, true);
            }
            catch
            {

            }
            System.Threading.Thread.Sleep(100);
            try
            {
                if (System.IO.Directory.Exists(deleteAddressDatabase))
                    System.IO.Directory.Delete(deleteAddressDatabase, true);
            }
            catch
            {

            }
            try
            {
                if (System.IO.Directory.Exists(deleteAddressGsport))
                    System.IO.Directory.Delete(deleteAddressGsport, true);
            }
            catch
            {

            }
            System.Threading.Thread.Sleep(100);
            try
            {
                if (System.IO.Directory.Exists(deleteAddressGsport))
                    System.IO.Directory.Delete(deleteAddressGsport, true);
            }
            catch
            {

            }
            isCancelled = false;
            this.Close();
        }

        private void btnCancelUninstall_Click(object sender, EventArgs e)
        {
            //throw new Exception("Cancelling the uninstallation");
            //Environment.Exit(1);//Will rollback the uninstallation
            isCancelled = true;
            //Environment.Exit(1);
            System.Diagnostics.Process p = System.Diagnostics.Process.GetCurrentProcess();
            p.Kill();            
            Environment.Exit(1);
            //throw new InstallException("Cancelling the uninstallation");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isCancelled)
            {
                //System.Diagnostics.Process p = System.Diagnostics.Process.GetCurrentProcess();
                //p.Kill();
                //Environment.Exit(1);
                //throw new InstallException("Cancelling the uninstallation");
                //DialogResult dr = MessageBox.Show("Cannot cancel the uninstallation at this stage. Continue the uninstallation?", "Yugamiru", MessageBoxButtons.YesNo);
                DialogResult dr = MessageBox.Show(Properties.Resources.CANNOT_CANCEL, "Yugamiru", MessageBoxButtons.YesNo);
                if (dr==DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    e.Cancel = true;
                    btnContinueUninstall_Click(null, null);
                }
            }
        }
    }
}
